def read_mdl(n):
    file_name = '../problemsL/LA%3.3i_tgt.mdl' % n
    with open(file_name, 'rb') as f:
        r = ord(f.read(1))
        mat = []
        i = 8
        b = 0
        for _ in range(r * r * r):
            if i == 8:
                i = 0
                b = ord(f.read(1))
            mat.append(1 if b & 1 << i else 0)
            i += 1
    return r, mat


def main():
    import sys

    m = __import__(sys.argv[1])
    r, mat = read_mdl(int(sys.argv[2]))
    turn_num = 1
    ofile = '%s/LA%3.3i.txt' % (sys.argv[1], int(sys.argv[2]))
    with open(ofile, 'w') as f:
        for turn in m.NanobotManager(20, r, mat).do():
            print(turn_num, len(turn), file=f)
            for cmd in turn:
                print(cmd, file=f)
            turn_num += 1


if __name__ == '__main__':
    main()
