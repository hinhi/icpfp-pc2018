class Vec3(object):
    def __init__(self, x: int, y: int, z: int):
        self.x = x
        self.y = y
        self.z = z

    def __add__(self, other: 'Vec3') -> 'Vec3':
        return Vec3(
            self.x + other.x,
            self.y + other.y,
            self.z + other.z,
        )

    def __iadd__(self, other: 'Vec3') -> 'Vec3':
        self.x += other.x
        self.y += other.y
        self.z += other.z
        return self

    def __sub__(self, other: 'Vec3') -> 'Vec3':
        return Vec3(
            self.x - other.x,
            self.y - other.y,
            self.z - other.z,
        )

    def __isub__(self, other: 'Vec3') -> 'Vec3':
        self.x -= other.x
        self.y -= other.y
        self.z -= other.z
        return self

    def __pos__(self) -> 'Vec3':
        return Vec3(self.x, self.y, self.z)

    def __neg__(self) -> 'Vec3':
        return Vec3(-self.x, -self.y, -self.z)

    def mlen(self):
        return abs(self.x) + abs(self.y) + abs(self.z)

    def clen(self):
        return max([abs(self.x), abs(self.y), abs(self.z)])

    def lld(self):
        if self.x == 0:
            if self.y == 0:
                return abs(self.z)
            elif self.z == 0:
                return abs(self.y)
        elif self.y == 0 and self.z == 0:
            return abs(self.x)
        raise ValueError('not linear coordinate difference')

    def is_nd(self):
        return 0 < self.mlen() <= 2 and self.clen() == 1

    def __str__(self):
        return '%i %i %i' % (self.x, self.y, self.z)

    def __repr__(self):
        return 'Vec3(%s, %s, %s)' % (self.x, self.y, self.z)


class CMDBase(object):
    def __init__(self, bid: int):
        self.bid = bid

    def __str__(self):
        return '%s %s' % (self.__class__.__name__, self.bid)


class Halt(CMDBase):
    pass


class Wait(CMDBase):
    pass


class Flip(CMDBase):
    pass


class SMove(CMDBase):
    def __init__(self, bid: int, lld: Vec3):
        super().__init__(bid)
        self.lld = lld
        assert self.lld.lld() <= 15, self.lld

    def __str__(self):
        return '%s %s' % (super().__str__(), self.lld)


class LMove(CMDBase):
    def __init__(self, bid: int, sld1: Vec3, sld2: Vec3):
        super().__init__(bid)
        self.sld1 = sld1
        self.sld2 = sld2
        assert self.sld1.lld() <= 5, self.sld1
        assert self.sld2.lld() <= 5, self.sld2

    def __str__(self):
        return '%s %s %s' % (super().__str__(), self.sld1, self.sld2)


class Fission(CMDBase):
    def __init__(self, bid: int, nd: Vec3, m: int):
        super().__init__(bid)
        self.nd = nd
        self.m = m
        assert self.nd.is_nd()

    def __str__(self):
        return '%s %s %i' % (super().__str__(), self.nd, self.m)


class Fill(CMDBase):
    def __init__(self, bid: int, nd: Vec3):
        super().__init__(bid)
        self.nd = nd
        assert self.nd.is_nd()

    def __str__(self):
        return '%s %s' % (super().__str__(), self.nd)


class FusionP(CMDBase):
    def __init__(self, bid: int, nd: Vec3):
        super().__init__(bid)
        self.nd = nd
        assert self.nd.is_nd()

    def __str__(self):
        return '%s %s' % (super().__str__(), self.nd)


class FusionS(CMDBase):
    def __init__(self, bid: int, nd: Vec3):
        super().__init__(bid)
        self.nd = nd
        assert self.nd.is_nd()

    def __str__(self):
        return '%s %s' % (super().__str__(), self.nd)
