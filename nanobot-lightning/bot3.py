from typing import List
from queue import deque
from lib import *


class Bot:

    def __init__(self, bid: int, pos: Vec3, seeds: List[int]):
        self.bid = bid
        self.pos = pos
        self.seeds = seeds


class NanobotManager:

    def __init__(self, bots, r, mat):
        self.bot_num = bots
        self.r = r
        self.mat = mat
        self.bot = Bot(1, Vec3(0, 0, 0), [i for i in range(2, bots + 1)])
        self.y = 0
        self.nd = [
            Vec3(0, -1, 0),
            Vec3(1, -1, 0),
            Vec3(-1, -1, 0),
            Vec3(0, -1, 1),
            Vec3(0, -1, -1),
        ]
        self.harmonics = 0

    def get_next_layer(self):
        layer = []
        yy = self.y * self.r
        for x in range(self.r):
            xx = x * self.r * self.r
            for z in range(self.r):
                layer.append(self.mat[xx + yy + z])
        self.y += 1
        return layer

    def best_pos(self, layer, pos):

        def can(p):
            c = 0
            for nd in self.nd:
                pp = p + nd
                if 0 <= pp.x < self.r and 0 <= pp.z < self.r and layer[pp.x * self.r + pp.z]:
                    return True
            return False

        checked = [False] * len(layer)
        q = deque([pos])
        while q:
            pos = q.popleft()
            for dx, dz in [
                    [1, 0],
                    [-1, 0],
                    [0, 1],
                    [0, -1],
                    ]:
                p = pos + Vec3(dx, 0, dz)
                if 0 <= p.x < self.r and 0 <= p.z < self.r:
                    n = p.x * self.r + p.z
                    if checked[n]:
                        continue
                    checked[n] = True
                    if can(p):
                        return p
                    q.append(p)

    def best_move(self, bid, start, goal):
        def m(d):
            if d > 0:
                return min(d, 15)
            else:
                return -min(-d, 15)
        dd = []
        while start.x != goal.x:
            d = Vec3(m(goal.x - start.x), 0, 0)
            dd.append(d)
            start = start + d
        while start.z != goal.z:
            d = Vec3(0, 0, m(goal.z - start.z))
            dd.append(d)
            start = start + d
        while start.y != goal.y:
            d = Vec3(0, m(goal.y - start.y), 0)
            dd.append(d)
            start = start + d
        cmd = []
        i = 0
        while i < len(dd):
            if i + 1 < len(dd) and dd[i].lld() <= 5 and dd[i + 1].lld() <= 5:
                cmd.append(LMove(bid, dd[i], dd[i + 1]))
                i += 2
            else:
                cmd.append(SMove(bid, dd[i]))
                i += 1
        return cmd

    def do(self):
        cmd = []
        layer = []
        while True:
            if not any(layer):
                layer = self.get_next_layer()
                if not any(layer):
                    break
                if self.bot.pos.y != self.y:
                    dy = Vec3(0, self.y - self.bot.pos.y, 0)
                    cmd.append([SMove(self.bot.bid, dy)])
                    self.bot.pos += dy
            for nd in self.nd:
                p = self.bot.pos + nd
                if not (0 <= p.x < self.r and 0 <= p.z < self.r):
                    continue
                if layer[p.x * self.r + p.z]:
                    layer[p.x * self.r + p.z] = 0
                    if self.y > 1 and self.harmonics == 0:
                        self.harmonics = 1
                        cmd.append([Flip(self.bot.bid)])
                    cmd.append([Fill(self.bot.bid, nd)])
            p = self.best_pos(layer, self.bot.pos)
            if p is None:
                continue
            for c in self.best_move(self.bot.bid, self.bot.pos, p):
                cmd.append([c])
            self.bot.pos = p

        for c in self.best_move(self.bot.bid, self.bot.pos, Vec3(0, 0, self.bot.pos.y)):
            cmd.append([c])
        self.bot.pos = Vec3(0, 0, self.bot.pos.y)
        for c in self.best_move(self.bot.bid, self.bot.pos, Vec3(0, 0, 0)):
            cmd.append([c])
        self.bot.pos += Vec3(0, 0, 0)

        if self.harmonics == 1:
            cmd.append([Flip(self.bot.bid)])

        cmd.append([Halt(self.bot.bid)])
        return cmd
