
#include "../lib/system.h"

#define debug false

using namespace std;

int R;

bool existUpper(int ty) {
	REP(y, ty, R) rep(x, R) rep(z, R) {
		if (System::model[x][y][z]) return true;
	}
	return false;
}

bool searchPlane(int y, int size, Vector3D &ret) {
	rep(x, R - size) rep(z, R - size) {
		bool ok = true;
		rep(dx, size) rep(dz, size) {
			if (!System::model[x + dx][y][z + dz] || System::matrix[x + dx][y][z + dz]) {
				ok = false;
				dx = size;
				dz = size;
			}
		}
		if (ok) {
			ret.x = x;
			ret.y = y;
			ret.z = z;
			return true;
		}
	}
	return false;
}

int main(int argc, char *argv[]) {
	string filename = "../../problemsF/FA186_tgt.mdl";
	if (argc == 2) {
		filename = string(argv[1]);
	}

	R = read_mdl(filename.c_str(), 0);

	System system(R, 39, 0);

	system.beforeTurn();
	system.fission(system.bots[0], Vector3D(1, 0, 0), 2);
	system.afterTurn();

	system.beforeTurn();
	system.wait(system.bots[0]);
	system.fission(system.bots[1], Vector3D(0, 0, 1), 1);
	system.afterTurn();

	system.beforeTurn();
	system.wait(system.bots[0]);
	system.wait(system.bots[1]);
	system.fission(system.bots[2], Vector3D(-1, 0, 0), 0);
	system.afterTurn();

	const int CHECK_MODEL_EXIST = 1;
	const int GO_TO_NEXT_LAYER = 2;
	const int CHECK_PLANE_EXIST = 3;
	const int FILL_PLANE = 4;
	const int FILL_INDIVIDUAL = 5;
	const int GO_BACK = 991;
	const int END = 999;

	int status = CHECK_MODEL_EXIST;

	vector<Vector3D> targets(4);

	int planeSize = -1;
	Vector3D plane;

	int goBackCnt = 0;
	bool prevTurnFlipped = false;

	while (status != END) {
		system.beforeTurn();

		int loopCnt = 0;
		while (true) {
			if (debug) {
				cerr << "## STATUS = " << status << endl;
				rep(id, 4) {
					cerr << " bot " << id << " (" << system.bots[id].pos.x << ", " << system.bots[id].pos.y << ", "
						 << system.bots[id].pos.z << ")" << endl;
				}
			}

			if (loopCnt++ == 10) {
				cout << "MUGEN LOOP!!" << endl;
				return 0;
			}

			if (status == CHECK_MODEL_EXIST) {
				int y = system.bots[0].pos.y;
				if (existUpper(y)) {
					targets[0] = Vector3D(0, y + 1, 0);
					targets[1] = Vector3D(R - 1, y + 1, 0);
					targets[2] = Vector3D(R - 1, y + 1, R - 1);
					targets[3] = Vector3D(0, y + 1, R - 1);
					status = GO_TO_NEXT_LAYER;
				} else {
					targets[0] = Vector3D(0, 0, 0);
					targets[1] = Vector3D(1, 0, 0);
					targets[2] = Vector3D(1, 1, 0);
					targets[3] = Vector3D(0, 1, 0);
					status = GO_BACK;
				}
				continue;
			}

			if (status == GO_TO_NEXT_LAYER) {
				vector<bool> moved(4, false);
				rep(id, 4) {
					Vector3D diff = targets[id] - system.bots[id].pos;
					if (diff.x) {
						moved[id] = true;
						if (!system.fmove(system.bots[id], 0, min(15, max(-15, diff.x)))) {
							system.wait(system.bots[id]);
						}
					} else if (diff.y) {
						moved[id] = true;
						if (!system.fmove(system.bots[id], 1, min(15, max(-15, diff.y)))) {
							system.wait(system.bots[id]);
						}
					} else if (diff.z) {
						moved[id] = true;
						if (!system.fmove(system.bots[id], 2, min(15, max(-15, diff.z)))) {
							system.wait(system.bots[id]);
						}
					}
				}
				int moveCnt = 0;
				rep(id, 4) moveCnt += moved[id];
				if (moveCnt) {
					rep(id, 4) {
						if (moved[id]) continue;
						system.wait(system.bots[id]);
					}
				} else {
					status = CHECK_PLANE_EXIST;
					continue;
				}
				break;
			}

			if (status == CHECK_PLANE_EXIST) {
				planeSize = -1;
				for (int sz = 30; sz > 5; sz--) {
					if (searchPlane(system.bots[0].pos.y - 1, sz, plane)) {
						planeSize = sz;
						break;
					}
				}
				if (planeSize == -1) {
					status = FILL_INDIVIDUAL;
					continue;
				}
				status = FILL_PLANE;
				targets[0] = plane + Vector3D(0, 1, 0);
				targets[1] = plane + Vector3D(planeSize - 1, 1, 0);
				targets[2] = plane + Vector3D(planeSize - 1, 1, planeSize - 1);
				targets[3] = plane + Vector3D(0, 1, planeSize - 1);
				continue;
			}

			if (status == FILL_PLANE) {
				vector<bool> moved(4, false);
				rep(id, 4) {
					Vector3D diff = targets[id] - system.bots[id].pos;
					if (diff.x) {
						moved[id] = true;
						system.fmove(system.bots[id], 0, min(15, max(-15, diff.x)));
					} else if (diff.z) {
						moved[id] = true;
						system.fmove(system.bots[id], 2, min(15, max(-15, diff.z)));
					}
				}
				int moveCnt = 0;
				rep(id, 4) moveCnt += moved[id];
				if (moveCnt) {
					rep(id, 4) {
						if (moved[id]) continue;
						system.wait(system.bots[id]);
					}
					break;
				} else {
					bool ok = system.harmonics;
					if (!ok) {
						ok = true;
						REP(x, system.bots[0].pos.x, system.bots[2].pos.x) {
							REP(z, system.bots[0].pos.z, system.bots[2].pos.z) {
								if (!system._isGround(x, system.bots[0].pos.y - 1, z)) {
									ok = false;
									break;
								}
							}
							if (!ok) break;
						}
					}

					if (ok) {
						Vector3D diff(0, -1, 0);
						system.GFillPlane(system.bots[0], system.bots[1], system.bots[2], system.bots[3], diff, diff,
										  diff, diff);
					} else {
						system.flip(system.bots[0]);
						REP(i, 1, 4) system.wait(system.bots[i]);
					}
					status = CHECK_PLANE_EXIST;
				}
				break;
			}

			if (status == FILL_INDIVIDUAL) {
				if (system.harmonics && !prevTurnFlipped && system.canLowHarmonics()) {
					system.flip(system.bots[0]);
					REP(i, 1, 4) system.wait(system.bots[i]);
					break;
				}

				prevTurnFlipped = false;

				int rx[5] = {-1, 0, 0, 0, 1};
				int rz[5] = {0, -1, 0, 1, 0};

				targets[0] = Vector3D(0, system.bots[0].pos.y + 1, 0);
				targets[1] = Vector3D(R - 1, system.bots[0].pos.y + 1, 0);
				targets[2] = Vector3D(R - 1, system.bots[0].pos.y + 1, R - 1);
				targets[3] = Vector3D(0, system.bots[0].pos.y + 1, R - 1);

				vector<bool> moved(4, false);
				rep(id, 4) {
					Vector3D &pos = system.bots[id].pos;

					bool filled = true;
					rep(dir, 5) {
						int x = pos.x + rx[dir];
						int y = pos.y - 1;
						int z = pos.z + rz[dir];
						if (System::model[x][y][z] && !System::matrix[x][y][z]) {
							filled = false;
							moved[id] = true;
							if (system.harmonics || system._isGround(x, y, z)) {
								system.fill(system.bots[id], Vector3D(rx[dir], -1, rz[dir]));
							} else {
								system.flip(system.bots[id]);
								prevTurnFlipped = true;
							}
							break;
						}
					}
					if (filled) {
						// 塗り終わってたら移動
						int sx = id == 0 || id == 3 ? 0 : R / 2;
						int tx = id == 0 || id == 3 ? R / 2 : R;
						int sz = id == 0 || id == 1 ? 0 : R / 2;
						int tz = id == 0 || id == 1 ? R / 2 : R;

						int maxVal = 0;
						Vector3D maxPos;
						REP(x, sx, tx) REP(z, sz, tz) {
							int val = 0;
							rep(dir, 5) {
								int nx = x + rx[dir];
								int ny = pos.y - 1;
								int nz = z + rz[dir];
								if (!system.check(nx, ny, nz)) continue;
								val += System::model[nx][ny][nz] && !System::matrix[nx][ny][nz];
							}
							if (val > maxVal) {
								maxVal = val;
								maxPos = Vector3D(x, pos.y, z);
							}
						}
						if (maxVal) {
							Vector3D diff = maxPos - system.bots[id].pos;
							if (diff.x) {
								moved[id] = true;
								if (!system.optmove(system.bots[id], maxPos)) {
									system.wait(system.bots[id]);
								}
							} else if (diff.z) {
								moved[id] = true;
								if (!system.optmove(system.bots[id], maxPos)) {
									system.wait(system.bots[id]);
								}
							}
						}
					}
				}
				int moveCnt = 0;
				rep(id, 4) moveCnt += moved[id];
				if (moveCnt) {
					rep(id, 4) {
						if (moved[id]) continue;
						Vector3D diff = targets[id] - system.bots[id].pos;
						if (diff.x) {
							moved[id] = true;
							if (!system.fmove(system.bots[id], 0, min(15, max(-15, diff.x)))) {
								system.wait(system.bots[id]);
							}
						} else if (diff.z) {
							moved[id] = true;
							if (!system.fmove(system.bots[id], 2, min(15, max(-15, diff.z)))) {
								system.wait(system.bots[id]);
							}
						} else {
							system.wait(system.bots[id]);
						}
					}
				} else {
					status = CHECK_MODEL_EXIST;
					continue;
				}
				break;
			}

			if (status == GO_BACK) {
				if (system.harmonics) {
					system.flip(system.bots[0]);
					REP(i, 1, 4) system.wait(system.bots[i]);
					break;
				}

				vector<bool> moved(4, false);
				REP(id, 1, 4) {
					if (!system.bots[id].isActive) continue;
					if (system.mlen(system.bots[id].pos - system.bots[0].pos) == 1) {
						system.fusion(system.bots[0], system.bots[id]);
						moved[0] = true;
						moved[id] = true;
						break;
					}
				}
				REP(id, 1, 4) {
					if (!system.bots[id].isActive) continue;
					if (moved[id]) continue;
					moved[id] = true;
					Vector3D diff = system.bots[0].pos - system.bots[id].pos;
					if (diff.x) {
						if (!system.fmove(system.bots[id], 0, min(15, max(-15, diff.x)))) {
							system.wait(system.bots[id]);
						}
					} else if (diff.z) {
						if (!system.fmove(system.bots[id], 2, min(15, max(-15, diff.z)))) {
							system.wait(system.bots[id]);
						}
					} else {
						system.wait(system.bots[id]);
					}
				}

				int moveCnt = 0;
				rep(id, 4) moveCnt += moved[id];
				if (moveCnt == 0) {
					Vector3D diff = Vector3D(0, 0, 0) - system.bots[0].pos;
					if (diff.x) {
						system.smove(system.bots[0], 0, min(15, max(-15, diff.x)));
					} else if (diff.z) {
						system.smove(system.bots[0], 2, min(15, max(-15, diff.z)));
					} else if (diff.y) {
						system.smove(system.bots[0], 1, min(15, max(-15, diff.y)));
					} else {
						status = END;
						system.halt();
					}
				} else if (!moved[0]) {
					system.wait(system.bots[0]);
				}

				break;
			}
		}

		system.afterTurn();
	}

	return 0;
}
