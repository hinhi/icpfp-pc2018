from lib import *


class NanobotManager:

    def __init__(self, bots, r, mat):
        self.bot_num = bots
        self.r = r
        self.mat = mat

    def do(self):
        return [
            [
                Wait(1),
            ],
            [
                SMove(1, Vec3(15, 0, 0)),
            ],
            [
                LMove(1, Vec3(-5, 0, 0), Vec3(0, 1, 0)),
            ],
            [
                Fission(1, Vec3(0, 0, 1), 10),
            ],
            [
                Wait(1),
                Fill(2, Vec3(0, 0, 1)),
            ],
            [
                FusionP(1, Vec3(0, 0, 1)),
                FusionS(2, Vec3(0, 0, -1)),
            ],
            [
                LMove(1, Vec3(0, -1, 0), Vec3(-5, 0, 0)),
            ],
            [
                SMove(1, Vec3(-5, 0, 0)),
            ],
            [
                Halt(1),
            ],
        ]
