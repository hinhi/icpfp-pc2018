import logging
from flask import Flask, render_template, request
from flask_bootstrap import Bootstrap
app = Flask(__name__)
app.config['JSON_AS_ASCII'] = True
Bootstrap(app)
logging.basicConfig(level=logging.INFO)
log = logging.getLogger('nbt_analyzer')


import sys
sys.path.append('..')


@app.route('/', methods=['GET'])
def index():
    import os
    from const import problem_info
    from aws import NBT_DIR, aws_client
    from lib.iobin import read_nbt
    from lib.nbt_core import simulate

    aws_client.get_nbt_files()
    inputs = os.listdir(NBT_DIR)
    inputs.sort()

    if request.args.get('target'):
        target = request.args['target']
        problem_id = target.split('_')[0]

        r = problem_info[problem_id]['R']
        commands = read_nbt(open(NBT_DIR + target, 'rb'))

        time, energy, data = simulate(r, commands)
        log.info('target=%s, time=%s, energy=%s' % (target, time, energy))

        results = {}
        for t in range(time):
            for k in results:
                if k not in data[t]:
                    results[k].append(0)

            for k, v in data[t].items():
                if k in results:
                    results[k].append(sum(v.values()))
                else:
                    results[k] = [0 for i in range(t)]
                    results[k].append(sum(v.values()))
        results['time'] = [t for t in range(time)]

        dist = {}
        for action in (
                'active', 'smove', 'lmove', 'fusion', 'fission',
                'fill', 'void', 'gfill', 'gvoid'):
            dist[action] = []
            for i in range(20):
                if i+1 in data[-1]:
                    if action in data[-1][i+1]:
                        dist[action].append(data[-1][i+1][action])
                    else:
                        dist[action].append(0)

        return render_template('index.html', inputs=inputs, results=results,
                               num=len(dist['active']), r=r,
                               target=target, dist=dist)

    return render_template('index.html', inputs=inputs)


@app.route('/score_board', methods=['GET'])
def score_board():
    import json
    from aws import aws_client
    from const import problem_info

    standing = json.load(open('../crowler/standing.json'))
    score = aws_client.get_score()

    target_id = request.args.get('problem_id')
    target_name = request.args.get('name')
    target_time = request.args.get('submit_time')

    del standing['total']
    problem_ids = list(standing.keys())
    names = list(set([s['name'] for s in score]))
    submit_times = list(set([s['submit_time'] for s in score]))

    problem_ids.sort()
    names.sort()
    submit_times.sort(reverse=True)

    results = []
    stats = {'key': [], 'energy': []}
    for s in score:
        if s['problem_id'] not in standing:
            continue

        if target_id and s['problem_id'] != target_id:
            continue

        if target_name and s['name'] != target_name:
            continue

        if target_time and s['submit_time'] != target_time:
            continue

        s['R'] = problem_info[s['problem_id']]['R']

        if s['problem_id'] in standing:
            top = standing[s['problem_id']][0]
            s['top_team'] = top['team']
            s['top_energy'] = top['total_energy']
            s['top_score'] = top['total_score']

        results.append(s)

        if target_time:
            stats['key'].append(s['problem_id'])
            stats['energy'].append(s['energy'])
        elif target_id:
            stats['key'].append('%s_%s' % (s['name'], s['submit_time']))
            stats['energy'].append(s['energy'])

    results.sort(key=lambda x: (x['problem_id']))
    results.sort(key=lambda x: (x['submit_time']), reverse=True)
    if stats['key']:
        stats['key'], stats['energy'] = zip(
            *sorted(zip(stats['key'], stats['energy'])))
    return render_template('score_board.html', results=results, stats=stats,
                           problem_ids=problem_ids, names=names,
                           submit_times=submit_times,
                           target_id=target_id, target_name=target_name,
                           target_time=target_time)


@app.route('/totals', methods=['GET'])
def totals():
    import json
    from aws import aws_client
    from const import problem_info

    standing = json.load(open('../crowler/standing.json'))
    score = aws_client.get_score()
    results = standing['total']

    totals = {}
    for s in score:
        if s['problem_id'] not in standing:
            continue

        key = '%s_%s' % (s['name'], s['submit_time'])

        if key in totals:
            totals[key]['total_energy'] += s['energy']
        else:
            totals[key] = {
                'team': key,
                'total_energy': s['energy'],
                'class': 'info',
            }

    results.extend(totals.values())
    results.sort(key=lambda x: x['total_energy'])
    return render_template('totals.html', results=results)


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5000)
