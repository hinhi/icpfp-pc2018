import os
import re
import json
import boto3


NBT_DIR = '/home/sugihara/icfp-pc2018/nbt/'
FILENAME_PATTERN = re.compile('(F[ADR]\d\d\d)/(.+)/(\d+)_(\d+).nbt')
RESULT_PATTERN = re.compile('(F[ADR]\d\d\d)_(.+)_(\d+).json')
RESULT_DIR = '/home/sugihara/icfp-pc2018/result/'


class AwsClient:
    def __init__(self):
        s3 = boto3.resource('s3')
        self.bucket = s3.Bucket('chirimenjako-2018-2')

    def get_json(self, filename):
        self.bucket.download_file(Key=filename, Filename=filename)
        return json.load(open(filename))

    def get_score(self):
        for obj in self.bucket.objects.all():
            m = RESULT_PATTERN.match(obj.key)
            if m:
                problem_id = m.group(1)
                name = m.group(2)
                submit_time = m.group(3)
                target = RESULT_DIR + '%s_%s_%s.json' % (
                    problem_id, name, submit_time)
                if not os.path.exists(target):
                    self.bucket.download_file(Key=obj.key, Filename=target)

        results = []
        for filename in os.listdir(RESULT_DIR):
            m = RESULT_PATTERN.match(filename)
            if m:
                problem_id = m.group(1)
                name = m.group(2)
                submit_time = m.group(3)
                target = RESULT_DIR + '%s_%s_%s.json' % (
                    problem_id, name, submit_time)

                try:
                    s = json.load(open(target))
                except json.decoder.JSONDecodeError:
                    continue
                results.append({
                    'problem_id': problem_id,
                    'name': name,
                    'submit_time': submit_time,
                    'energy': s['energy'],
                })
        return results

    def get_nbt_files(self):
        for obj in self.bucket.objects.all():
            m = FILENAME_PATTERN.match(obj.key)
            if m:
                problem_id = m.group(1)
                name = m.group(2)
                submit_time = m.group(3)
                turn = m.group(4)
                target = NBT_DIR + '%s_%s_%s.nbt' % (
                    problem_id, name, submit_time)
                if not os.path.exists(target):
                    self.bucket.download_file(Key=obj.key, Filename=target)

    def upload_file(self, filename, Key=None):
        self.bucket.upload_file(filename, Key=Key)


aws_client = AwsClient()
