import re
import os
import sys
import json
from const import problem_info


sys.path.append('..')
from lib.iobin import read_nbt
from lib.nbt_core import simulate
from aws import aws_client, NBT_DIR


def score(problem_id, energy, top):
    from math import log2

    r = problem_info[problem_id]['R']
    dflt = problem_info[problem_id]['energy']

    return int(log2(r)) * 1000 * (dflt - energy) / (dflt - top)


if __name__ == '__main__':
    pat = re.compile('(F[ADR]\d\d\d)_(.+)_(\d{14}).nbt')
    results = []
    for target in os.listdir(NBT_DIR):
        m = pat.match(target)
        if m:
            problem_id = m.group(1)
            name = m.group(2)
            submit_time = m.group(3)


            if problem_info[problem_id].get('energy'):
                commands = read_nbt(open(NBT_DIR + '/' + target, 'rb'))
                r = problem_info[problem_id]['R']

                standing = aws_client.get_standing()
                if not standing.get(problem_id):
                    continue

                top = standing[problem_id][0]['total_energy']
                time, energy, data = simulate(r, commands, with_history=False)
                results.append({
                    'problem_id': problem_id,
                    'name': name,
                    'submit_time': submit_time,
                    'energy': energy,
                    'score': score(problem_id, energy, top),
                })

    with open('score.json', 'w') as f:
        f.write(json.dumps(results, indent=4, ensure_ascii=False))

    aws_client.bucket.upload_file(Key='score.json', Filename='score.json')
