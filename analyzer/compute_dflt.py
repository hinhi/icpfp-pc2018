import os
import sys
import json
from const import problem_info


sys.path.append('..')
from lib.iobin import read_nbt
from lib.nbt_core import simulate


target_dir = '../dfltTracesF'
for target in os.listdir(target_dir):
    commands = read_nbt(open(target_dir + '/' + target, 'rb'))
    problem_id = target.replace('.nbt', '')
    r = problem_info[problem_id]['R']
    time, energy, data = simulate(r, commands, with_history=False)
    problem_info[problem_id]['energy'] = energy
    print('%s: %s: %s' % (problem_id, r, energy))

with open('problem_info.json', 'w') as f:
    f.write(json.dumps(problem_inf, indent=4, ensure_ascii=False))
