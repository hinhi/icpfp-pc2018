# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String, DateTime
from db import Base


class Entity:
    @classmethod
    def import_dict(cls, data):
        entity = cls()
        for c in cls.__table__.columns:
            key = c.key
            val = data.get(key)
            if val:
                setattr(entity, key, val)
        return entity

    def export_dict(self):
        data = {}
        for c in self.__table__.columns:
            data[c.key] = getattr(self, c.key)
        return data


class ScoreBoard(Base, Entity):
    __tablename__ = 'score_board'

    score_board_id = Column(Integer, primary_key=True)
    name = Column(String)
    submit_time = Column(Integer)
    problem_id = Column(String)
    energy = Column(Integer)
