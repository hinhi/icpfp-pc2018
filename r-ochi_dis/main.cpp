
#include "../lib/system.h"

#define debug false

using namespace std;

int R;

bool searchPlane(int y, int size, Vector3D &ret) {
	rep(x, R - size) rep(z, R - size) {
		bool ok = true;
		rep(dx, size) rep(dz, size) {
			if (!System::model[x + dx][y][z + dz] || System::matrix[x + dx][y][z + dz]) {
				ok = false;
				dx = size;
				dz = size;
			}
		}
		if (ok) {
			ret.x = x;
			ret.y = y;
			ret.z = z;
			return true;
		}
	}
	return false;
}

int main(int argc, char *argv[]) {
	string filename = "../../problemsF/FD078_src.mdl";
	if (argc == 2) {
		filename = string(argv[1]);
	}

	R = read_mdl(filename.c_str(), 1);

	System system(R, 39, 1);

	const int FIRST_MOVE_UP = 1;
	const int FISSION = 2;
	const int CHECK_PLANE_EXIST = 3;
	const int STAND_BY = 4;
	const int DIG = 5;
	const int GO_BACK = 991;
	const int END = 999;

	int status = FIRST_MOVE_UP;

	vector<Vector3D> targets(4);

	int planeSize = -1;
	Vector3D plane;

	int planeX = 0;
	int planeZ = 0;

	bool prevTurnFlipped = false;

	system.beforeTurn();
	system.flip(system.bots[0]);
	system.afterTurn();

	while (status != END) {
		system.beforeTurn();

		int loopCnt = 0;
		while (true) {
			if (debug) {
				cerr << "## STATUS = " << status << endl;
				rep(id, 4) {
					cerr << " bot " << id << " (" << system.bots[id].pos.x << ", " << system.bots[id].pos.y << ", "
						 << system.bots[id].pos.z << ")" << endl;
				}
			}
			if (system.turn == 237) {
				int hoge = 1;
			}

			if (loopCnt++ == 10) {
				cout << "MUGEN LOOP!!" << endl;
				return 0;
			}

			if (status == FIRST_MOVE_UP) {
				int targetY = R;
				for (int y = R - 1; y >= 0; y--) {
					bool exist = false;
					rep(x, R) rep(z, R) {
						if (System::matrix[x][y][z]) {
							exist = true;
						}
					}
					if (exist) {
						break;
					}
					targetY = y;
				}

				if (system.bots[0].pos.y < targetY) {
					system.smove(system.bots[0], 1, min(15, targetY - system.bots[0].pos.y));
					break;
				} else {
					status = FISSION;
					continue;
				}
			}

			if (status == FISSION) {
				if (!system.bots[1].isActive) {
					system.fission(system.bots[0], Vector3D(1, 0, 0), 2);
				} else if (!system.bots[2].isActive) {
					system.wait(system.bots[0]);
					system.fission(system.bots[1], Vector3D(0, 0, 1), 1);
				} else if (!system.bots[3].isActive) {
					system.wait(system.bots[0]);
					system.wait(system.bots[1]);
					system.fission(system.bots[2], Vector3D(-1, 0, 0), 0);
				} else {
					status = STAND_BY;
					continue;
				}
				break;
			}

			if (status == STAND_BY) {
				int targetY = R;

				for (int y = R - 1; y >= 0; y--) {
					bool exist = false;
					REP(x, planeX, min(planeX + 30, R)) REP(z, planeZ, min(planeZ + 30, R)) {
							if (System::matrix[x][y][z]) {
								exist = true;
							}
						}
					if (exist) {
						break;
					}
					targetY = y;
				}

				targets[0] = Vector3D(planeX, targetY, planeZ);
				targets[1] = Vector3D(min(planeX + 29, R - 1), targetY, planeZ);
				targets[2] = Vector3D(min(planeX + 29, R - 1), targetY, min(planeZ + 29, R - 1));
				targets[3] = Vector3D(planeX, targetY, min(planeZ + 29, R - 1));

				bool moved = false;
				rep(id, 4) {
					Vector3D diff = targets[id] - system.bots[id].pos;
					if (diff.y)  {
						moved = true;
						system.smove(system.bots[id], 1, min(15, max(-15, diff.y)));
					} else if (diff.x) {
						moved = true;
						system.smove(system.bots[id], 0, min(15, max(-15, diff.x)));
					} else if (diff.z) {
						moved = true;
						system.smove(system.bots[id], 2, min(15, max(-15, diff.z)));
					} else {
						system.wait(system.bots[id]);
					}
				}
				if (!moved) {
					status = DIG;
				}
				break;
			}

			if (status == DIG) {
				if (system.bots[0].pos.y == 0) {
					planeX += 30;
					if (planeX >= R) {
						planeX = 0;
						planeZ += 30;
						if (planeZ >= R) {
							status = GO_BACK;
							continue;
						}
						planeZ = min(planeZ, R - 20);
					}
					planeX = min(planeX, R - 20);
					status = STAND_BY;
					continue;
				}
				bool exist = false;
				int y = system.bots[0].pos.y - 1;
				REP(x, system.bots[0].pos.x, system.bots[2].pos.x + 1) {
					REP(z, system.bots[0].pos.z, system.bots[2].pos.z + 1) {
						if (System::matrix[x][y][z]) {
							exist = true;
							break;
						}
					}
					if (exist) break;
				}
				if (exist) {
					Vector3D diff(0, -1, 0);
					system.GVoidPlane(system.bots[0], system.bots[2], system.bots[1], system.bots[3], diff, diff, diff, diff);
				} else {
					rep(id, 4) {
						system.smove(system.bots[id], 1, -1);
					}
				}
				break;
			}

			if (status == GO_BACK) {
				targets[0] = Vector3D(0, 0, 0);
				targets[1] = Vector3D(1, 0, 0);
				targets[2] = Vector3D(1, 0, 1);
				targets[3] = Vector3D(0, 0, 1);

				if (system.harmonics) {
					system.flip(system.bots[0]);
					REP(i, 1, 4) system.wait(system.bots[i]);
					break;
				}

				vector<bool> moved(4, false);
				rep(id, 4) {
					Vector3D diff = targets[id] - system.bots[id].pos;
					if (diff.x) {
						moved[id] = true;
						if (system.smoveResult(system.bots[id], 0, min(15, max(-15, diff.x))).first) {
							system.smove(system.bots[id], 0, min(15, max(-15, diff.x)));
						} else {
							system.wait(system.bots[id]);
						}
					} else if (diff.z) {
						moved[id] = true;
						if (system.smoveResult(system.bots[id], 2, min(15, max(-15, diff.z))).first) {
							system.smove(system.bots[id], 2, min(15, max(-15, diff.z)));
						} else {
							system.wait(system.bots[id]);
						}
					} else if (diff.y) {
						moved[id] = true;
						if (system.smoveResult(system.bots[id], 1, min(15, max(-15, diff.y))).first) {
							system.smove(system.bots[id], 1, min(15, max(-15, diff.y)));
						} else {
							system.wait(system.bots[id]);
						}
					}
				}
				int moveCnt = 0;
				rep(id, 4) moveCnt += moved[id];
				if (moveCnt) {
					rep(id, 4) {
						if (moved[id]) continue;
						system.wait(system.bots[id]);
					}
				} else {
					rep(id, 4) {
						system.wait(system.bots[id]);
					}
					status = END;
				}
				break;
			}
		}

		system.afterTurn();
	}

	system.beforeTurn();
	system.fusion(system.bots[0], system.bots[3]);
	system.fusion(system.bots[1], system.bots[2]);
	system.afterTurn();

	system.beforeTurn();
	system.fusion(system.bots[0], system.bots[1]);
	system.afterTurn();

	system.beforeTurn();
	system.halt();
	system.afterTurn();

	return 0;
}
