from pathlib import Path


def guid() -> str:
    import uuid
    return str(uuid.uuid4())


def make_dirs(path: Path):
    try:
        path.mkdir()
    except FileNotFoundError:
        make_dirs(path.parent)
        path.mkdir()
    except FileExistsError:
        pass


def clean_dir(path: Path):
    for p in path.glob('*'):
        if p.is_dir():
            clean_dir(p)
        else:
            p.unlink()
    path.rmdir()


def make_tmp_dir() -> Path:
    p = Path.home() / 'work' / guid()
    make_dirs(p)
    return p

