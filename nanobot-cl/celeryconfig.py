import boto3

_ssm = boto3.client('ssm')
access_key = _ssm.get_parameter(Name='access_key')['Parameter']['Value']
secret_key = _ssm.get_parameter(Name='secret_key', WithDecryption=True)['Parameter']['Value']
broker_url = 'sqs://%s:%s@' % (access_key, secret_key)
