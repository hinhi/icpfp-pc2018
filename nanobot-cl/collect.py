import re
from pathlib import Path
from collections import defaultdict
import json

import boto3

from const import *
from utils import clean_dir, make_dirs


s3 = boto3.resource('s3')
bucket = s3.Bucket(bucket_name)

nbt_p = re.compile(r'(F[ADR]{1,2}\d{3})/([^/]+)/(\d{14})_(\d+)\.nbt')
score_p = re.compile(r'(F[ADR]{1,2}\d{3})_([^_]+)_(\d{14})\.(json|png)')


def main():
    zip_dir = Path('../submit-tmp')
    if zip_dir.is_dir():
        clean_dir(zip_dir)
    make_dirs(zip_dir)
    tmp_dir = Path('../tmp-tmp')
    if tmp_dir.is_dir():
        clean_dir(tmp_dir)
    make_dirs(tmp_dir)

    keys = {}
    for o in bucket.objects.all():
        key = o.key
        m = nbt_p.match(key)
        if m:
            num, name, time, ene = m.groups()
            val = [name, int(ene), key, time, -1]
            keys[(num, name, time)] = val

    scores = {}
    for o in bucket.objects.all():
        key = o.key
        m = score_p.match(key)
        if m:
            num, name, time, ex = m.groups()
            if ex != 'json':
                scores[(num, name, time)] = None
                continue
            f = tmp_dir / key
            bucket.download_file(Key=key,
                                 Filename=str(f))
            try:
                ene = int(json.load(f.open())['energy'])
            except Exception:
                print('skip score', key)
                continue
            scores[(num, name, time)] = ene

    for key, ene in scores.items():
        if key in keys:
            keys[key][4] = ene

    def lam(k):
        _, ene1, _, _, ene2 = keys[k]
        if ene2 is None:
            return float('inf')
        if ene2 != -1:
            return ene2
        return ene1

    for key in sorted(keys, key=lam):
        num, _, _ = key
        name, ene1, key, time, ene2 = keys[key]
        f = zip_dir / (num + '.nbt')
        if f.exists():
            continue
        bucket.download_file(Key=key,
                             Filename=str(f))
        print(num, name, time, ene1, ene2)

    for n in range(1, problem_num['R'] + 1):
        a = zip_dir / f'FRA{n:03}.nbt'
        d = zip_dir / f'FRD{n:03}.nbt'
        r = zip_dir / f'FR{n:03}.nbt'
        if a.exists() and d.exists():
            with r.open('wb') as rf:
                with a.open('rb') as af:
                    with d.open('rb') as df:
                        data = df.read()
                        data = data[:-1] + af.read()
                        rf.write(data)
        a.unlink()
        d.unlink()
        print(r, a, d)

    for typ, n in problem_num.items():
        for i in range(1, n + 1):
            num = zip_dir / f'F{typ}{i:03}.nbt'
            if num.exists():
                continue
            src = Path('../dfltTracesF', f'F{typ}{i:03}.nbt')
            dst = zip_dir / f'F{typ}{i:03}.nbt'
            with src.open('rb') as i:
                with dst.open('wb') as o:
                    o.write(i.read())
            print('デフォルトで補完', num)


if __name__ == '__main__':
    main()
