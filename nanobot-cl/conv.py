import struct


class BinaryConverter(object):

    cmd_direction = {
        'x': '01',
        'y': '10',
        'z': '11',
    }

    def get_coordinate_diff(self, x, y, z):
        assert -1 <= x <= 1
        assert -1 <= y <= 1
        assert -1 <= z <= 1
        return '{0:05b}'.format((x + 1) * 9 + (y + 1) * 3 + (z + 1))

    def get_short_liner_coordinate_diff(self, value):
        assert -5 <= value <= 5
        return '{0:04b}'.format(value + 5)

    def get_long_liner_coordinate_diff(self, value):
        assert -15 <= value <= 15
        return '{0:05b}'.format(value + 15)

    def get_direction(self, x, y, z):
        k, tv = '', 0
        if x != 0:
            k += 'x'
            tv = x
        if y != 0:
            k += 'y'
            tv = y
        if z != 0:
            k += 'z'
            tv = z
        assert len(k) == 1
        return [k, tv]

    def to_uchar(self, bin_str):
        return struct.pack('B', int(bin_str, 2))

    def convert(self, inf, outf):

        def get_line():
            return inf.readline().strip()

        while True:
            line = get_line()
            # End Of File = Error
            if not line:
                return 99999999999999999999

            turn, bot_num = line.split()
            if turn.lower() == 'end':
                return int(bot_num)
            bot_num = int(bot_num)
            ts = [b''] * bot_num
            for _ in range(bot_num):
                fragments = get_line().split()
                command = fragments[0].lower()
                bid = int(fragments[1]) - 1
                method = getattr(self, command)
                bins = method(fragments[2:])
                ts[bid] = b''.join(self.to_uchar(b) for b in bins)
            outf.write(b''.join(ts))

    def halt(self, _):
        return ['11111111']

    def wait(self, _):
        return ['11111110']

    def flip(self, _):
        return ['11111101']

    def smove(self, fragments):
        x, y, z = map(int, fragments)
        k, v = self.get_direction(x, y, z)
        return [
            '00%s0100' % self.cmd_direction[k],
            '000%s' % self.get_long_liner_coordinate_diff(v)
        ]

    def lmove(self, fragments):
        x1, y1, z1, x2, y2, z2 = map(int, fragments)
        k1, v1 = self.get_direction(x1, y1, z1)
        k2, v2 = self.get_direction(x2, y2, z2)
        return [
            '%s%s1100' % (self.cmd_direction[k1], self.cmd_direction[k2]),
            '%s%s' % (self.get_short_liner_coordinate_diff(v1),
                      self.get_short_liner_coordinate_diff(v2)),
        ]

    def fusionp(self, fragments):
        x, y, z = map(int, fragments)
        return ['%s111' % self.get_coordinate_diff(x, y, z)]

    def fusions(self, fragments):
        x, y, z = map(int, fragments)
        return ['%s110' % self.get_coordinate_diff(x, y, z)]

    def fission(self, fragments):
        x, y, z, m = map(int, fragments)
        return [
            '%s101' % self.get_coordinate_diff(x, y, z),
            '{0:08b}'.format(m),
        ]

    def fill(self, fragments):
        x, y, z = map(int, fragments)
        return ['%s011' % self.get_coordinate_diff(x, y, z)]

    def void(self, fragments):
        x, y, z = map(int, fragments)
        return ['%s010' % self.get_coordinate_diff(x, y, z)]

    def gfill(self, fragments):
        x1, y1, z1, x2, y2, z2 = map(int, fragments)
        assert -30 <= x2 <= 30
        assert -30 <= y2 <= 30
        assert -30 <= z2 <= 30
        return [
            '%s001' % self.get_coordinate_diff(x1, y1, z1),
            '{:08b}'.format(x2 + 30),
            '{:08b}'.format(y2 + 30),
            '{:08b}'.format(z2 + 30),
        ]

    def gvoid(self, fragments):
        x1, y1, z1, x2, y2, z2 = map(int, fragments)
        assert -30 <= x2 <= 30
        assert -30 <= y2 <= 30
        assert -30 <= z2 <= 30
        return [
            '%s000' % self.get_coordinate_diff(x1, y1, z1),
            '{:08b}'.format(x2 + 30),
            '{:08b}'.format(y2 + 30),
            '{:08b}'.format(z2 + 30),
        ]
