#!/bin/bash

bin=/home/ec2-user/.local/bin
sufix=.ap-northeast-1.compute.amazonaws.com

workers="ec2-13-112-108-202 ec2-13-113-191-101 ec2-13-114-140-254 ec2-13-231-134-88 ec2-13-231-236-40 ec2-18-179-30-101 ec2-18-179-45-223 ec2-52-68-160-99 ec2-52-196-243-65 ec2-176-34-0-112"
webs="ec2-52-69-100-161"

for i in $webs
do
    echo
    echo "#### $i ####"
    scp -i daiju.pem *.py *.ini ec2-user@$i$sufix:nanobot-cl/
    ssh -i daiju.pem ec2-user@$i$sufix "cd nanobot-cl && $bin/uwsgi --ini web.ini"
done

for i in $workers
do
    echo
    echo "#### $i ####"
    scp -i daiju.pem *.py *.sh ec2-user@$i$sufix:nanobot-cl/
    ssh -i daiju.pem ec2-user@$i$sufix "cd nanobot-cl && ./worker.sh"
done
