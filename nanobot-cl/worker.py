from zipfile import ZipFile
import subprocess
import time
import io
import json
import traceback

from celery import Celery
import boto3
import botocore.exceptions

from const import *
from utils import *
from conv import BinaryConverter


app = Celery('chirimenjako-worker')
app.config_from_object('celeryconfig')
s3 = boto3.resource('s3')


@app.task()
def submit_all(task_guid, typ, name, t):
    for i in range(1, problem_num[typ[0]] + 1):
        calc.apply_async(args=(task_guid, typ, i, name, t))


def get_problem(tmp, typ, num):
    if typ == 'A':
        names = ['FA%3.3i_tgt.mdl' % num]
    elif typ == 'D':
        names = ['FD%3.3i_src.mdl' % num]
    elif typ == 'RA':
        names = ['FR%3.3i_tgt.mdl' % num]
    elif typ == 'RD':
        names = ['FR%3.3i_src.mdl' % num]
    else:
        names = ['FR%3.3i_src.mdl' % num, 'FR%3.3i_tgt.mdl' % num]
    for name in names:
        with (tmp / name).open('wb') as o:
            with Path(Path.home(), problem_root, name).open('rb') as i:
                o.write(i.read())
    return [str(tmp / n) for n in names]


@app.task()
def calc(task_guid, typ, num, name, t):
    tmp = make_tmp_dir()
    result_meta = {
        'task_guid': task_guid,
        'type': typ,
        'number': num,
        'name': name,
        'time': t,
    }
    try:
        b = s3.Bucket(bucket_name)
        zip_path = tmp / 'submit.zip'
        try:
            b.download_file(Key=f'{task_guid}/submit.zip',
                            Filename=str(zip_path))
        except botocore.exceptions.ClientError as e:
            result_meta['error'] = str(e)
            return

        with ZipFile(zip_path) as f:
            f.extractall(path=tmp)

        csh = list(tmp.glob('**/compile.sh'))
        if len(csh) != 1:
            result_meta['detail'] = 'zip に compile.sh が含まれていません: %s' % csh
            return
        csh = csh[0]
        csh.chmod(0o755)

        t1 = time.time()
        cp = subprocess.Popen(['./compile.sh'], cwd=csh.parent,
                              stderr=subprocess.PIPE,
                              stdout=subprocess.PIPE)
        try:
            stdout, stderr = cp.communicate(timeout=30)
        except subprocess.TimeoutExpired:
            cp.kill()
            stdout, stderr = cp.communicate()
        t2 = time.time()
        result_meta['compile_status'] = cp.returncode
        result_meta['compile_stdout'] = stdout.decode('utf8')
        result_meta['compile_stderr'] = stderr.decode('utf8')
        result_meta['compile_time'] = t2 - t1
        if cp.returncode != 0:
            return

        names = get_problem(tmp, typ, num)

        rsh = list(tmp.glob('**/run.sh'))
        if len(rsh) != 1:
            result_meta['detail'] = 'zip に run.sh が含まれていません: %s' % rsh
            return
        rsh = rsh[0]
        rsh.chmod(0o755)

        output = tmp / guid()

        t1 = time.time()
        rp = subprocess.Popen(['./run.sh'] + names + [str(output)], cwd=rsh.parent,
                              stderr=subprocess.PIPE)
        try:
            _, stderr = rp.communicate(timeout=60 * 10)
        except subprocess.TimeoutExpired:
            rp.kill()
            _, stderr = rp.communicate()
        t2 = time.time()
        result_meta['run_status'] = rp.returncode
        result_meta['run_error'] = stderr.decode('utf8')
        result_meta['run_time'] = t2 - t1

        if rp.returncode != 0:
            return

        nbt = tmp / guid()
        try:
            with output.open() as inf:
                with nbt.open('wb') as outf:
                    e = BinaryConverter().convert(inf, outf)
        except Exception as e:
            result_meta['convert'] = str(e)
            result_meta['convert_tb'] = traceback.format_exc()
            return

        result_meta['e'] = e

        try:
            b.upload_file(Filename=str(nbt),
                          Key=f'F{typ}{num:03}/{name}/{t}_{e}.nbt')
        except botocore.exceptions.ClientError as e:
            result_meta['upload_file_convert'] = str(e)
            return
    finally:
        clean_dir(tmp)
        try:
            b = s3.Bucket(bucket_name)
            data = json.dumps(result_meta, ensure_ascii=False).encode('utf8')
            b.upload_fileobj(io.BytesIO(data),
                             Key=f'{task_guid}/F{typ}{num:03}.json')
        except botocore.exceptions.ClientError as e:
            print(e)
            return
