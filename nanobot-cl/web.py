from datetime import datetime

from flask import Flask, jsonify
import boto3
import botocore.exceptions

from const import *
from utils import *


app = Flask('chirimenjako-front')
s3 = boto3.resource('s3')


@app.route('/')
def hello():
    return "Hello World!"


@app.route('/submit', methods=['POST'])
def submit():
    from flask import request
    from worker import submit_all

    if len(request.files) != 1:
        return jsonify({
            'status': 'bad',
            'detail': 'ファイルを添付してください: %s' % request.files,
        })
    filename = list(request.files)[0]
    f = request.files[filename]
    if f.content_type != 'application/zip':
        return jsonify({
            'status': 'bad',
            'detail': 'zipのみ: %s' % f.content_type,
        })
    typ = request.args.get('type')
    if typ not in ['A', 'R', 'D', 'RA', 'RD']:
        return jsonify({
            'status': 'bad',
            'detail': 'type を A,R,D,RA,RD で指定',
        })
    name = request.args.get('name')
    if not name:
        return jsonify({
            'status': 'bad',
            'detail': 'name は必須',
        })

    task_guid = guid()
    bucket = s3.Bucket(bucket_name)
    key = f'{task_guid}/submit.zip'
    try:
        bucket.Object(key).upload_fileobj(f)
    except botocore.exceptions.ClientError as e:
        return jsonify({
            'status': 'error',
            'detail': str(e),
        })

    t = f'{datetime.now():%Y%m%d%H%M%S}'
    submit_all.apply_async(args=(task_guid, typ, name, t))

    return jsonify({
        'status': 'success',
        'task_guid': task_guid,
        'time': t,
    })


if __name__ == '__main__':
    app.run(port='18120')
