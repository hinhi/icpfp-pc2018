
#include "../lib/system.h"

using namespace std;

typedef pair<int,int> P;

class AI {

public:
	int R, N;
	int LX, RX, LY, RY, LZ, RZ;
	System system;

	vector<P> borderX;
	vector<P> borderZ;
	vector<Vector3D> target;
	vector<int> end;

	AI(int R, int N) : system(R, N, 0), R(R), N(N), target(N+1, Vector3D(-1,-1,-1)), end(N+1, 0) {
	};

	void fill(Bot &bot, Vector3D t) {
		auto nd = Vector3D(- bot.pos.x + t.x, - bot.pos.y + t.y, - bot.pos.z + t.z);
		int res = system.canFill(bot.pos, nd, bot.id);
		if (res == 1) {
			system.fill(bot, nd);
		}else 
		if (res == 0){ 
			system.wait(bot); 
		} 
		else if (res == 2) { 
			system.flip(bot); 
		}
	}

	void move(Bot &bot, Vector3D t) {

		int dir, dist;
		if(bot.pos.x != t.x) {
			int d = t.x - bot.pos.x;
			dir = 0;
			dist = (d > 0 ? min(15, d) : max(-15, d));
		} else
		if(bot.pos.y != t.y) {
			int d = t.y - bot.pos.y;
			dir = 1;
			dist = (d > 0 ? min(15, d) : max(-15, d));
		} else
		if(bot.pos.z != t.z) {
			int d = t.z - bot.pos.z;
			dir = 2;
			dist = (d > 0 ? min(15, d) : max(-15, d));
		} else assert(false);

		if (system.smoveResult(bot, dir, dist).first) {
			system.smove(bot, dir, dist);
		} else {
			system.wait(bot);
		}
	}

	Vector3D nearTarget(Bot &bot) {

		int xl = borderX[bot.id].first;
		int xr = borderX[bot.id].second;

		int zl = borderZ[bot.id].first;
		int zr = borderZ[bot.id].second;

		int mi = 1e9;
		Vector3D vec;

		for (int x = xl; x < xr; x++) {
			int y = bot.pos.y - 1;
			if (y == -1) {
				break;
			}
			for (int z = zl; z < zr; z++) {
				if (system.model[x][y][z] && !system.matrix[x][y][z]) {
					int dist = abs(bot.pos.x - x) + abs(bot.pos.y - y) + abs(bot.pos.z - z);
					if (mi > dist) {
						mi = dist;
						vec = Vector3D(x, y, z);
					}
				}
			}
		}

		return vec;

	}

	int think(Bot &bot) {


		int lx = borderX[bot.id].first;
		int rx = borderX[bot.id].second;

		int lz = borderZ[bot.id].first;
		int rz = borderZ[bot.id].second;

		bool ye = true;
		for(int x = lx; x < rx; x++) {
			int y = bot.pos.y - 1;
			if (y == -1) {
				break;
			}
			for(int z = lz; z < rz; z++) {
				if(system.model[x][y][z] && !system.matrix[x][y][z]) {
					ye = false;
					break;
				}
			}
		}

		bool top = bot.pos.y == RY;

		if (ye && top) {
			Vector3D target = Vector3D(bot.id-1, RY, 0);
			if (bot.pos == target) {
				end[bot.id] = 1;
				system.wait(bot);
			} else {
				move(bot, target);
			}
		} else if (bot.pos.x < lx) {
			int dir = 0, dist = min(15, lx - bot.pos.x);
			if (system.smoveResult(bot, dir, dist).first) {
				system.smove(bot, dir, dist);
			} else {system.wait(bot);}
		} else if (bot.pos.z < lz) {
			int dir = 2, dist = min(15, lz - bot.pos.z);
			if (system.smoveResult(bot, dir, dist).first) {
				system.smove(bot, dir, dist);
			}
			else { system.wait(bot); }
		} else if(bot.seeds.size() >= 1){
			system.fission(bot, Vector3D(bot.id % 2 ? 1 : 0, 0, bot.id % 2 ? 0 : 1), int(bot.seeds.size())-1);
		} else if (ye) {
			if (system.smoveResult(bot, 1, 1).first) {
				system.smove(bot, 1, 1);
			} else {system.wait(bot);}
		} else {
			if (target[bot.id].x == -1) {
				target[bot.id] = nearTarget(bot);
			}
			if (system.isNear(Vector3D(target[bot.id].x - bot.pos.x, target[bot.id].y - bot.pos.y,
									   target[bot.id].z - bot.pos.z))) {
				fill(bot, target[bot.id]);
				target[bot.id] = Vector3D(-1, -1, -1);
			} else {
				move(bot, Vector3D(target[bot.id].x, target[bot.id].y + 1, target[bot.id].z));
			}
		}


		return 0;
	}

	void fusionEndBot(vector<int> &bid) {
		vector<int> f(N, 0);
		each(i, bid) if(f[i] == 0 && end[i]){
			each(j, bid) if (f[j] == 0 && end[j] && i < j) {
				if (system.mlen(Vector3D(system.bots[i].pos.x - system.bots[j].pos.x, system.bots[i].pos.y - system.bots[j].pos.y, system.bots[i].pos.z - system.bots[j].pos.z)) == 1) {
					f[i] = 1;
					f[j] = 2;
					system.fusion(system.bots[i], system.bots[j]);
				}
			}
		}
		
		rep(i, f.size()){
			if(f[i] == 2) {
				rep(j, bid.size()) {
					if(bid[j] == i) {
						bid.erase(bid.begin() + j);
						break;
					}
				}
			}
		}
	}

	bool standbyHalt() {



		vector<int> bid;
		rep(i, system.bots.size()) {
			Bot &bot = system.bots[i];
			if (bot.isActive) bid.push_back(bot.id-1);
		}

		if (bid.size() == 1) {
			if (system.bots[0].pos == Vector3D(0, 0, 0)) {
				return 1;
			}
			else {
				system.smove(system.bots[0], 1, max(-15, -system.bots[0].pos.y));
				return 0;
			}
		}

		vector<int> f(N, 0);
		int count = 0;
		each(i, bid) if(f[i] == 0){
			each(j, bid) if (f[j] == 0 && i < j) {
				if (system.mlen(Vector3D(system.bots[i].pos.x - system.bots[j].pos.x, system.bots[i].pos.y - system.bots[j].pos.y, system.bots[i].pos.z - system.bots[j].pos.z)) == 1) {
					f[i] = 1;
					f[j] = 1;
					system.fusion(system.bots[i], system.bots[j]);
				}
			}
		}

		each(i, bid) if (f[i] == 0) {
			if (i == 0) {
				if (system.harmonics) {
					system.flip(system.bots[i]);
				}
				else {
					system.wait(system.bots[i]);
				}
			}
			else {
				system.smove(system.bots[i], 0, -1);
			}
		}

		return 0;
	}

	void run() {

		LY = LX = LZ = 1e9;
		RY = RX = RZ = 0;
		rep(x, R) rep(y, R) rep(z, R) {
			if (system.model[x][y][z]) {
				LY = min(LY, y);
				RY = max(RY, y + 1);
				LX = min(LX, x);
				RX = max(RX, x + 1);
				LZ = min(LZ, z);
				RZ = max(RZ, z + 1);
			}
		}

		int xl = (RX - LX) / (N/2);
		int xc = (RX - LX) % (N/2);

		borderX.push_back(P());
		int rx = LX;
		rep(i, N/2) {
			if (xc > 0) {
				borderX.push_back(P(rx, min(rx + xl + 1, RX)));
				borderX.push_back(P(rx, min(rx + xl + 1, RX)));
				xc--;
				rx += xl + 1;
			} else {
				borderX.push_back(P(rx, min(rx + xl, RX)));
				borderX.push_back(P(rx, min(rx + xl, RX)));
				rx += xl;
			}
		}

		int zl = (RZ - LZ) / 2;

		borderZ.push_back(P());
		rep(i, N) {
			if (i % 2 == 0) {
				borderZ.push_back(P(LZ, LZ + zl));
			}
			else {
				borderZ.push_back(P(LZ + zl, RZ));
			}
		}

		int lastFlipTurn = 0;

		while(true) {
			system.beforeTurn();
			vector<int> bid;
			rep(i, system.bots.size()) {
				Bot &bot = system.bots[i];
				if (bot.isActive) bid.push_back(bot.id);
			}
			if (system.harmonics && lastFlipTurn + 5 < system.turn) {
				if (system.notGrounded.empty()) {
					lastFlipTurn = system.turn;
					each(i, bid) {
						Bot &bot = system.bots[i - 1];
						if (bid[0] == i) {
							system.flip(bot);
						}
						else {
							system.wait(bot);
						}
					}
					system.afterTurn();
					continue;
				}
			}

			int count = 0;
			each(i, bid) {
				if (end[i]) count++;
			}

			if (count == bid.size()) {
				if (standbyHalt()) break;
			}
			else {
				each(i, bid) {
					think(system.bots[i - 1]);
				}
			}

			system.afterTurn();
		}
		
		system.beforeTurn();
		system.halt();
		system.afterTurn();

	}

};

int main(int argc, char *argv[]) {
	string filename = "C:/Users/100556/Documents/icpfp-pc2018/problemsF/FA004_tgt.mdl";
	if (argc == 2) {
		filename = string(argv[1]);
	}
	int R = read_mdl(filename.c_str(), 0);
	int N = 40;
	N = min(N, R);

	AI(R, N).run();

	return 0;
}
