# -*- coding: utf-8 -*-
from selenium import webdriver
from collections import OrderedDict
from const import problem_info

import boto3
import json
import re
import math
import time
from pathlib import Path


class ScoringBase(object):
    score_base_json = './score_base.json'
    scores_json = './score.json'
    standing_json = './standing.json'

    def calc_score(self, r, ed, eb, es):
        return max(0, int(int(math.log2(r)) * 1000 * (ed - es) / (ed - eb)))

    def valid_start_pattern(self, elem):
        for p in ['FRA', 'FRD', 'RA', 'RD']:
            if elem.startswith(p):
                return True
        return None

    def write_score(self):
        base_score, best_score = dict(), dict()
        with open(self.score_base_json, 'r') as f:
            for d in json.load(f):
                base_score[d['problem_id']] = {
                    'energy': d['energy'],
                    # 'R': d['R'],
                }
        with open(self.standing_json, 'r') as f:
            standing_json_data = json.load(f)
            del standing_json_data['total']

            for d in standing_json_data:
                if self.valid_start_pattern(d):
                    score, energy = 0, 0
                    for e in standing_json_data[d]:
                        if score <= e['total_score']:
                            score, energy = e['total_score'], e['total_energy']
                    best_score[d] = {
                        'energy': energy,
                    }


        # scores.jsonに書き出し
        old_json_list = None
        with open(self.scores_json, 'r') as f:
            old_json_list = json.load(f)
        if not old_json_list:
            old_json_list = []
        file_list = self.aws_load(old_json_list, best_score.keys())

        append_dates = []
        for file in file_list:
            with open(file, 'r') as f:
                m = re.search(
                    r'.*\/(?P<problem_id>\w{2,3}\d{3})\/(?P<user_name>\w+)\/('
                    r'?P<submit_time>\d+)\.nbt', file)
                problem_id = m.group('problem_id')
                user_name = m.group('user_name')
                submit_time = m.group('submit_time')

                sc = ScoreCrowlar(problem_id)
                t_score = sc.get_score(file)

                if problem_id[2].isdigit():
                    pn = problem_id
                else:
                    pn = 'FR' + problem_id[3:]
                r = problem_info[pn]['R']
                eb = 0
                if base_score.get(pn):
                    ed = base_score[pn].get('energy', 0)
                score = 0
                if False:
                    score = self.calc_score(r, ed, eb, t_score)
                append_dates.append(
                    {
                        'problem_id': problem_id,
                        'name': user_name,
                        'submit_time': submit_time,
                        'energy': t_score,
                        'score': score
                    }
                )

                old_json_list.extend(append_dates)
                with open(self.scores_json, 'w') as f:
                    json.dump(old_json_list, f, indent=4)
                # 書き込みが激しいため
                self.send_scores_json_to_aws()

    def aws_load(self, old_json_list, key_filter):
        # 最近更新されたものだけ処理する
        created_list = []
        key_filter = set([k for k in key_filter])
        for d in old_json_list:
            problem_id = d['problem_id']
            user_name = d['name']
            submit_time = d['submit_time']
            t = '%s/%s/%s.nbt' % (problem_id, user_name, submit_time)
            created_list.append(t)

        ret_list = []
        s3 = boto3.resource('s3')
        bucket = s3.Bucket('chirimenjako-2018')
        # ちょっと後で直す
        FILENAME_PATTERN = re.compile('(FR[AD]\d\d\d)/(.+)/(\d{14}).nbt')
        R_FILENAME_PATTERN = re.compile('(R[AD]\d\d\d)/(.+)/(\d{14}).nbt')
        for obj in bucket.objects.all():
            m = FILENAME_PATTERN.match(obj.key)
            if not m:
                tm = R_FILENAME_PATTERN.match(obj.key)
                if tm:
                    m = tm
            if m:
                if obj.key not in created_list:
                    problem_id = obj.key.split('/')[0]
                    # if not (problem_id in key_filter):
                    #     continue
                    filename = './tmp/' + obj.key
                    dir = '/'.join([elem for elem in filename.split('/')[:-1]])
                    Path(dir).mkdir(parents=True, exist_ok=True)
                    if not Path(filename).exists():
                        bucket.download_file(Key=obj.key, Filename=filename)
                        print('Download file:%s' % obj.key)
                    ret_list.append(filename)

        return ret_list

    def send_scores_json_to_aws(self):
        s3 = boto3.resource('s3')
        b = s3.Bucket('chirimenjako-2018')
        b.upload_file(self.scores_json, 'scores.json')


class ScoreCrowlar(object):

    url = 'https://icfpcontest2018.github.io/full/exec-trace-novis.html'
    problem_path_dir = '/home/sugihara/icfp-pc2018/problemsF/'
    model_file_validate_ext = ['.mdl', ]
    trace_file_validate_ext = ['.nbt', ]
    responce_list = ['energy', ]

    def __init__(self):
        pass

    def init(self, problem_id):
        self.problem_id = problem_id
        # FR[DA]
        separator = 3
        if problem_id[2].isdigit():
            # F[DA]
            separator = 2
        self.problem_type = problem_id[:separator]
        self.problem_number = problem_id[separator:]

    def open_browser(self):
        if getattr(self, 'browser', None):
            self.browser.quit()
            raise Exception('Multi browser open !!')

        #self.browser = webdriver.PhantomJS(executable_path='./phantomjs')
        self.browser = webdriver.Chrome(
            '/usr/lib/chromium-browser/chromedriver')
        self.browser.get(self.url)

    def close_browser(self):
        if not getattr(self, 'browser', None):
            raise Exception('No browser open !!')
        self.browser.quit()

    def validate_file_ext(self, filepath, validate_list):
        for valid in validate_list:
            if valid.endswith(valid):
                return
        raise Exception('Invalid File extention:%s' % filepath)

    def select_check_box(self, _id):
        self.browser.find_element_by_id(_id).click()

    def set_src_model_empty(self, flag):
        if flag:
            self.select_check_box('srcModelEmpty')

    def set_tgt_model_empty(self, flag):
        if flag:
            self.select_check_box('tgtModelEmpty')

    def set_file(self, _id, filepath):
        self.browser.find_element_by_id(_id).send_keys(filepath)

    def set_src_model_file_in(self, filepath):
        if not filepath:
            self.set_src_model_empty(True)
        else:
            self.validate_file_ext(filepath, self.model_file_validate_ext)
            self.set_file('srcModelFileIn', filepath)

    def set_tgt_model_file_in(self, filepath: str):
        if not filepath:
            self.set_tgt_model_empty(True)
        else:
            self.validate_file_ext(filepath, self.model_file_validate_ext)
            self.set_file('tgtModelFileIn', filepath)

    def set_trace_file_in(self, filepath):
        self.validate_file_ext(filepath, self.trace_file_validate_ext)
        self.set_file('traceFileIn', filepath)

    def submit(self):
        self.browser.find_element_by_id("execTrace").click()

    def get_result(self):
        return self.browser.find_element_by_id("stdout").text


    def get_score(self, trace_file_path, error_filename):
        ret = None
        try:
            model_file_path = None
            target_file_path = None
            if self.problem_type == 'FA':
                target_file_path = self.problem_path_dir + '%s_tgt.mdl' % self.problem_id
            elif self.problem_type == 'FD':
                model_file_path = self.problem_path_dir + '%s_src.mdl' % self.problem_id
            elif self.problem_type == 'FR':
                model_file_path = self.problem_path_dir + '%s_src.mdl' % self.problem_id
                target_file_path = self.problem_path_dir + '%s_tgt.mdl' % self.problem_id
            elif self.problem_type == 'FRA':
                target_file_path = self.problem_path_dir + '%s_tgt.mdl' % (
                        'FR' + self.problem_number)
            elif self.problem_type == 'FRD':
                model_file_path = self.problem_path_dir + '%s_src.mdl' % (
                        'FR' + self.problem_number)
            try:
                self.set_src_model_file_in(model_file_path)
                self.set_tgt_model_file_in(target_file_path)
                self.set_trace_file_in(trace_file_path)
                # アップロードを待つ
                time.sleep(2)

                self.submit()
                count = 0
                # 成功するまで待ち見続ける必要がある
                while count <= 50:
                    time.sleep(2)
                    result = self.get_result()
                    if "Success" in result:
                        break
                    if "Failure" in result:
                        self.save_screenshot(error_filename)
                        break
                    count += 1
                if "Success" in result:
                    response_content = OrderedDict({})
                    # raw data
                    # Success::
                    # Time: 2978809
                    # Commands: 2978809
                    # Energy: 301604296762512
                    # ClockTime: 68328 ms
                    for line in result.split('\n'):
                        contents = [c.lower().strip() for c in line.split(':')]
                        if len(contents) == 2:
                            if contents[0] in self.responce_list and contents[
                                1].isdigit():
                                ret = contents[1]
            except Exception as e:
                print(e)
                self.save_screenshot(error_filename)
        except Exception as e:
            print(e)
            self.save_screenshot(error_filename)

        self.browser.get(self.url)
        if ret is not None:
            return int(ret)

    def save_screenshot(self, filename):
        self.browser.save_screenshot(filename)


if __name__ == '__main__':
    import sys

    sb = ScoringBase()
    sb.write_score()
