from selenium import webdriver
from collections import OrderedDict
import json

class Crawler(object):
    model_file_validate_ext = ['.mdl', ]
    trace_file_validate_ext = ['.nbt', ]

    def open_browser(self, url):
        if getattr(self, 'browser', None):
            self.browser.quit()
            raise Exception('Multi browser open !!')

        if url is not None:
            self.browser = webdriver.PhantomJS(executable_path='./phantomjs')
            self.browser.get(url)
        else:
            raise Exception('Url Not Found !!')

    def close_browser(self):
        if not getattr(self, 'browser', None):
            raise Exception('No browser open !!')
        self.browser.quit()

    def validate_file_ext(self, filepath, validate_list):
        for valid in validate_list:
            if valid.endswith(valid):
                return
        raise Exception('Invalid File extention:%s' % filepath)

    def select_check_box(self, _id):
        self.browser.find_element_by_id(_id).click()

    def set_src_model_empty(self, flag):
        if flag:
            self.select_check_box('srcModelEmpty')

    def set_tgt_model_empty(self, flag):
        if flag:
            self.select_check_box('tgtModelEmpty')

    def set_file(self, _id, filepath):
        self.browser.find_element_by_id(_id).send_keys(filepath)

    def set_src_model_file_in(self, filepath):
        if not filepath:
            self.set_src_model_empty(True)
        else:
            self.validate_file_ext(filepath, self.model_file_validate_ext)
            self.set_file('srcModelFileIn', filepath)

    def set_tgt_model_file_in(self, filepath):
        if not filepath:
            self.set_tgt_model_empty(True)
        else:
            self.validate_file_ext(filepath, self.model_file_validate_ext)
            self.set_file('tgtModelFileIn', filepath)

    def set_trace_file_in(self, filepath: str):
        self.validate_file_ext(filepath, self.trace_file_validate_ext)
        self.set_file('traceFileIn', filepath)

    def submit(self):
        self.browser.find_element_by_id("execTrace").click()

    def get_result(self):
        return self.browser.find_element_by_id("stdout").text


def submit_trace(model_file_path='', target_file_path='',
                 trace_file_path='', username='default'):
    if not model_file_path and not target_file_path:
        return 'failed: both model file and target file are not exists!'
    if not trace_file_path:
        return 'failed: trace file is not exists!'

    import time
    import re
    task_info = re.search(r'.*(?P<name>\w{2})(?P<number>\d{3}).+',
                          trace_file_path)
    task_name, task_number = 'task_name', '000'
    try:
        task_name, task_number = task_info.group('name'), task_info.group(
            'number')
    except:
        print('task_name parse error')
    task_key = task_name + task_number
    result = 'failed:%s' % trace_file_path
    responce_list = ['energy', 'time']

    try:
        url = 'https://icfpcontest2018.github.io/full/exec-trace-novis.html'
        cw = Crawler()
        cw.open_browser(url)

        cw.set_src_model_file_in(model_file_path)
        cw.set_tgt_model_file_in(target_file_path)
        cw.set_trace_file_in(trace_file_path)
        time.sleep(2)

        cw.submit()
        count = 0
        while count <= 1000:
            time.sleep(2)
            result = cw.get_result()
            if "Success" in result:
                break
            if "Failure" in result:
                break
            count += 1
        response = OrderedDict({})
        if "Success" in result:
            response_content = OrderedDict({})
            # raw data
            # Success::
            # Time: 2978809
            # Commands: 2978809
            # Energy: 301604296762512
            # ClockTime: 68328 ms
            for line in result.split('\n'):
                contents = [c.lower().strip() for c in line.split(':')]
                if len(contents) == 2:
                    if contents[0] in responce_list and contents[1].isdigit():
                        response_content[contents[0]] = contents[1]
            response[task_key] = {
                username: response_content
            }
        result = json.dumps(response)
        with open('result.txt', 'a') as f:
            print(result, file=f)
        cw.close_browser()
    except Exception as e:
        print(e)
    return result


def get_args():
    import argparse
    parser = argparse.ArgumentParser(description='計算投げつけスクリプト')
    parser.add_argument('-m', '--model_file_path',
                        default=None, help='source model(create game)')
    parser.add_argument('-t', '--target_file_path',
                        default=None, help='target model(delete game)')
    parser.add_argument('-T', '--trace_file_path',
                        default=None, help='trace file(command file)')
    parser.add_argument('-u', '--username', default='default', help='user name')
    return vars(parser.parse_args())


if __name__ == '__main__':
    settings = get_args()
    print(submit_trace(**settings))
