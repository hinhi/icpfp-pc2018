import json
from collections import defaultdict

def start_matching():
    score_base_json = './score_base.json'
    best_match_json = './best_match.json'
    best_match = dict()
    with open(score_base_json, 'r') as f:
        contents = json.load(f)
        create_data_map = defaultdict(list)
        delete_data_map = defaultdict(list)
        for d in contents:
            problem_name = d['problem_id']
            problem_id = problem_name[2:]
            if 'RA' in problem_name:
                create_data_map[problem_id].append({
                    'name' : d['name'],
                    'submit_time': d['submit_time'],
                    'energy': d['energy']
                })
            if 'RD' in problem_name:
                delete_data_map[problem_id].append({
                    'name' : d['name'],
                    'submit_time': d['submit_time'],
                    'energy': d['energy']
                })
        for ck in create_data_map.keys():
            clist = create_data_map[ck]
            dlist = delete_data_map[ck]
            c = min(clist, lambda x:x['energy'])
            d = min(dlist, lambda x:x['energy'])
            best_match[ck] = {
                'RA' + ck : c,
                'RD' + ck : d,
            }
    with o
    return best_match

if __name__ == '__main__':
    start_matching()