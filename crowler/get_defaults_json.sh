#!/bin/bash

set -eu

PROGF_MODEL_FILEPATH='../problemsF'
PROGF_TRACE_FILEPATH='../dfltTracesF'
PROGF_SRC_PRE='FR'
PROGF_MODEL_PRE='FR'
PROGF_TRACE_PRE='FR'
PROGF_SRC_EXT='_src.mdl'
PROGF_MODEL_EXT='_tgt.mdl'
PROGF_TRACE_EXT='.nbt'

for i in 005 009 014 021 024 029 047 048 058 063 067 078 083 085 087 100; do
    python score_scraper.py -m ${PROGF_MODEL_FILEPATH}/${PROGF_SRC_PRE}${i}${PROGF_SRC_EXT} -t ${PROGF_MODEL_FILEPATH}/${PROGF_MODEL_PRE}${i}${PROGF_MODEL_EXT} -T ${PROGF_TRACE_FILEPATH}/${PROGF_TRACE_PRE}${i}${PROGF_TRACE_EXT}
done