#!/bin/bash

set -eu

UPLOAD_FILENAME="standing.json"

python get_top_score.py > ${UPLOAD_FILENAME}
python send_aws.py ${UPLOAD_FILENAME}
