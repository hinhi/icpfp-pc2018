from selenium import webdriver
from bs4 import BeautifulSoup
import json
from collections import OrderedDict

class Crawler(object):

    def crawl(self, url):
        if url is not None:
            browser = webdriver.PhantomJS(executable_path='./phantomjs')
            browser.get(url)

        html_source = browser.page_source
        bs_obj = BeautifulSoup(html_source, 'html.parser')

        browser.quit()

        return bs_obj


def top_scores():
    url = 'https://icfpcontest2018.github.io/full/live-standings.html'
    cw = Crawler()
    bs_obj = cw.crawl(url)

    top_scores = OrderedDict()
    for table in bs_obj.findAll('table'):
        if table.thead.th.get_text() == 'Rank':
            problem_scores = []
            for tr in table.tbody.findAll('tr'):
                tds = tr.findAll('td')
                rank = int(tds[0].get_text())
                team = tds[1].get_text()
                total_energy = int(tds[2].get_text())
                total_score = int(tds[3].get_text())
                problem_scores.append(
                    OrderedDict({
                        'rank': rank,
                        'team': team,
                        'total_energy': total_energy,
                        'total_score': total_score,
                    }))
            top_scores['total'] = problem_scores

        if table.thead.th.get_text() == 'Team':
            # get problem data
            # example: <h3 id="problem-fr063">Problem FR063</h3>
            problem_name = table.find_previous().get_text().split(" ")[1]

            problem_scores = []
            for tr in table.tbody.findAll('tr'):
                tds = tr.findAll('td')
                team = tds[0].get_text()
                total_energy = int(tds[1].get_text())
                total_score = int(tds[2].get_text())
                problem_scores.append(
                    OrderedDict({
                        'team': team,
                        'total_energy': total_energy,
                        'total_score': total_score,
                    }))
            top_scores[problem_name] = problem_scores
    return json.dumps(top_scores, indent=4)


if __name__ == '__main__':
    print(top_scores())
