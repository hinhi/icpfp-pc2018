import json
from const import problem_info

def create_default_json(input_filename, result_filename):
    output_list = []
    with open(input_filename, "r") as f:
        for line in f:
            content = json.loads(line)
            for k, v in content.items():
                key, value = k, v
            d = {
                "problem_id": k,
                "name": "default",
                "submit_time": "0",
                "energy": int(content[k]['default']['energy']),
                "score": 0,
                "R": problem_info[k]['R'],
            }
            output_list.append(d)
    with open(result_filename, 'w') as f:
        json.dump(output_list, f, indent=4)

if __name__ == '__main__':
    import sys

    input_filename = sys.argv[1]
    result_filename = sys.argv[2]
    create_default_json(input_filename, result_filename)







[
    {
        "problem_id": "FA004",
        "name": "omu",
        "submit_time": "20180722230100",
        "energy": 33785080,
        "score": 3886
    },
    {
        "problem_id": "FA004",
        "name": "ochi",
        "submit_time": "20180722230101",
        "energy": 79285684,
        "score": 3720
    }
]