## 参考
* https://qiita.com/gtaiyou24/items/21d7d3b539c43e6616a6
* https://uitspitss.hatenablog.com/entry/2016/04/15/193446
* https://qiita.com/hujuu/items/b0339404b8b0460087f9

## 開発環境の準備

1. `pip install -r requirements.txt`
1. http://phantomjs.org/download.html このリンクからバイナリを落とす
1. 所定のディレクトリの配下にバイナリファイルを置く（zip内のbin配下）
1. `python get_top_score.py` を実行すると動く

