import re
import os
import json
from const import problem_info
from get_submit_score import ScoreCrowlar


import sys
sys.path.append('..')
from analyzer.aws import aws_client, NBT_DIR, RESULT_DIR


aws_client.get_nbt_files()
pat = re.compile('(F[ADR]\d\d\d)_(.+)_(\d{14}).nbt')
standing = json.load(open('standing.json'))



sc = ScoreCrowlar()
sc.open_browser()

for target in os.listdir(NBT_DIR):
    m = pat.match(target)

    if m:
        problem_id, name, submit_time = m.group(1), m.group(2), m.group(3)

        if problem_info[problem_id]['R'] > 150:
            continue

        result_filename = '%s_%s_%s.json' % (problem_id, name, submit_time)
        error_filename = '%s_%s_%s.png' % (problem_id, name, submit_time)

        if os.path.exists(RESULT_DIR + result_filename):
            continue

        sc.init(problem_id)
        energy = sc.get_score(NBT_DIR + target, RESULT_DIR + error_filename)

        if energy:
            result = {'energy': energy}

            with open(RESULT_DIR + result_filename, 'w') as f:
                f.write(json.dumps(result, indent=4, ensure_ascii=False))

            aws_client.upload_file(
                RESULT_DIR + result_filename, Key=result_filename)
        else:
            aws_client.upload_file(
                RESULT_DIR + result_filename, Key=result_filename)


sc.close_browser()
