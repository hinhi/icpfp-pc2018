import boto3

def send_file_to_aws(file_name):
    s3 = boto3.resource('s3')
    b = s3.Bucket('chirimenjako-2018')
    # FIXME: ローカル起動なのでローカルのパスが入ってる
    b.upload_file('/home/m-hirao/git_repos/icpfp-pc2018/crowler/standing.json', 'standing.json')

if __name__ == '__main__':
    import sys

    file_name = sys.argv[1]
    send_file_to_aws(file_name)