def read_mdl(f):
    r = ord(f.read(1))
    mat = []
    i = 8
    b = 0
    for _ in range(r * r * r):
        if i == 8:
            i = 0
            b = ord(f.read(1))
        mat.append(1 if b & 1 << i else 0)
        i += 1
    return r, mat


def conv2bit(file_name):
    s = ''
    i = 0
    with open(file_name, 'rb') as f:
        while True:
            if i % 8 == 0:
                try:
                    s += str(bin(ord(f.read(1)))).replace('0b', '').zfill(8)
                except TypeError:
                    break
            i += 1
    return s


def read_nbt(target_file):
    i = 0
    commands = []
    while True:
        if i % 8 == 0:
            try:
                commands.append(
                    bin(ord(target_file.read(1))).replace('0b', '').zfill(8))
            except TypeError:
                break
        i += 1
    return iter(commands)
