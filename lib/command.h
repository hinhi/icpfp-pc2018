
#ifndef R_OCHI_COMMAND_H
#define R_OCHI_COMMAND_H

using namespace std;

class Command {
public:
	virtual string toString() = 0;
};

class HaltCommand : public Command {
	int id;
	long long energy;
public:
	HaltCommand(int id, long long energy):id(id), energy(energy) {}
	string toString() {
		return "Halt " + tostr(id) + "\nEnd " + tostr(energy);
	}
};

class WaitCommand : public Command {
	int id;
public:
	WaitCommand(int id) :id(id) {}
	string toString() {
		return "Wait " + tostr(id);
	}
};
class FlipCommand : public Command {
	int id;
public:
	FlipCommand(int id) :id(id) {}
	string toString() {
		return "Flip " + tostr(id);
	}
};

class SmoveCommand : public Command {
	int id;
	Vector3D lld;
public:
	SmoveCommand(int id, Vector3D lld) :id(id) {
		this->lld = lld;
	}
	string toString() {
		return "Smove " + tostr(id) + " " + tostr(lld.x) + " " + tostr(lld.y) + " " + tostr(lld.z);
	}

};
class LmoveCommand : public Command {
	int id;
	Vector3D sld1, sld2;
public:

	LmoveCommand(int id, Vector3D slb1, Vector3D slb2) :id(id) {
		this->sld1 = slb1;
		this->sld2 = slb2;
	}
	string toString() {
		return "Lmove " + tostr(id) + " " + tostr(sld1.x) + " " + tostr(sld1.y) + " " + tostr(sld1.z) + " " + tostr(sld2.x) + " " + tostr(sld2.y) + " " + tostr(sld2.z);
	}
};
class FusionSCommand : public Command {
	int id;
	Vector3D nd;
	int target;
public:
	FusionSCommand(int id, Vector3D nd, int target) :id(id) {
		this->nd = nd;
		this->target = target;
	}
	string toString() {
		return "FusionS " + tostr(id) + " " + tostr(nd.x) + " " + tostr(nd.y) + " " + tostr(nd.z);
	}
};
class FusionPCommand : public Command {
	int id;
	Vector3D nd;
	int target;
public:
	FusionPCommand(int id, Vector3D nd, int target) :id(id) {
		this->nd = nd;
		this->target = target;
	}
	string toString() {
		return "FusionP " + tostr(id) + " " + tostr(nd.x) + " " + tostr(nd.y) + " " + tostr(nd.z);
	}
};
class FissionCommand : public Command {
	int id;
	Vector3D nd;
	int m;
public:
	FissionCommand(int id, Vector3D nd, int m) :id(id) {
		this->nd = nd;
		this->m = m;
	}
	string toString() {
		return "Fission " + tostr(id) + " " + tostr(nd.x) + " " + tostr(nd.y) + " " + tostr(nd.z) + " " + tostr(m);
	}
};
class FillCommand : public Command {
	int id;
	Vector3D nd;
public:
	FillCommand(int id, Vector3D nd) :id(id) {
		this->nd = nd;
	}
	string toString() {
		return "Fill " + tostr(id) + " " + tostr(nd.x) + " " + tostr(nd.y) + " " + tostr(nd.z);
	}
};
class VoidCommand : public Command {
	int id;
	Vector3D nd;
public:
	VoidCommand(int id, Vector3D nd) :id(id) {
		this->nd = nd;
	}
	string toString() {
		return "Void " + tostr(id) + " " + tostr(nd.x) + " " + tostr(nd.y) + " " + tostr(nd.z);
	}
};
class GFillCommand : public Command {
	int id;
	Vector3D nd;
	Vector3D fd;
public:
	GFillCommand(int id, Vector3D nd, Vector3D fd) :id(id) {
		this->nd = nd;
		this->fd = fd;
	}
	string toString() {
		return "GFill " + tostr(id) + " " + tostr(nd.x) + " " + tostr(nd.y) + " " + tostr(nd.z) + " " + tostr(fd.x) + " " + tostr(fd.y) + " " + tostr(fd.z);
	}
};
class GVoidCommand : public Command {
	int id;
	Vector3D nd;
	Vector3D fd;
public:
	GVoidCommand(int id, Vector3D nd, Vector3D fd) :id(id) {
		this->nd = nd;
		this->fd = fd;
	}
	string toString() {
		return "GVoid " + tostr(id) + " " + tostr(nd.x) + " " + tostr(nd.y) + " " + tostr(nd.z) + " " + tostr(fd.x) + " " + tostr(fd.y) + " " + tostr(fd.z);
	}
};


#endif
