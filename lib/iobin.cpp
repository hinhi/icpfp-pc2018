#include <iostream>
#include <fstream>
#include <vector>


using namespace std;


int read_mdl(char *file_name, vector<int> &mat) {
    ifstream f = ifstream(file_name, ios::in | ios::binary);

    if (!f) {
        cerr << "ファイルが開けません" << endl;
        return 0;
    }

    char c;
    int r;
    f.read(&c, 1);
    r = c;
    for (int i = 0; i < r * r * r; i++) {
        if (i % 8 == 0) {
            f.read(&c, 1);
        }
        mat.push_back(c & (1 << i % 8) ? 1 : 0);
    }
    return r;
}


int main(int argc, char** argv) {
    int r;
    vector<int> mat;

    r = read_mdl(argv[1], mat);
    cout << r << endl;
    for (auto i = mat.begin(); i != mat.end(); i++) {
        cout << *i << endl;
    }
    return 0;
}
