//
// Created by 100525 on 2018/07/21.
//

#ifndef R_OCHI_GEOMETRY_H
#define R_OCHI_GEOMETRY_H

class Vector3D {
public:
    int x, y, z;

    Vector3D() {
        x = -1;
        y = -1;
        z = -1;
    }

    Vector3D(int x, int y, int z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    Vector3D(int dir, int dist) {
        x = y = z = 0;
        if (dir == 0) x = dist;
        else if (dir == 1) y = dist;
        else if (dir == 2) z = dist;
        else
            assert(false);
    }

    Vector3D(const Vector3D &p1, const Vector3D &p2) {
        x = p1.x - p2.x;
        y = p1.y - p2.y;
        z = p1.z - p2.z;
    }

    void move(int dir, int dist) {
        if (dir == 0) x += dist;
        else if (dir == 1) y += dist;
        else if (dir == 2) z += dist;
        else
            assert(false);
    }

    void move(Vector3D &diff) {
        x += diff.x;
        y += diff.y;
        z += diff.z;
    }

    int dimension() {
        int dim = 0;
        dim += x != 0;
        dim += y != 0;
        dim += z != 0;
        return dim;
    }

    bool operator==(const Vector3D &a) const {
        return x == a.x && y == a.y && z == a.z;
    }

    bool operator<(const Vector3D &a) const {
    	if (x != a.x) return x < a.x;
    	if (y != a.y) return y < a.y;
    	return z < a.z;
    }

    Vector3D operator+(const Vector3D &a) {
    	return Vector3D(x + a.x, y + a.y, z + a.z);
    }

    Vector3D operator-(const Vector3D &a) {
        return Vector3D(x - a.x, y - a.y, z - a.z);
    }
};

#endif //R_OCHI_GEOMETRY_H
