import re
import numpy as np
import logging
from collections import OrderedDict


logging.basicConfig(level=logging.WARNING)
log = logging.getLogger('nbt_core')


command_patterns = OrderedDict((
    ('halt', re.compile('11111111')),
    ('wait', re.compile('11111110')),
    ('flip', re.compile('11111101')),
    ('smove', re.compile('00([01]{2})0100')),
    ('lmove', re.compile('([01]{2})([01]{2})1100')),
    ('fusion_p', re.compile('([01]{5})111')),
    ('fusion_s', re.compile('([01]{5})110')),
    ('fission', re.compile('([01]{5})101')),
    ('fill', re.compile('([01]{5})011')),
    ('void', re.compile('([01]{5})010')),
    ('gfill', re.compile('([01]{5})001')),
    ('gvoid', re.compile('([01]{5})000')),
))


def mlen(vec):
    x, y, z = vec
    return abs(x) + abs(y) + abs(z)


def ld(a, i):
    if a == '01':
        return np.array([i, 0, 0])
    elif a == '10':
        return np.array([0, i, 0])
    elif a == '11':
        return np.array([0, 0, i])


def nd(i):
    x = int(i / 9) - 1
    y = int((i - (x + 1) * 9)/3) - 1
    z = i - (x + 1) * 9 - (y + 1) * 3 - 1
    return np.array([x, y, z])


class Bot:
    def __init__(self, bid=None, pos=None, seeds=None):
        self.bid = bid
        self.pos = pos
        self.seeds = seeds

    def move(self, d):
        self.pos += d


class System:
    def __init__(self, R):
        self.R = R
        self.energy = {0: {}}
        self.harmonics = 0
        self.bots = {1: Bot(bid=1, pos=np.array([0, 0, 0]),
                            seeds=[i+2 for i in range(19)])}

    def cost(self, bid, action, c):
        if bid in self.energy:
            if action in self.energy[bid]:
                self.energy[bid][action] += c
            else:
                self.energy[bid][action] = c
        else:
            self.energy[bid] = {action: c}

    def after(self):
        if self.harmonics:
            field_cost = 30 * (self.R ** 3)
        else:
            field_cost = 3 * (self.R ** 3)

        self.cost(0, 'field', field_cost)

        for bid in self.bots:
            self.cost(bid, 'active', 20)

    def halt(self, bid):
        log.debug('%s: halt' % bid)

    def wait(self, bid):
        log.debug('%s: wait' % bid)

    def flip(self, bid):
        log.debug('%s: flip' % bid)
        self.harmonics = not self.harmonics

    def smove(self, bid, lld):
        log.debug('%s: smove <%s, %s, %s>' % (bid, lld[0], lld[1], lld[2]))
        self.bots[bid].move(lld)
        self.cost(bid, 'smove', 2 * mlen(lld))

    def lmove(self, bid, sld1, sld2):
        log.debug('%s: lmove <%s, %s, %s>  <%s, %s, %s>' % (
            bid, sld1[0], sld1[1], sld1[2], sld2[0], sld2[1], sld2[2]))
        self.bots[bid].move(sld1 + sld2)
        self.cost(bid, 'lmove', 2 * (mlen(sld1) + 2 + mlen(sld2)))

    def fusion_p(self, bid, nd_):
        for bid_, bot in self.bots.items():
            if all(bot.pos == self.bots[bid].pos + nd_):
                self.bots[bid].seeds.extend([bid_] + bot.seeds)
                log.debug('%s: fusion_p with %s' % (bid, bid_))
        self.cost(bid, 'fusion', -12)

    def fusion_s(self, bid, nd_):
        for bid_, bot in self.bots.items():
            if all(bot.pos == self.bots[bid].pos + nd_):
                log.debug('%s: fusion_s with %s' % (bid, bid_))
        del self.bots[bid]
        self.cost(bid, 'fusion', -12)

    def fission(self, bid, nd_, m):
        bot = self.bots[bid]
        bid2 = bot.seeds[0]
        bot2 = Bot(bid=bid2, pos=bot.pos + nd_, seeds=bot.seeds[1:m+1])
        bot.seeds = bot.seeds[m+1:]
        log.debug('%s: fission into %s' % (bid, bid2))
        self.bots[bid2] = bot2
        self.cost(bid, 'fission', 12)
        self.cost(bid2, 'fission', 12)

    def fill(self, bid):
        log.debug('%s: fill' % bid)
        self.cost(bid, 'fill', 12)

    def void(self, bid):
        log.debug('%s: void' % bid)
        self.cost(bid, 'void', -12)

    def gfill(self, bid, nd, fd):
        log.debug('%s: gfill <%s, %s, %s>  <%s, %s, %s>' % (
            bid, nd[0], nd[1], nd[2], fd[0], fd[1], fd[2]))
        vec = fd - nd
        nonzeros = vec.nonzero()[0]
        d = len(nonzeros)
        if d == 1:
            self.cost(bid, 'gfill', mlen(vec) / 2)
        elif d == 2:
            s = 1
            for e in nonzeros:
                s *= abs(vec[e])
            self.cost(bid, 'gfill', s / 4)
        elif d == 3:
            self.cost(bid, 'gfill', -abs(np.prod(vec)) / 8)

    def gvoid(self, bid, nd, fd):
        log.debug('%s: gvoid <%s, %s, %s>  <%s, %s, %s>' % (
            bid, nd[0], nd[1], nd[2], fd[0], fd[1], fd[2]))
        vec = fd - nd
        nonzeros = vec.nonzero()[0]
        d = len(nonzeros)
        if d == 1:
            self.cost(bid, 'gvoid', -mlen(vec) / 2)
        elif d == 2:
            s = 1
            for e in nonzeros:
                s *= abs(vec[e])
            self.cost(bid, 'gvoid', -s / 4)
        elif d == 3:
            self.cost(bid, 'gvoid', -abs(np.prod(vec)) / 8)

    def execute(self, commands):
        bids = [bid for bid in self.bots]
        for bid in bids:
            command = commands.__next__()

            for key, pat in command_patterns.items():
                m = pat.match(command)
                if m:
                    if key == 'halt':
                        self.halt(bid)
                    elif key == 'wait':
                        self.wait(bid)
                    elif key == 'flip':
                        self.flip(bid)
                    elif key == 'smove':
                        a = m.group(1)
                        c = commands.__next__()
                        i = re.compile('000([01]{5})').match(c).group(1)
                        i = int(i, 2) - 15
                        lld = ld(a, i)
                        self.smove(bid, lld)
                    elif key == 'lmove':
                        a1 = m.group(1)
                        a2 = m.group(2)
                        c = commands.__next__()
                        m2 = re.compile('([01]{4})([01]{4})').match(c)
                        i1 = m2.group(1)
                        i2 = m2.group(2)
                        i1 = int(i1, 2) - 5
                        i2 = int(i2, 2) - 5
                        sld1 = ld(a1, i1)
                        sld2 = ld(a2, i2)
                        self.lmove(bid, sld1, sld2)
                    elif key == 'fusion_p':
                        i = m.group(1)
                        i = int(i, 2)
                        nd_ = nd(i)
                        self.fusion_p(bid, nd_)
                    elif key == 'fusion_s':
                        i = m.group(1)
                        i = int(i, 2)
                        nd_ = nd(i)
                        self.fusion_s(bid, nd_)
                    elif key == 'fission':
                        i = m.group(1)
                        i = int(i, 2)
                        nd_ = nd(i)
                        c = commands.__next__()
                        m_ = int(c, 2)
                        self.fission(bid, nd_, m_)
                    elif key == 'fill':
                        self.fill(bid)
                    elif key == 'void':
                        self.void(bid)
                    elif key == 'gfill':
                        i = m.group(1)
                        i = int(i, 2)
                        nd_ = nd(i)
                        cx = commands.__next__()
                        x = int(cx, 2) - 30
                        cy = commands.__next__()
                        y = int(cy, 2) - 30
                        cz = commands.__next__()
                        z = int(cz, 2) - 30
                        fd = np.array([x, y, z])
                        self.gfill(bid, nd_, fd)
                    elif key == 'gvoid':
                        i = m.group(1)
                        i = int(i, 2)
                        nd_ = nd(i)
                        cx = commands.__next__()
                        x = int(cx, 2) - 30
                        cy = commands.__next__()
                        y = int(cy, 2) - 30
                        cz = commands.__next__()
                        z = int(cz, 2) - 30
                        fd = np.array([x, y, z])
                        self.gvoid(bid, nd_, fd)
                    break
            else:
                log.error('invalid: %s' % command)
        self.after()
        return commands


def simulate(r, commands, with_history=True):
    from copy import deepcopy
    system = System(r)

    time = 0
    energy = 0
    data = []

    if with_history:
        data.append(deepcopy(system.energy))

    while True:
        try:
            commands = system.execute(commands)
            time += 1
            energy_local = 0
            for bid, dist in system.energy.items():
                for action, e in dist.items():
                    energy_local += e
            energy += int(energy_local)
        except Exception as e:
            log.debug(e)
            break
        finally:
            if with_history:
                data.append(deepcopy(system.energy))

    return time, energy, data
