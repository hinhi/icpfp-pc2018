//
// Created by 100525 on 2018/07/21.
//

#ifndef R_OCHI_DEFINE_H
#define R_OCHI_DEFINE_H

#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <functional>
#include <queue>
#include <stack>
#include <cmath>
#include <iomanip>
#include <list>
#include <tuple>
#include <bitset>
#include <ciso646>
#include <cassert>
#include <complex>
#include <random>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <thread>
#include <chrono>
#include <ratio>
#include <bitset>
#include <fstream>

#include <cstdio>
#include <ctime>
#include <cstdio>
#include <sys/types.h>
#include <sys/timeb.h>

#define rep(i, n)    for(int (i) = 0; (i) < (n); (i)++)
#define REP(i, a, n)    for(int (i) = (a); (i) < (n); (i)++)
#define each(i, n)    for(auto &i : n)
#define clr(a)        memset((a), 0 ,sizeof(a))
#define mclr(a)        memset((a), -1 ,sizeof(a))
#define all(a)        (a).begin(),(a).end()
#define dump(val)    cerr << #val " = " << val << endl;
#define abs(a)       ((a) > 0 ? (a) : -(a))

template <class T>
std::string tostr(T x) {
    std::stringstream o;
    o << x;
    return o.str();
}


#endif //R_OCHI_DEFINE_H
