//
// Created by 100498 on 2018/07/21.
//

#ifndef R_OCHI_SYSTEM_H
#define R_OCHI_SYSTEM_H

#include "define.h"
#include "geometry.h"
#include "command.h"
#include <numeric>

int rx[6] = {0, 0, 0, -1, 1, 0};
int ry[6] = {-1, 0, 0, 0, 0, 1};
int rz[6] = {0, -1, 1, 0, 0, 0};

typedef pair<int, int> P;


class Bot {
public:
	int id;
	bool isActive;
	Vector3D pos;
	set<int> seeds;

	Bot() = default;

	Bot(int id) {
		this->id = id;
		isActive = false;
	}

	bool operator<(const Bot &a) const {
		return id < a.id;
	}
};



class Trace {
public:

	vector<Command*> commandList;

	void append(Command* command) {
		commandList.push_back(command);
	}
	void output(int turn, long long energy, bool harmonics, vector<Bot> bots) {

		int numOfActiveBots = accumulate(bots.begin(), bots.end(), 0, [](int x, Bot bot){ return x + bot.isActive; });

		cout << turn << " " << numOfActiveBots << endl;

		each(command, commandList) {
			cout << command->toString() << endl;
		}

		commandList.clear();

	}
};


class System {
private:


	bool canUseVoidVersion(Vector3D pos, int bid) {
		each(bot, bots) {
			if (bot.isActive && bot.id != bid) {
				if (pos == bot.pos) {
					return false;
				}
			}
		}

		if (used[pos.x][pos.y][pos.z])return false;
		return true;
	}

	bool canUse(Vector3D pos, int bid) {

		each(bot, bots) {
			if (bot.isActive && bot.id != bid) {
				if (pos == bot.pos) {
					return false;
				}
			}
		}

		if (used[pos.x][pos.y][pos.z])return false;
		if (matrix[pos.x][pos.y][pos.z])return false;
		return true;
	}

	void useVoidVersion(Vector3D &pos, int bid) {
		each(bot, bots) {
			if (bot.isActive && bot.id != bid) {
				assert(!(pos == bot.pos));
			}
		}

		assert(!used[pos.x][pos.y][pos.z]);
		used[pos.x][pos.y][pos.z] = bid;
		usedPos.push_back(pos);

	}

	void use(Vector3D &pos, int bid) {
		each(bot, bots) {
			if (bot.isActive && bot.id != bid) {
				assert(!(pos == bot.pos));
			}
		}

		assert(!used[pos.x][pos.y][pos.z]);
		assert(!matrix[pos.x][pos.y][pos.z]);
		used[pos.x][pos.y][pos.z] = bid;
		usedPos.push_back(pos);
	}

	bool canBulkUseVoidVersion(Vector3D &pos1, Vector3D &pos2, int bid) {
		int minx = min(pos1.x, pos2.x);
		int maxx = max(pos1.x, pos2.x);
		int miny = min(pos1.y, pos2.y);
		int maxy = max(pos1.y, pos2.y);
		int minz = min(pos1.z, pos2.z);
		int maxz = max(pos1.z, pos2.z);

		REP(x, minx, maxx + 1) REP(y, miny, maxy + 1) REP(z, minz, maxz + 1) {
			Vector3D pos = Vector3D(x, y, z);
			if (canUseVoidVersion(pos, bid) == false) return false;
		}
		return true;

	}

	bool canBulkUse(Vector3D &pos1, Vector3D &pos2, int bid) {
		int minx = min(pos1.x, pos2.x);
		int maxx = max(pos1.x, pos2.x);
		int miny = min(pos1.y, pos2.y);
		int maxy = max(pos1.y, pos2.y);
		int minz = min(pos1.z, pos2.z);
		int maxz = max(pos1.z, pos2.z);

		REP(x, minx, maxx + 1) REP(y, miny, maxy + 1) REP(z, minz, maxz + 1) {
			Vector3D pos = Vector3D(x, y, z);
			if (canUse(pos, bid) == false) return false;
		}
		return true;

	}

	void bulkUseVoidVersion(Vector3D &pos1, Vector3D &pos2, int bid) {
		int minx = min(pos1.x, pos2.x);
		int maxx = max(pos1.x, pos2.x);
		int miny = min(pos1.y, pos2.y);
		int maxy = max(pos1.y, pos2.y);
		int minz = min(pos1.z, pos2.z);
		int maxz = max(pos1.z, pos2.z);

		REP(x, minx, maxx + 1) REP(y, miny, maxy + 1) REP(z, minz, maxz + 1) {
			Vector3D pos = Vector3D(x, y, z);
			useVoidVersion(pos, bid);
		}
	}

	void bulkUse(Vector3D &pos1, Vector3D &pos2, int bid) {
		int minx = min(pos1.x, pos2.x);
		int maxx = max(pos1.x, pos2.x);
		int miny = min(pos1.y, pos2.y);
		int maxy = max(pos1.y, pos2.y);
		int minz = min(pos1.z, pos2.z);
		int maxz = max(pos1.z, pos2.z);

		REP(x, minx, maxx + 1) REP(y, miny, maxy + 1) REP(z, minz, maxz + 1) {
			Vector3D pos = Vector3D(x, y, z);
			use(pos, bid);
		}
	}

	bool isGround(int tx, int ty, int tz) {
		if(ty == -1) return true;
		return grounded[tx][ty][tz];
	}

	void setGround(int tx, int ty, int tz) {
		// 周囲に grounded セルがあるか確認、なければ終了
		bool ground = false;
		rep(dir, 6) {
			int nx = tx + rx[dir];
			int ny = ty + ry[dir];
			int nz = tz + rz[dir];
			if (!(check(nx, ny, nz) || check(nx, ny + 1, nz))) continue;
			if (isGround(nx, ny, nz)) {
				ground = true;
				break;
			}
		}
		if (!ground) {
			notGrounded.insert(Vector3D(tx, ty, tz));
			return;
		}

		// 周囲に grounded セルがあればそのセルは grounded
		grounded[tx][ty][tz] = true;
		notGrounded.erase(Vector3D(tx, ty, tz));

		// 周囲 6 セルについても再帰的に grounded にしていく
		rep(dir, 6) {
			int nx = tx + rx[dir];
			int ny = ty + ry[dir];
			int nz = tz + rz[dir];
			if (!check(nx, ny, nz)) continue;
			if (isGround(nx, ny, nz)) continue;
			if (matrix[nx][ny][nz]) setGround(nx, ny, nz);
		}
	}

	bool isFloor(Vector3D pos) {
		return pos.y == 0;
	}

	bool dfsToFloor(Vector3D pos, map<Vector3D, bool> &visited) {
		if(visited.count(pos)) return visited[pos];
		visited[pos] = false;

		if (pos.y == 0) return true;

		rep(dir, 6) {
			Vector3D npos = pos;
			npos.x += rx[dir];
			npos.y += ry[dir];
			npos.z += rz[dir];

			if(!check(npos)) continue;
			if(!grounded[npos.x][npos.y][npos.z]) continue;
			if (npos.y == 0) return true;
			if(dfsToFloor(npos, visited)) return visited[pos] = true;
		}

		return grounded[pos.x][pos.y][pos.z] = false;
	}

	bool checkDfsToFloor(Vector3D pos, map<Vector3D, bool> &visited) {
		if (visited.count(pos)) return visited[pos];
		visited[pos] = false;

		if (pos.y == 0) return true;

		rep(dir, 6) {
			Vector3D npos = pos;
			npos.x += rx[dir];
			npos.y += ry[dir];
			npos.z += rz[dir];

			if (!check(npos)) continue;
			if (matrix[npos.x][npos.y][npos.z] == false) continue;
			if (checkDfsToFloor(npos, visited)) {
				return true;
			}
		}

		return false;
	}

	bool checkOutsideGrounded(Vector3D minPos, Vector3D maxPos, int bid) {
		map<Vector3D, bool> visited;
		REP(x, minPos.x, maxPos.x + 1) {
			REP(y, minPos.y, maxPos.y + 1) {
				REP(z, minPos.z, maxPos.z + 1) {

					rep(dir, 6) {
						Vector3D pos(x + rx[dir], y + ry[dir], z + rz[dir]);
						if (!check(pos)) continue;
						if (check(pos, minPos, maxPos)) continue; // 外側でなければ除外
						if (matrix[pos.x][pos.y][pos.z] == false) continue;
						if (checkDfsToFloor(pos, visited) == false) return false;
					}
				}
			}
		}
		assert(false);
		return true;
	}

	void updateOutsideGrounded(Vector3D minPos, Vector3D maxPos, int bid) {
		bulkUseVoidVersion(minPos, maxPos, bid);

		map<Vector3D, bool> visited;
		REP(x, minPos.x, maxPos.x + 1) {
			REP(y, minPos.y, maxPos.y + 1) {
				REP(z, minPos.z, maxPos.z + 1) {
					energy += (matrix[x][y][z] ? -12: 3);
					matrix[x][y][z] = false;

					rep(dir, 6) {
						Vector3D pos(x + rx[dir], y + ry[dir], z + rz[dir]);
						if(!check(pos)) continue;
						if(check(pos, minPos, maxPos)) continue; // 外側でなければ除外
						if(!isGround(pos.x, pos.y, pos.z)) continue;

						dfsToFloor(pos, visited);
					}
				}
			}
		}
	}

	

public:
	int R;
	int turn;
	long long energy;
	bool harmonics; // false->Low, true->High
	static bool matrix[250][250][250]; // false->Empty, true->Full
	static bool model[250][250][250];
	static bool grounded[250][250][250];
	static int modelCm[251][251][251];
	vector<Bot> bots;
	Trace trace;
	vector<int> nextActivateBid;
	vector<int> nextInactivateBid;
	set<Vector3D> notGrounded;

	static int used[250][250][250];
	vector<Vector3D> usedPos;

	int mode;

	/*
	* N: seeds.size
	* mode: 0->Assembly Problems, 1->Disassembly Problems, 2->Reassembly Problems
	*/
	System(int R, int N, int mode) {
		turn = 0;
		energy = 0ll;
		harmonics = false;
		this->mode = mode;

		if (mode == 0) {
			clr(matrix);
		}
		if (mode == 1) {
			clr(model);
		}

		clr(used);
		clr(grounded);

		rep(i, N) {
			bots.emplace_back(i + 1);
		}
		bots[0].pos = Vector3D(0, 0, 0);
		bots[0].isActive = true;
		REP(i, 1, N) {
			bots[0].seeds.insert(i + 1);
		}
		this->R = R;

		// model の累積和 modelCm を計算
		rep(x, R) rep(y, R) rep(z, R) {
			modelCm[x + 1][y + 1][z + 1] = model[x][y][z];
		}
		rep(x, R - 1) rep(y, R) rep(z, R) {
			modelCm[x + 1][y][z] += modelCm[x][y][z];
		}
		rep(x, R) rep(y, R - 1) rep(z, R) {
			modelCm[x][y + 1][z] += modelCm[x][y][z];
		}
		rep(x, R) rep(y, R) rep(z, R - 1) {
			modelCm[x][y][z + 1] += modelCm[x][y][z];
		}
	}

	int countModel(Vector3D p1, Vector3D p2) {
		int minX = min(p1.x, p2.x);
		int maxX = max(p1.x, p2.x);
		int minY = min(p1.y, p2.y);
		int maxY = max(p1.y, p2.y);
		int minZ = min(p1.z, p2.z);
		int maxZ = max(p1.z, p2.z);

		int ret = modelCm[maxX + 1][maxY + 1][maxZ + 1];
		ret -= modelCm[minX][maxY + 1][maxZ + 1];
		ret -= modelCm[maxX + 1][minY][maxZ + 1];
		ret -= modelCm[maxX + 1][maxY + 1][minZ];
		ret += modelCm[maxX + 1][minY][minZ];
		ret += modelCm[minX][maxY + 1][minZ];
		ret += modelCm[minX][minY][maxZ + 1];
		ret -= modelCm[minX][minY][minZ];

		return ret;
	}

	bool canLowHarmonics() {
		return notGrounded.empty();
	}


	bool isWellFormed() {
		if (!harmonics) {
			if (canLowHarmonics() == false) return false;
		}

		// 各 bot が異なる id を持つかチェック
		set<int> ids;
		each(bot, bots) {
			if (ids.count(bot.id)) return false;
			ids.insert(bot.id);
		}

		// 各 bot の位置が独立で、Void セルであるかチェック
		set<Vector3D> poss;
		each(bot, bots) {
			if (!bot.isActive) continue;
			if (poss.count(bot.pos) || matrix[bot.pos.x][bot.pos.y][bot.pos.z]) return false;
			poss.insert(bot.pos);
		}

		// 各 bot の seeds が独立かチェック
		// thisIsEachBotSeedsDistinctCheckMethod(bot);	// シミュレータがバグってなければ不正な状態になることはないので不要

		// 各 bot が active な bot の ID を seeds として持っていないかチェック
		// thisIsEachBotSeedsNotIncludeOtherActiveBotIdMethod(bot);	// シミュレータがバグってなければ不正な状態になることはないので不要

		return true;
	}

	bool isModelCreateComplete() {
		rep(x, R) rep(y, R) rep(z, R) {
			if (matrix[x][y][z] != model[x][y][z]) {
				return false;
			}
		}
		return true;
	}

	void halt() {
		int numOfActiveBots = accumulate(bots.begin(), bots.end(), 0, [](int x, Bot bot){ return x + bot.isActive; });
		assert(numOfActiveBots == 1);
		assert(bots.begin()->pos == Vector3D(0, 0, 0));
		assert(!harmonics);
		assert(isModelCreateComplete());
		trace.append(new HaltCommand(bots[0].id, energy));
	}

	void wait(Bot &bot) {
		use(bot.pos, bot.id);
		trace.append(new WaitCommand(bot.id));
	}

	void flip(Bot &bot) {
		use(bot.pos, bot.id);
		harmonics = !harmonics;
		trace.append(new FlipCommand(bot.id));
		if (!harmonics) assert(isWellFormed());
	}

	/**
	 * smove した結果の Position を返す
	 */
	pair<int, Vector3D> smoveResult(Bot bot, int dir, int dist) {

		if(abs(dist) > 15) return make_pair(false, Vector3D());

		Vector3D from = bot.pos;
		bot.pos.move(dir, dist);
		
		if(!check(bot.pos)) return make_pair(false, Vector3D());
		if(!canBulkUse(bot.pos, from, bot.id)) return make_pair(false, Vector3D());

		return make_pair(true, bot.pos);
	}

	/**
	 * dir: 0->x座標 1->y座標 2->z座標
	 * dist: 動く距離
	 */
	void smove(Bot &bot, int dir, int dist) {
		assert(abs(dist) <= 15);
		Vector3D from = bot.pos;
		bot.pos.move(dir, dist);
		assert(check(bot.pos));
		bulkUse(bot.pos, from, bot.id);
		energy += 2 * abs(dist);
		trace.append(new SmoveCommand(bot.id, Vector3D(dir, dist)));
	}

	bool fmove(Bot &bot, int dir, int dist) {
		if (dist > 0) {
			for (int d = dist; d >= 1; d--) {
				if (smoveResult(bot, dir, d).first) {
					smove(bot, dir, d);
					return true;
				}
			}
		}
		else {
			for (int d = dist; d <= -1; d++) {
				if (smoveResult(bot, dir, d).first) {
					smove(bot, dir, d);
					return true;
				}
			}
		}
		return false;
	}

	int getDir(Vector3D v) {
		assert(v.dimension() == 1);
		if (v.x) return 0;
		if (v.y) return 1;
		return 2;
	}

	int getDist(Vector3D v) {
		assert(v.dimension() == 1);
		if (v.x) return v.x;
		if (v.y) return v.y;
		return v.z;
	}

	bool optmove(Bot &bot, Vector3D target) {
		set<Vector3D> visited;

		each(bot, bots) {
			if (!bot.isActive) continue;
			visited.insert(bot.pos);
		}

		queue<pair<Vector3D, Vector3D>> q;
		q.push(make_pair(bot.pos, Vector3D(-100, -100, -100)));
		while (!q.empty()) {
			auto info = q.front(); q.pop();
			Vector3D now = info.first;

			if (now == target) {
				if (turn == 300) {
					int hoge = 1;
				}
				smove(bot, getDir(info.second), getDist(info.second));
				return true;
			}

			rep(dirr, 4) {
				int dir = (dirr + 1) % 6;
				REP(dist, 1, 16) {
					Vector3D next(now.x + rx[dir] * dist, now.y + ry[dir] * dist, now.z + rz[dir] * dist);
					if (!check(next)) break;
					if (used[next.x][next.y][next.z]) break;
					if (matrix[next.x][next.y][next.z] || visited.count(next)) break;
					visited.insert(next);

					if (turn == 95 && bot.id == 2) {
						Vector3D hoge = next - now;
					}

					q.push(make_pair(next, info.second.x == -100 ? next - now : info.second));
				}
			}
		}

		return false;
	}

	/**
	 * lmove した結果の Position を返す
	 */
	pair<bool, Vector3D> lmoveResult(Bot bot, int dir1, int dist1, int dir2, int dist2) {
		if(abs(dist1) > 5 || dist1 == 0) return make_pair(false, Vector3D());
		if(abs(dist2) > 5 || dist2 == 0) return make_pair(false, Vector3D());

		Vector3D from = bot.pos;
		bot.pos.move(dir1, dist1 - (dist1 > 0 ? 1 : -1));
		bot.pos.move(dir1, (dist1 > 0 ? 1 : -1));
		if(!check(bot.pos)) return make_pair(false, Vector3D());

		from = bot.pos;
		bot.pos.move(dir2, dist2);
		if(!check(bot.pos)) return make_pair(false, Vector3D());

		return make_pair(true, bot.pos);
	};


	void lmove(Bot &bot, int dir1, int dist1, int dir2, int dist2) {
		assert(abs(dist1) <= 5 && dist1 != 0);
		assert(abs(dist2) <= 5 && dist2 != 0);
		Vector3D from = bot.pos;
		bot.pos.move(dir1, dist1 - (dist1 > 0 ? 1 : -1));
		bulkUse(bot.pos, from, bot.id);
		bot.pos.move(dir1, (dist1 > 0 ? 1 : -1));
		assert(check(bot.pos));

		from = bot.pos;
		bot.pos.move(dir2, dist2);
		assert(check(bot.pos));
		bulkUse(bot.pos, from, bot.id);

		energy += 2 * (abs(dist1) + 2 + abs(dist2));
		trace.append(new LmoveCommand(bot.id, Vector3D(dir1, dist1), Vector3D(dir2, dist2)));
	}

	bool canFission(Bot bot, Vector3D diff, int m) {
		if (!(0 <= m && m <= bot.seeds.size() - 1)) return false;

		// 分裂後のposを更新
		Bot nBot = bots[*bot.seeds.begin() - 1];
		nBot.pos = bot.pos;
		nBot.pos.move(diff);
		bot.seeds.erase(bot.seeds.begin());
		if (!check(nBot.pos))return false;

		if (!canUse(bot.pos, bot.id)) return false;
		if (!canUse(nBot.pos, nBot.id)) return false;

		return true;
	}

	void fission(Bot &bot, Vector3D diff, int m) {
		assert(0 <= m && m <= bot.seeds.size() - 1);

		// 分裂後のposを更新
		
		
		Bot &nBot = bots[*bot.seeds.begin() - 1];
		nBot.pos = bot.pos;
		nBot.pos.move(diff);
		nextActivateBid.push_back(nBot.id);
		bot.seeds.erase(bot.seeds.begin());
		assert(check(nBot.pos));

		use(bot.pos, bot.id);
		use(nBot.pos, nBot.id);

		// seedの更新
		nBot.seeds.clear();
		rep(i, m) {
			nBot.seeds.insert(*bot.seeds.begin());
			bot.seeds.erase(bot.seeds.begin());
		}

		energy += 24;

		trace.append(new FissionCommand(bot.id, diff, m));
	}

	void fusion(Bot &botP, Bot &botS) {
		nextInactivateBid.push_back(botS.id);

		// seedのマージ
		int m = botS.seeds.size();
		rep(i, m) {
			botP.seeds.insert(*botS.seeds.begin());
			botS.seeds.erase(botS.seeds.begin());
		}
		botP.seeds.insert(botS.id);

		use(botP.pos, botP.id);
		use(botS.pos, botS.id);

		energy -= 24;

		auto diffP = Vector3D(botS.pos, botP.pos);
		auto diffS = Vector3D(botP.pos, botS.pos);
		trace.append(new FusionPCommand(botP.id, diffP, botS.id));
		trace.append(new FusionSCommand(botS.id, diffS, botP.id));
	}

	int canFill(Vector3D pos, Vector3D diff, int bid) {
		if(!isNear(diff)) return 0;
		Vector3D _pos = pos;
		pos.move(diff);
		if(!check(pos)) return 0;
		if(!model[pos.x][pos.y][pos.z]) return 0;
		if (!canUse(pos, bid)) return 0;
		if (!canUse(_pos, bid)) return 0;

		if (harmonics) return 1;
		if (_isGround(pos.x, pos.y, pos.z)) return 2;
	}


	void fill(Bot &bot, Vector3D diff) {
		assert(isNear(diff));
		Vector3D pos = bot.pos;
		pos.move(diff);
		assert(check(pos));
		assert(model[pos.x][pos.y][pos.z]);
		use(bot.pos, bot.id);
		use(pos, bot.id);
		matrix[pos.x][pos.y][pos.z] = true;
		trace.append(new FillCommand(bot.id, diff));

		setGround(pos.x, pos.y, pos.z);
		assert(harmonics || grounded[pos.x][pos.y][pos.z]);
	}


	bool canVoid(Bot bot, Vector3D diff) {
		// TODO: Voidメソッドの実装
		if (!isNear(diff)) return false;

		Vector3D pos = bot.pos;
		pos.move(diff);
		if(!check(pos)) return false;
		if(!matrix[pos.x][pos.y][pos.z]) return false;
		if (!canUse(bot.pos, bot.id)) return false;
		if (!canUseVoidVersion(pos, bot.id)) return false;

		return true;
	}

	void Void(Bot &bot, Vector3D diff) {
		// TODO: Voidメソッドの実装
		assert(isNear(diff));

		Vector3D pos = bot.pos;
		pos.move(diff);
		assert(check(pos));
		assert(matrix[pos.x][pos.y][pos.z]);
		use(bot.pos, bot.id);
		useVoidVersion(pos, bot.id);
		matrix[pos.x][pos.y][pos.z] = false;
		trace.append(new VoidCommand(bot.id, diff));

		// groundedの更新
		grounded[pos.x][pos.y][pos.z] = false;
		map<Vector3D, bool> visited;
		rep(dir, 6) {
			Vector3D npos = pos;
			npos.x += rx[dir];
			npos.y += ry[dir];
			npos.z += rz[dir];

			if(!check(npos)) continue;
			if(!grounded[npos.x][npos.y][npos.z]) continue;
			dfsToFloor(npos, visited);
		}

		energy -= 12;
	}

	void GFillLine(Bot &bot1, Bot &bot2, Vector3D nd1, Vector3D nd2) {
		assert(isNear(nd1));
		assert(isNear(nd2));

		Vector3D pos1 = bot1.pos;
		pos1.move(nd1);
		assert(check(pos1));

		Vector3D pos2 = bot2.pos;
		pos2.move(nd2);
		assert(check(pos2));

		Vector3D fd1 = Vector3D(- bot1.pos.x - nd1.x + pos2.x, - bot1.pos.y - nd1.y + pos2.y, - bot1.pos.z - nd1.z + pos2.z);
		Vector3D fd2 = Vector3D(- bot2.pos.x - nd2.x + pos1.x, - bot2.pos.y - nd2.y + pos1.y, - bot2.pos.z - nd2.z + pos1.z);

		assert(isFar(fd1));
		assert(isFar(fd2));
		assert(pos1.x == pos2.x || pos1.y == pos2.y || pos1.z == pos2.z);

		Vector3D diff(pos1.x - pos2.x, pos1.y - pos2.y, pos1.z - pos2.z);
		assert(diff.dimension() == 1);

		use(bot1.pos, bot1.id);
		use(bot2.pos, bot2.id);
		bulkUse(pos1, pos2, bot1.id); // TODO bot2 の bid も入れるようにする

		int minx = min(pos1.x, pos2.x);
		int maxx = max(pos1.x, pos2.x);
		int miny = min(pos1.y, pos2.y);
		int maxy = max(pos1.y, pos2.y);
		int minz = min(pos1.z, pos2.z);
		int maxz = max(pos1.z, pos2.z);

		REP(x, minx, maxx + 1) REP(y, miny, maxy + 1) REP(z, minz, maxz + 1) {
			if (matrix[x][y][z]) {
				energy += 6;
			} else {
				energy += 12;
				matrix[x][y][z] = true;
			}
			setGround(x, y, z);
		}

		trace.append(new GFillCommand(bot1.id, nd1, fd1));
		trace.append(new GFillCommand(bot2.id, nd2, fd2));
	}

	// bot1とbot3、bot2とbot4 がそれぞれ対角上
	void GFillPlane(Bot &bot1, Bot &bot2, Bot &bot3, Bot &bot4, Vector3D nd1, Vector3D nd2, Vector3D nd3, Vector3D nd4) {
		assert(isNear(nd1));
		assert(isNear(nd2));
		assert(isNear(nd3));
		assert(isNear(nd4));

		Vector3D pos1 = bot1.pos;
		pos1.move(nd1);
		assert(check(pos1));

		Vector3D pos2 = bot2.pos;
		pos2.move(nd2);
		assert(check(pos2));

		Vector3D pos3 = bot3.pos;
		pos3.move(nd3);
		assert(check(pos3));

		Vector3D pos4 = bot4.pos;
		pos4.move(nd4);
		assert(check(pos4));

		Vector3D fd1 = Vector3D(- bot1.pos.x - nd1.x + pos3.x, - bot1.pos.y - nd1.y + pos3.y, - bot1.pos.z - nd1.z + pos3.z);
		Vector3D fd2 = Vector3D(- bot2.pos.x - nd2.x + pos4.x, - bot2.pos.y - nd2.y + pos4.y, - bot2.pos.z - nd2.z + pos4.z);
		Vector3D fd3 = Vector3D(- bot3.pos.x - nd3.x + pos1.x, - bot3.pos.y - nd3.y + pos1.y, - bot3.pos.z - nd3.z + pos1.z);
		Vector3D fd4 = Vector3D(- bot4.pos.x - nd4.x + pos2.x, - bot4.pos.y - nd4.y + pos2.y, - bot4.pos.z - nd4.z + pos1.z);

		assert(isFar(fd1));
		assert(isFar(fd2));
		assert(isFar(fd3));
		assert(isFar(fd4));
		assert(pos1.x == pos2.x && pos2.x == pos3.x && pos3.x == pos4.x
			   || pos1.y == pos2.y && pos2.y == pos3.y && pos3.y == pos4.y
			   || pos1.z == pos2.z && pos2.z == pos3.z && pos3.z == pos4.z);

		assert((pos1 - pos2).dimension() == 1);
		assert((pos1 - pos3).dimension() == 2);
		assert((pos1 - pos4).dimension() == 1);
		assert((pos2 - pos3).dimension() == 1);
		assert((pos2 - pos4).dimension() == 2);
		assert((pos3 - pos4).dimension() == 1);

		use(bot1.pos, bot1.id);
		use(bot2.pos, bot2.id);
		use(bot3.pos, bot3.id);
		use(bot4.pos, bot4.id);
		bulkUse(pos1, pos3, bot1.id);

		int minx = min(pos1.x, pos3.x);
		int maxx = max(pos1.x, pos3.x);
		int miny = min(pos1.y, pos3.y);
		int maxy = max(pos1.y, pos3.y);
		int minz = min(pos1.z, pos3.z);
		int maxz = max(pos1.z, pos3.z);

		REP(x, minx, maxx + 1) REP(y, miny, maxy + 1) REP(z, minz, maxz + 1) {
			if (matrix[x][y][z]) {
				energy += 6;
			} else {
				energy += 12;
				matrix[x][y][z] = true;
			}
			setGround(x, y, z);
		}

		trace.append(new GFillCommand(bot1.id, nd1, fd1));
		trace.append(new GFillCommand(bot2.id, nd2, fd2));
		trace.append(new GFillCommand(bot3.id, nd3, fd3));
		trace.append(new GFillCommand(bot4.id, nd4, fd4));
	}

	// bot番号の対応 1-2, 3-4, 5-6, 7-8
	void GFillBox(Bot &bot1, Bot &bot2, Bot &bot3, Bot &bot4, Bot &bot5, Bot &bot6, Bot &bot7, Bot &bot8,
					Vector3D nd1, Vector3D nd2, Vector3D nd3, Vector3D nd4, Vector3D nd5, Vector3D nd6, Vector3D nd7, Vector3D nd8) {
		assert(isNear(nd1));
		assert(isNear(nd2));
		assert(isNear(nd3));
		assert(isNear(nd4));
		assert(isNear(nd5));
		assert(isNear(nd6));
		assert(isNear(nd7));
		assert(isNear(nd8));

		Vector3D pos1 = bot1.pos;
		pos1.move(nd1);
		assert(check(pos1));

		Vector3D pos2 = bot2.pos;
		pos2.move(nd2);
		assert(check(pos2));

		Vector3D pos3 = bot3.pos;
		pos3.move(nd3);
		assert(check(pos3));

		Vector3D pos4 = bot4.pos;
		pos4.move(nd4);
		assert(check(pos4));

		Vector3D pos5 = bot5.pos;
		pos5.move(nd5);
		assert(check(pos5));

		Vector3D pos6 = bot6.pos;
		pos6.move(nd6);
		assert(check(pos6));

		Vector3D pos7 = bot7.pos;
		pos7.move(nd7);
		assert(check(pos7));

		Vector3D pos8 = bot8.pos;
		pos8.move(nd8);
		assert(check(pos8));

		Vector3D fd1 = Vector3D(- bot1.pos.x - nd1.x + pos2.x, - bot1.pos.y - nd1.y + pos2.y, - bot1.pos.z - nd1.z + pos2.z);
		Vector3D fd2 = Vector3D(- bot2.pos.x - nd2.x + pos1.x, - bot2.pos.y - nd2.y + pos1.y, - bot2.pos.z - nd2.z + pos1.z);
		Vector3D fd3 = Vector3D(- bot3.pos.x - nd3.x + pos4.x, - bot3.pos.y - nd3.y + pos4.y, - bot3.pos.z - nd3.z + pos4.z);
		Vector3D fd4 = Vector3D(- bot4.pos.x - nd4.x + pos3.x, - bot4.pos.y - nd4.y + pos3.y, - bot4.pos.z - nd4.z + pos3.z);
		Vector3D fd5 = Vector3D(- bot5.pos.x - nd5.x + pos6.x, - bot5.pos.y - nd5.y + pos6.y, - bot5.pos.z - nd5.z + pos6.z);
		Vector3D fd6 = Vector3D(- bot6.pos.x - nd6.x + pos5.x, - bot6.pos.y - nd6.y + pos5.y, - bot6.pos.z - nd6.z + pos5.z);
		Vector3D fd7 = Vector3D(- bot7.pos.x - nd7.x + pos8.x, - bot7.pos.y - nd7.y + pos8.y, - bot7.pos.z - nd7.z + pos8.z);
		Vector3D fd8 = Vector3D(- bot8.pos.x - nd8.x + pos7.x, - bot8.pos.y - nd8.y + pos7.y, - bot8.pos.z - nd8.z + pos7.z);

		assert(isFar(fd1));
		assert(isFar(fd2));
		assert(isFar(fd3));
		assert(isFar(fd4));
		assert(isFar(fd5));
		assert(isFar(fd6));
		assert(isFar(fd7));
		assert(isFar(fd8));

		use(bot1.pos, bot1.id);
		use(bot2.pos, bot2.id);
		use(bot3.pos, bot3.id);
		use(bot4.pos, bot4.id);
		use(bot5.pos, bot5.id);
		use(bot6.pos, bot6.id);
		use(bot7.pos, bot7.id);
		use(bot8.pos, bot8.id);
		bulkUse(pos1, pos2, bot1.id);

		int minx = min(pos1.x, pos2.x);
		int maxx = max(pos1.x, pos2.x);
		int miny = min(pos1.y, pos2.y);
		int maxy = max(pos1.y, pos2.y);
		int minz = min(pos1.z, pos2.z);
		int maxz = max(pos1.z, pos2.z);

		REP(x, minx, maxx + 1) REP(y, miny, maxy + 1) REP(z, minz, maxz + 1) {
			if (matrix[x][y][z]) {
				energy += 6;
			} else {
				energy += 12;
				matrix[x][y][z] = true;
			}
			setGround(x, y, z);
		}

		trace.append(new GFillCommand(bot1.id, nd1, fd1));
		trace.append(new GFillCommand(bot2.id, nd2, fd2));
		trace.append(new GFillCommand(bot3.id, nd3, fd3));
		trace.append(new GFillCommand(bot4.id, nd4, fd4));
		trace.append(new GFillCommand(bot5.id, nd5, fd5));
		trace.append(new GFillCommand(bot6.id, nd6, fd6));
		trace.append(new GFillCommand(bot7.id, nd7, fd7));
		trace.append(new GFillCommand(bot8.id, nd8, fd8));
	}

	void GVoidLine(Bot &bot1, Bot &bot2, Vector3D nd1, Vector3D nd2) {
		assert(isNear(nd1));
		assert(isNear(nd2));

		Vector3D pos1 = bot1.pos;
		pos1.move(nd1);

		Vector3D pos2 = bot2.pos;
		pos2.move(nd2);

		assert(pos1 == pos2);

		Vector3D fd1 = pos2 - pos1;
		Vector3D fd2 = pos1 - pos2;

		assert(isFar(fd1));
		assert(isFar(fd2));

		int minX, minY, minZ;
		int maxX, maxY, maxZ;
		tie(minX, minY, minZ, maxX, maxY, maxZ) = getMinMax({pos1, pos2});		
		
		use(bot1.pos, bot1.id);
		use(bot2.pos, bot2.id);

		updateOutsideGrounded(Vector3D(minX, minY, minZ), Vector3D(maxX, maxY, maxZ), bot1.id);

		trace.append(new GVoidCommand(bot1.id, nd1, fd1));
		trace.append(new GVoidCommand(bot2.id, nd2, fd2));
	}

	// botはbot1とbor2, bot3とbot4がそれぞれ対角線的に対応する
	bool canGVoidPlane(Bot bot1, Bot bot2, Bot bot3, Bot bot4, Vector3D nd1, Vector3D nd2, Vector3D nd3, Vector3D nd4) {
		if(!isNear(nd1)) return false;
		if(!isNear(nd2)) return false;
		if(!isNear(nd3)) return false;
		if(!isNear(nd4)) return false;

		Vector3D pos1 = bot1.pos;
		pos1.move(nd1);

		Vector3D pos2 = bot2.pos;
		pos2.move(nd2);

		Vector3D pos3 = bot3.pos;
		pos3.move(nd3);

		Vector3D pos4 = bot4.pos;
		pos4.move(nd4);

		//assert(pos1 == pos2);
		//assert(pos3 == pos4);

		Vector3D fd1 = pos2 - pos1;
		Vector3D fd2 = pos1 - pos2;
		Vector3D fd3 = pos4 - pos3;
		Vector3D fd4 = pos3 - pos4;

		if (!isFar(fd1))return false;
		if (!isFar(fd2)) return false;
		if (!isFar(fd3)) return false;
		if (!isFar(fd4)) return false;;

		int minX, minY, minZ;
		int maxX, maxY, maxZ;
		tie(minX, minY, minZ, maxX, maxY, maxZ) = getMinMax({ pos1, pos2, pos3, pos4 });

		if (!canUseVoidVersion(bot1.pos, bot1.id))return false;
		if (!canUseVoidVersion(bot2.pos, bot2.id))return false;
		if (!canUseVoidVersion(bot3.pos, bot3.id))return false;
		if (!canUseVoidVersion(bot4.pos, bot4.id))return false;
		
		// TODO 2, 3, 4 も bid を渡すようにする
		if (harmonics) return true;

		static int pre[250][250][250];
		clr(pre);
		for (int x = minX; x <= maxX; x++) {
			for (int y = minY; y <= maxY; y++) {
				for (int z = minZ; z <= maxZ; z++) {
					pre[x][y][z] = matrix[x][y][z];
					matrix[x][y][z] = false;
				}
			}
		}

		bool r = canLowHarmonicsSimpleHonesty();

		for (int x = minX; x <= maxX; x++) {
			for (int y = minY; y <= maxY; y++) {
				for (int z = minZ; z <= maxZ; z++) {
					matrix[x][y][z] = pre[x][y][z];
				}
			}
		}

		return r;
	}


	bool canLowHarmonicsSimpleHonesty() {

		typedef Vector3D V;
		queue<V> q;

		static int vis[251][251][251];
		clr(vis);

		int count2 = 0;
		rep(z, R) rep(x, R) {
			if (matrix[x][0][z]) {
				q.push(V(x, 0, z));
				vis[x][0][z] = 1;
				count2++;
			}
		}

		int count = 0;
		rep(z, R) rep(y, R) rep(x, R) {
			if (matrix[x][y][z]) {
				count++;
			}
		}

		while (q.size()) {

			auto p = q.front();
			q.pop();

			int x = p.x;
			int y = p.y;
			int z = p.z;

			rep(dir, 6) {
				int tx = x + rx[dir];
				int ty = y + ry[dir];
				int tz = z + rz[dir];

				if (check(V(tx, ty, tz)) && !vis[tx][ty][tz] && matrix[tx][ty][tz]) {
					vis[tx][ty][tz] = 1;
					count2++;
					q.push(V(tx, ty, tz));
				}
			}

		}
		return count == count2;

	}


	// botはbot1とbor2, bot3とbot4がそれぞれ対角線的に対応する
	void GVoidPlane(Bot &bot1, Bot &bot2, Bot &bot3, Bot &bot4, Vector3D nd1, Vector3D nd2, Vector3D nd3, Vector3D nd4) {
		assert(isNear(nd1));
		assert(isNear(nd2));
		assert(isNear(nd3));
		assert(isNear(nd4));

		Vector3D pos1 = bot1.pos;
		pos1.move(nd1);

		Vector3D pos2 = bot2.pos;
		pos2.move(nd2);

		Vector3D pos3 = bot3.pos;
		pos3.move(nd3);

		Vector3D pos4 = bot4.pos;
		pos4.move(nd4);

		//assert(pos1 == pos2);
		//assert(pos3 == pos4);

		Vector3D fd1 = pos2 - pos1;
		Vector3D fd2 = pos1 - pos2;
		Vector3D fd3 = pos4 - pos3;
		Vector3D fd4 = pos3 - pos4;

		assert(isFar(fd1));
		assert(isFar(fd2));
		assert(isFar(fd3));
		assert(isFar(fd4));

		int minX, minY, minZ;
		int maxX, maxY, maxZ;
		tie(minX, minY, minZ, maxX, maxY, maxZ) = getMinMax({pos1, pos2, pos3, pos4});		
		
		useVoidVersion(bot1.pos, bot1.id);
		useVoidVersion(bot2.pos, bot2.id);
		useVoidVersion(bot3.pos, bot3.id);
		useVoidVersion(bot4.pos, bot4.id);

		for (int x = minX; x <= maxX; x++) {
			for (int y = minY; y <= maxY; y++) {
				for (int z = minZ; z <= maxZ; z++) {
					matrix[x][y][z] = false;
				}
			}
		}
			
		updateOutsideGrounded(Vector3D(minX, minY, minZ), Vector3D(maxX, maxY, maxZ), bot1.id); // TODO 2, 3, 4 も bid を渡すようにする

		trace.append(new GVoidCommand(bot1.id, nd1, fd1));
		trace.append(new GVoidCommand(bot2.id, nd2, fd2));
		trace.append(new GVoidCommand(bot3.id, nd3, fd3));
		trace.append(new GVoidCommand(bot4.id, nd4, fd4));
	}

	// bot番号の対応 1-2, 3-4, 5-6, 7-8
	void GVoidbox(Bot &bot1, Bot &bot2, Bot &bot3, Bot &bot4, Bot &bot5, Bot &bot6, Bot &bot7, Bot &bot8,
					Vector3D nd1, Vector3D nd2, Vector3D nd3, Vector3D nd4, Vector3D nd5, Vector3D nd6, Vector3D nd7, Vector3D nd8) {
		assert(isNear(nd1));
		assert(isNear(nd2));
		assert(isNear(nd3));
		assert(isNear(nd4));
		assert(isNear(nd5));
		assert(isNear(nd6));
		assert(isNear(nd7));
		assert(isNear(nd8));

		Vector3D pos1 = bot1.pos;
		pos1.move(nd1);

		Vector3D pos2 = bot2.pos;
		pos2.move(nd2);

		Vector3D pos3 = bot3.pos;
		pos3.move(nd3);

		Vector3D pos4 = bot4.pos;
		pos4.move(nd4);

		Vector3D pos5 = bot5.pos;
		pos5.move(nd5);

		Vector3D pos6 = bot6.pos;
		pos6.move(nd6);

		Vector3D pos7 = bot7.pos;
		pos7.move(nd7);

		Vector3D pos8 = bot8.pos;
		pos8.move(nd8);

//		assert(pos1 == pos2);
//		assert(pos3 == pos4);
//		assert(pos5 == pos6);
//		assert(pos7 == pos8);

		Vector3D fd1 = pos2 - pos1;
		Vector3D fd2 = pos1 - pos2;
		Vector3D fd3 = pos4 - pos3;
		Vector3D fd4 = pos3 - pos4;
		Vector3D fd5 = pos6 - pos5;
		Vector3D fd6 = pos5 - pos6;
		Vector3D fd7 = pos8 - pos7;
		Vector3D fd8 = pos7 - pos8;
		
		assert(isFar(fd1));
		assert(isFar(fd2));
		assert(isFar(fd3));
		assert(isFar(fd4));
		assert(isFar(fd5));
		assert(isFar(fd6));
		assert(isFar(fd7));
		assert(isFar(fd8));

		int minX, minY, minZ;
		int maxX, maxY, maxZ;
		tie(minX, minY, minZ, maxX, maxY, maxZ) = getMinMax({pos1, pos2, pos3, pos4, pos5, pos6, pos7, pos8});		
		
		use(bot1.pos, bot1.id);
		use(bot2.pos, bot2.id);
		use(bot3.pos, bot3.id);
		use(bot4.pos, bot4.id);
		use(bot5.pos, bot5.id);
		use(bot6.pos, bot6.id);
		use(bot7.pos, bot7.id);
		use(bot8.pos, bot8.id);

		updateOutsideGrounded(Vector3D(minX, minY, minZ), Vector3D(maxX, maxY, maxZ), bot1.id); // TODO 2,3,4,5,6,7,8, の bid も入れるようにする

		for (int x = minX; x <= maxX; x++) {
			for (int y = minY; y <= maxY; y++) {
				for (int z = minZ; z <= maxZ; z++) {
					matrix[x][y][z] = false;
				}
			}
		}

		trace.append(new GVoidCommand(bot1.id, nd1, fd1));
		trace.append(new GVoidCommand(bot2.id, nd2, fd2));
		trace.append(new GVoidCommand(bot3.id, nd3, fd3));
		trace.append(new GVoidCommand(bot4.id, nd4, fd4));
		trace.append(new GVoidCommand(bot5.id, nd5, fd5));
		trace.append(new GVoidCommand(bot6.id, nd6, fd6));
		trace.append(new GVoidCommand(bot7.id, nd7, fd7));
		trace.append(new GVoidCommand(bot8.id, nd8, fd8));
	}

	bool isNear(Vector3D diff) {
		int mdist = mlen(diff);
		return mdist > 0 && mdist <= 2 && clen(diff) == 1;
	}

	bool isFar(Vector3D &diff) {
		int cdist = clen(diff);
		return cdist > 0 && cdist <= 30;
	}

	bool check(Vector3D pos) {
		return check(pos.x, pos.y, pos.z);
	}

	bool check(int x, int y, int z) {
		return x >= 0 && x < R && y >= 0 && y < R && z >= 0 && z < R;
	}

	bool check(Vector3D pos, Vector3D minPos, Vector3D maxPos) {
		return minPos.x <= pos.x && pos.x <= maxPos.x
			&& minPos.y <= pos.y && pos.y <= maxPos.y
			&& minPos.z <= pos.z && pos.z <= maxPos.z;
	}

	tuple<int, int, int, int, int, int> getMinMax(vector<Vector3D> vec){
		int minX = 1e9;
		int minY = 1e9;
		int minZ = 1e9;
		int maxX = 0;
		int maxY = 0;
		int maxZ = 0;
		each(p, vec) {
			minX = min(minX, p.x);
			minY = min(minY, p.y);
			minZ = min(minZ, p.z);
			maxX = max(maxX, p.x);
			maxY = max(maxY, p.y);
			maxZ = max(maxZ, p.z);
		}
		return tuple<int, int, int, int, int, int>(minX, minY, minZ, maxX, maxY, maxZ);
	}

	bool _isGround(int tx, int ty, int tz) {
		// 周囲に grounded セルがあるか確認、なければ終了
		bool ground = false;
		rep(dir, 6) {
			int nx = tx + rx[dir];
			int ny = ty + ry[dir];
			int nz = tz + rz[dir];
			if (!(check(nx, ny, nz) || check(nx, ny + 1, nz))) continue;
			if (isGround(nx, ny, nz)) {
				ground = true;
				break;
			}
		}
		if (!ground) return false;

		return true;
	}


	int mlen(Vector3D diff) {
		return abs(diff.x) + abs(diff.y) + abs(diff.z);
	}

	int clen(Vector3D diff) {
		return max(abs(diff.x), max(abs(diff.y), abs(diff.z)));
	}

	void beforeTurn() {

		isWellFormed();

		if (harmonics) {
			energy += 30 * R * R * R;
		}
		else {
			energy += 3 * R * R * R;
		}

		int numOfActiveBots = accumulate(bots.begin(), bots.end(), 0, [](int x, Bot bot){ return x + bot.isActive; });
		energy += 20 * numOfActiveBots;

		each(pos, usedPos) {
			used[pos.x][pos.y][pos.z] = false;
		}

		usedPos.clear();

		turn++;
	}

	void afterTurn() {
		trace.output(turn, energy, harmonics, bots);

		each(bid, nextActivateBid) {
			this->bots[bid-1].isActive = true;
		}
		nextActivateBid.clear();
		each(bid, nextInactivateBid) {
			this->bots[bid-1].isActive = false;
		}
		nextInactivateBid.clear();

		sort(all(this->bots), [](const Bot &a, const Bot &b) {
			if (a.isActive != b.isActive) return a.isActive > b.isActive;
			return a.id < b.id;
		});

		rep(i, this->bots.size()) {
			this->bots[i].id = i + 1;
		}

	}

};


bool System::matrix[250][250][250]; // false->Empty, true->Full
bool System::model[250][250][250];
int System::used[250][250][250];
bool System::grounded[250][250][250];
int System::modelCm[251][251][251];

int read_mdl(const char *file_name, int mode) {
	ifstream f = ifstream(file_name, ios::in | ios::binary);

	if (!f) {
		cerr << "ファイルが開けません" << endl;
		return 0;
	}

	char c;
	int r;
	f.read(&c, 1);
	unsigned char c2 = c;
	r = c2;
	int x = 0, y = 0, z = 0;
	for (int i = 0; i < r * r * r; i++) {
		if (i % 8 == 0) {
			f.read(&c, 1);
		}
		// メモリ節約のため直接書き込む
		if (mode == 0) {
			System::model[x][y][z] = (c & (1 << i % 8)) != 0;
		}
		if (mode == 1) {
			System::matrix[x][y][z] = (c & (1 << i % 8)) != 0;
		}
		if (mode == 2) {
			//TODO Reassembly Problems model と matrix の両方を読み込む必要がある
		}

		z++;
		if (z == r) {
			z = 0;
			y++;
			if (y == r) {
				y = 0;
				x++;
			}
		}
	}
	return r;
}

#endif //R_OCHI_SYSTEM_H
