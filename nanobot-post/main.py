from zipfile import ZipFile, ZIP_DEFLATED
from pathlib import Path
import hashlib
import json
import time

import requests


url = 'http://ec2-52-69-100-161.ap-northeast-1.compute.amazonaws.com/submit'

targets = [
    {
        'name': 'ochi',
        'type': 'A',
        'src': [
            ('../r-ochi/main.cpp', 'src/main.cpp'),
            ('../lib', 'lib'),
            ('template/run.sh', 'run.sh'),
            ('template/compile.sh', 'compile.sh'),
        ],
    },
    {
        'name': 'inoue',
        'type': 'A',
        'src': [
            ('../t-inoue/main.cpp', 'src/main.cpp'),
            ('../lib', 'lib'),
            ('template/run.sh', 'run.sh'),
            ('template/compile.sh', 'compile.sh'),
        ],
    },
    {
        'name': 'ochi',
        'type': 'D',
        'src': [
            ('../r-ochi_dis/main.cpp', 'src/main.cpp'),
            ('../lib', 'lib'),
            ('template/run.sh', 'run.sh'),
            ('template/compile.sh', 'compile.sh'),
        ],
    },
    {
        'name': 'omu',
        'type': 'D',
        'src': [
            ('../a-omura/main_dis.cpp', 'src/main.cpp'),
            ('../lib', 'lib'),
            ('template/run.sh', 'run.sh'),
            ('template/compile.sh', 'compile.sh'),
        ],
    },
]


def make_zip(info):
    zip_file = Path(info['name'] + '-' + info['type'] + '.zip')
    with ZipFile(zip_file, mode='w', compression=ZIP_DEFLATED) as z:
        for src, arc in info['src']:
            src = Path(src)
            if src.is_dir():
                for s in src.glob('*'):
                    z.write(s, arcname=Path(arc) / s.name)
            else:
                z.write(src, arcname=arc)
    return zip_file


def load_post():
    try:
        return json.load(open('post.json'))
    except IOError:
        return {}


def post(info, zip_file):
    data = zip_file.open('rb').read()
    m = hashlib.sha256()
    m.update(data)
    h = m.hexdigest()
    post_log = load_post()

    if h in post_log.get(str(zip_file), []):
        print('skip', zip_file)
        return
    post_log[str(zip_file)] = post_log.get(str(zip_file), []) + [h]

    print('post', str(zip_file))
    r = requests.post(url,
                      files={'file': ('submit.zip', data, 'application/zip')},
                      params={'type': info['type'], 'name': info['name']})
    print(r.json())
    r = requests.post(url,
                      files={'file': ('submit.zip', data, 'application/zip')},
                      params={'type': 'R' + info['type'], 'name': info['name']})
    print(r.json())

    json.dump(post_log, open('post.json', 'w'), indent=4, ensure_ascii=False)


def main():
    for info in targets:
        zip_file = make_zip(info)
        post(info, zip_file)
        time.sleep(2)


if __name__ == '__main__':
    main()
