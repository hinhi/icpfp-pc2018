# Task Description (Full)
Thank you for purchasing a Deluxe Nanobot Matter Manipulation System (NMMS). Enclosed you will find one matter subspace pad and one fission/fusion-capable nanobot. Simply position an optional source object on the matter space pad, load an appropriate trace, place the nanobot at the origin of the pad, power on the system, and watch the nanobot begin construction, deconstruction, or reconstruction.

Deluxe Nanobot Matter Manipulation System（NMMS）をお買い上げいただきありがとうございます。 1つの物質の部分空間パッドと1つの核分裂/融合可能なナノボットがあります。 マテリアルスペースパッドに任意のソースオブジェクトを配置し、適切なトレースをロードし、パッドの原点にナノボットを置き、システムの電源を入れ、ナノボットが構築、分解または再構築を開始するのを見るだけです。

In comparison to the Standard Nanobot Matter Manipulation System, the Deluxe Nanobot Matter Manipulation System:

Standard Nanobot Matter Manipulation SystemとDeluxe Nanobot Matter Manipulation Systemの比較：

Supports destroying matter in addition to creating matter; thus, your Deluxe Nanobot Matter Manipulation System can be used to construct target objects, destruct source objects, and reconstruct a source object into a target object.
Has three new nanobot commands: Void, GFill, and GVoid
The Void command allows a nanobot to destroy matter in a voxel.
The GFill and GVoid commands allow a group of nanobots to create or destroy matter in a region.
Initializes the nanobot with 39 seeds.
As an early adopter, please understand the shortage of freely available nanobot traces. Although your Deluxe Nanobot Matter Manipulation System ships with default nanobot traces for constructing, destructing, and reconstructing target models, they are extremely energy inefficient. We hope that you will enjoy generating your own nanobot traces and will contribute energy efficient ones back to the community.


問題を作成することに加えて、問題を破壊することをサポートします。したがって、あなたのDeluxe Nanobot Matter Manipulation Systemを使用してターゲットオブジェクトを構築し、ソースオブジェクトを破壊し、ソースオブジェクトをターゲットオブジェクトに再構築することができます。
3つの新しいナノボットコマンドがあります：Void、GFill、およびGVoid
Voidコマンドは、ナノボットがボクセル内の物質を破壊することを可能にする。
GFillコマンドとGVoidコマンドを使用すると、ナノボットのグループがリージョン内の問題を作成または破棄できます。
39種子でナノボットを初期化します。
早期採用者としては、自由に入手できるナノボットのトレースの不足を理解してください。あなたのDeluxe Nanobot Matter Manipulation Systemはターゲットモデルを構築、破壊、再構築するためのデフォルトのナノボットトレースを添付していますが、非常にエネルギー効率が悪いです。独自のナノボットの痕跡を楽しんでいただき、エネルギー効率の良いものをコミュニティに還元することを願っています。


## Synopsis 筋書き
Generate nanobot traces to construct, deconstruct, and reconstruct target 3D objects while minimizing energy used.

使用されるエネルギーを最小限に抑えながら、ターゲット3Dオブジェクトを構築、分解、再構築するためにナノボットトレースを生成します。

## Nanobot Matter Manipulation System Overview ナノボットマター操作システムの概要
Build-a-Tron 4000 The initial nanobot is affectionately known as the Build-a-Tron 4000.

Build-a-Tron 4000最初のナノボットはBuild-a-Tron 4000として親しまれています。

The Nanobot Matter Manipulation System is a breakthrough technology that enables a new form of 3D printing. The matter subspace pad utilizes advances in subspace physics to facilitate the light-weight conversion of energy to matter and vice versa. During execution, the pad generates a (global) resonant subspace field that establishes a matrix of voxels in which matter can be created and destroyed. The field holds matter at its fixed position in the matrix; under low harmonics, all matter must be part of a connected component that rests on the floor (“grounded”), while, under high harmonics, matter is unconstrained (“floating”). For precision construction, nanobots are used to focus the resonant subspace field during energy-matter conversion, either creating matter within a voxel or destroying matter within a voxel. Nanobots are able to move through empty voxels of the matrix; utilizing a local high-harmonics resonant subspace field, a nanobot’s position is unconstrained (“floating”). Nanobots are able to undergo fission (to fork off another nanobot) and fusion (to join with another nanobot).

Nanobot Matter Manipulation Systemは、新しい形の3D印刷を可能にする画期的な技術です。事象部分空間パッドは、部分空間物理学における進歩を利用して、エネルギーを物質に軽量に変換することを容易にし、逆もまた同様である。実行中、パッドは、物質が生成され、破壊されるボクセルのマトリックスを確立する（グローバル）共鳴部分空間フィールドを生成する。フィールドは行列の固定された位置に問題を保持します。低調波下では、すべての事項は、高調波の下では問題が制約されていない（「浮動」している）間に、床に置かれた（「接地された」）接続されたコンポーネントの一部でなければなりません。精密構造のために、ボクセル内の物質を生成するか、ボクセル内の物質を破壊するエネルギー物質変換中に共振部分空間フィールドを集中させるために、ナノボットが使用される。ナノボットは行列の空のボクセルを移動することができます。局所的な高調波共鳴部分空間フィールドを利用すると、ナノボットの位置は制約されない（「浮動」）。ナノボットは核分裂（別のナノボットを分岐させる）と融合（別のナノボットと結合する）を受けることができる。

Construction always begins and ends with a single nanobot at the origin of the matter subspace pad and proceeds in discrete time steps. Each time step, the pad generates a synchronous time-step signal that coordinates the actions of the nanobots. In response to the time-step signal, each active nanobot performs a single command. Commands include moving, swaping harmonics, creating and destroying matter, and undergoing fission and fusion. All commands take effect simultaneously at the end of the time step. In general, it is an error if the commands of different nanobots interfere or have conflicting updates to the state of the system.

構成は、物質の部分空間パッドの起点で単一のナノボットで常に開始され、終了し、離散的な時間ステップで進行する。 各時間ステップにおいて、パッドは、ナノボットの動作を調整する同期時間ステップ信号を生成する。 タイムステップ信号に応答して、各アクティブナノボットは単一のコマンドを実行する。 コマンドには、移動、高調波のスワップ、物質の作成と破壊、核分裂と融合が含まれます。 すべてのコマンドは、タイムステップの終了時に同時に有効になります。 一般に、異なるナノボットのコマンドが、システムの状態を妨害するか、または相反する更新を有する場合、エラーである。

Each time step that the matter subspace pad is active has an energy cost, which depends on the volume of the space in which the resonant subspace field is being generated and the global harmonics. Similarly, each time step that a nanobot is active has an energy cost that depends on the command being performed.

問題部分空間パッドがアクティブである各時間ステップは、共鳴部分空間フィールドが生成されている空間の体積および全体的な高調波に依存するエネルギーコストを有する。 同様に、ナノボットがアクティブである各時間ステップは、実行されるコマンドに依存するエネルギーコストを有する。

# Details

## Coordinate System

The (global) resonant subspace field of the matter subspace pad establishes a cubical matrix (Cartesian grid) in finite three-dimensional Euclidean space, where each voxel of the matrix corresponds to a cubical volume of space. With respect to an observer, the x-axis extends from left to right, the y-axis extends from bottom to top, and the z-axis extends from near to far.

事象部分空間パッドの（グローバル）共鳴部分空間フィールドは、有限3次元ユークリッド空間内の立方体行列（デカルト格子）を確立する。ここで、行列の各ボクセルは、空間の立方体体積に対応する。 観察者に関しては、x軸は左から右に延び、y軸は下から上に延び、z軸は近づくから遠くに延びる。

## Resolutions
A resolution R specifies the number of voxels of the matrix along the x-, y-, and z-axes, where R is an integer and satisfies 0 < R ≤ 250.

### Coordinate Differences 座標差

A coordinate difference d specifies the relative position of one coordinate to another and is written `<dx, dy, dz>` , where dx, dy, and dz are (positive or negative) integers.
Adding distance `d = <dx, dy, dz>` to coordinate `c = (x, y, z)` , written `c + d` , yields the coordinate `(x + dx, y + dy, z + dz)` .

座標差 `d` は、ある座標の相対的な位置を指定し、`<dx、dy、dz>` と書かれています。
ここで `dx` 、`dy` 、および `dz` は（正または負の）整数です。
座標 `c = (x、y、z)` に距離 `d = <dx、dy、dz>` を加算すると（ `c + d` と書きます）、座標 `(x + dx、y + dy、z + dz)` が得られます。

The Manhattan length (or L1 norm) of a coordinate difference `d = <dx, dy, dz>` is written `mlen(d)` and defined as `|dx| + |dy| + |dz|` (the sum of the absolute values of dx, dy, and dz). The Manhattan length of a coordinate difference is always a non-negative integer.

座標差 `d = <dx、dy、dz>` のマンハッタン長（またはL1ノルム）は `mlen（d）` と書かれ、 `| dx | + | dy | + | dz |` として定義される。
（dx、dy、dzの絶対値の総和）である。
座標差のマンハッタン長は、常に負ではない整数です。

Coordinates c and c′ are adjacent if there exists a coordinate difference d such that c′ = c + d and mlen(d) = 1. More intuitively, coordinates are adjacent if they differ in exactly one component by exactly 1.

`c'= c + d` で `mlen（d）= 1` となるような座標差 `d` が存在する場合、座標 `c` と `c'` は隣接している。
より正確には、正確に1つの成分が正確に1だけ異なる場合、座標は隣接する。

The Chessboard length (or Chebyshev distance or L∞ norm) of a coordinate difference d = <dx, dy, dz> is written clen(d) and defined as max(|dx|, |dy|, |dz|) (the maximum of the absolute values of dx, dy, and dz). The Chessboard length of a coordinate difference is always a non-negative integer.

座標差 `d = <dx、dy、dz>` のチェス盤長（またはチェビシェフ距離またはL∞ノルム）は `clen（d）` と書かれ、 `max（| dx |、| dy |、| dz |）` で定義される（ `dx、dy、dz` の絶対値の最大値）。
座標差のチェスボード長は、常に負ではない整数です。

#### Linear Coordinate Differences 直線座標差

A coordinate difference d = <dx, dy, dz> is a linear coordinate difference (notated ld) if dx ≠ 0 ∧ dy = 0 ∧ dz = 0 or dx = 0 ∧ dy ≠ 0 ∧ dz = 0 or dx = 0 ∧ dy = 0 ∧ dz ≠ 0.
That is, a coordinate difference is a linear coordinate difference if exactly one component is non-zero. (For a linear coordinate difference, the Manhattan length is always equal to the Chessboard length and is always greater than zero.)

dx ≠ 0 ∧ dy = 0 ∧ dz = 0 or dx = 0 ∧ dy ≠ 0 ∧ dz = 0 or dx = 0 ∧ dy = 0 ∧ dz ≠ 0 の場合、座標差d = <dx、dy、dz>は線形座標差（notated ld）である。
つまり、正確に1つの成分が非ゼロである場合、座標差は線形座標差である。 （線形座標の差については、マンハッタンの長さは常にチェス盤の長さに等しく、常にゼロより大きい）。

A linear coordinate difference ld is a short linear coordinate difference (notated sld) if mlen(ld) ≤ 5. There are exactly 30 short linear coordinate differences.

直線座標差ldは、mlen（ld）≦5の場合、短い直線座標差（notated sld）です。ちょうど30の短い直線座標差があります。

A linear coordinate difference ld is a long linear coordinate difference (notated lld) if mlen(ld) ≤ 15. There are exactly 90 long linear coordinate differences.

直線座標差ldは、mlen（ld）≦15の場合、長い直線座標差（notated lld）です。正確に90の長い直線座標差があります。

#### Near Coordinate Differences

A coordinate difference d is a near coordinate difference (notated nd) if 0 < mlen(d) ≤ 2 and clen(d) = 1. That is, a coordinate difference is a near coordinate difference if at least one and at most two components have the value -1 or 1 and the other components have the value 0. There are exactly 18 near coordinate differences.

`0 <mlen（d）≦2` かつ `clen（d）= 1` であれば、座標差dは近似座標差（notated nd）である。
すなわち、座標差は、少なくとも1つ、多くとも2つの成分 -1または1の値を持ち、他の成分は0の値を持ちます。ちょうど18の近くに座標の差があります。

#### Far Coordinate Differences
A coordinate difference d is a far coordinate difference (notated fd) if 0 < clen(d) ≤ 30. That is, a coordinate difference is a far coordinate difference if all components have values between -30 and 30 and at least one component is non-zero. There are exactly 226980 far coordinate differences.

座標差dは、`0 <clen（d）≦30`の場合、far座標の差（notated fd）です。つまり、座標差は、すべての成分が-30〜30の値を持ち、少なくとも1つの成分が 非ゼロ。 正確に226980の遠方座標の違いがあります。

#### Regions

A region r specifies opposing corners of a rectangular cuboid and is written [c1, c2]. A coordinate c = (x, y, z) is a member of a region r = [c1,c2], where c1 = (x1, y1, z1) and c2 = (x2, y2, z2), if min(x1,x2) ≤ x ≤ max(x1,x2), min(y1,y2) ≤ y ≤ max(y1,y2), and min(z1,z2) ≤ z ≤ max(z1,z2).

領域rは、長方形の直方体の対向するコーナーを指定し、[c1、c2]と書かれる。
座標c =（x、y、z）は領域r = [c1、c2]のメンバーであり、min（x1、y1、z1）であればc1 =（x1、y1、z1）およびc2 =（x2、y2、 （y1、y2）、min（z1、z2）≦z≦max（z1、z2）のように、

Two regions are considered equal if they have the same set of coordinates as members; equivalently, two regions are considered equal if they describe the same rectangular cuboid.

2つの領域は、メンバーと同じ座標セットを持つ場合、等しいとみなされます。 等価的に、2つの領域は、同じ長方形の立方体を記述する場合、等しいとみなされる。

The dimension of a region r = [(x1, y1, z1), (x2, y2, z2)] is written dim(r) and is defined as (x1 = x2 ? 0 : 1) + (y1 = y2 ? 0 : 1) + (z1 = z2 ? 0 : 1). That is, the dimension of a region counts the number of components that differ. A region with dimension 0 is a “point”; a region with dimension 1 is a “line”; a region with dimension 2 is a “plane”; and a region with dimension 3 is a “box”.

領域r = [（x1、y1、z1）、（x2、y2、z2）]の次元はdim（r）で書かれ、(x1 = x2 ? 0 : 1) + (y1 = y2 ? 0 : 1) + (z1 = z2 ? 0 : 1) となる。
つまり、領域の次元は、異なるコンポーネントの数を数えます。
次元0の領域は「ポイント」です。 次元1の領域は「線」である。 次元2の領域は「平面」である。 次元3の領域は「ボックス」です。

## Matrix

A matrix M has an implicit resolution R and specifies the state of each voxel as either Full (containing matter) or Void (containing no matter). A matrix M can be considered a function from coordinates valid with respect to the resolution R to either Full or Void.

行列Mは暗黙的な分解能Rを持ち、Full（物質を含む）またはVoid（物質なし）のいずれかとして各ボクセルの状態を指定する。
行列Mは、解像度Rに関して有効な座標からFullまたはVoidへの関数と考えることができる。

### Grounded 接地された

A Full coordinate `c = (x, y, z)` of a matrix M is grounded if either `y = 0` or there exists an adjacent Full coordinate `c′ = c + d` (where `mlen(d) = 1` ) that is itself grounded.
(Alternatively, a Full coordinate c is grounded if there is a (possibly empty) sequence of adjacent Full coordinates that starts with the coordinate c and ends with a Full coordinate c′ = (x′, 0, z′).)

y = 0の場合、またはそれ自体が接地されている隣接完全座標`c '= c + d`（`mlen（d）= 1`）が存在する場合、行列Mの完全座標`c =（x、y、z）`は接地される。 （あるいは、完全な座標cは、座標cで始まり完全な座標c '=（x'、0、z '）で終わる隣接する完全な座標のシーケンス（おそらく空の）があれば接地される。

要するに Full のボックスを辿って地面まで行けたら設置している。

## System State and Execution

### State

The state S of an executing Nanobot Matter Manipulation System is comprised of:

+ energy: the amount of energy expended (an integer)
+ harmonics: the (global) field harmonics (either Low or High)
+ matrix: the matrix of voxels (each voxel either Full or Empty)
+ bots: the set of active nanobots
+ trace: the sequence of commands to be performed by nanobots

実行中のナノボットマター操作システムの状態Sは、

+ energy ：消費されたエネルギーの量（整数）
+ harmonics ：（グローバル）フィールド高調波（低または高）
+ matrix ：ボクセルのマトリクス（各ボクセルはFullまたはEmpty）
+ bots ：アクティブなナノボットの集合
+ trace ：ナノボットによって実行されるコマンドのシーケンス

The state of an active nanobot bot is comprised of:

+ bid: the (unique) identifier (a positive integer)
+ pos: the position (a coordinate)
+ seeds: the set of identifiers available for fission

アクティブなナノボットボットの状態は、

+ bid ：（一意の）識別子（正の整数）
+ pos ：位置（座標）
+ seeds：分裂のために利用可能な識別子のセット

Furthermore, a system state is well-formed if it satisfies the following properties:

+ If the harmonics is Low, then all Full voxels of the matrix are grounded.
+ Each active nanobot has a different identifier.
+ The position of each active nanobot is distinct and is Void in the matrix.
+ The seeds of each active nanobot are disjoint.
+ The seeds of each active nanobot does not include the identifier of any active nanobot.

さらに、システム状態は、以下の特性を満たす場合、well-formed である。

+ harmonics がLowの場合、マトリクスの全ボクセルは接地されます。
+ それぞれのアクティブなナノボットは異なる識別子を持っています。
+ 各活性ナノボットの位置はそれぞれ別々であり、マトリックス中ではVoidである。
+ 各活性ナノボットのseedsは互いに素である、つまり集合が共通部分を持たない。
+ 各活性ナノボットのseedsは、活性ナノボットの識別子を含まない。

### Execution

Each time step that there are active nanobots, commands are taken from the trace and assigned to nanobots and the system state is updated in response to the commands performed by each nanobot.

アクティブなナノボットがある各時間ステップでは、トレースからコマンドが取り出され、ナノボットに割り当てられ、各ナノボットによって実行されるコマンドに応答してシステム状態が更新される。

In general, it is an error if the commands of different nanobots interfere.
Examples of interference include: two nanobots moving through the same coordinate; two nanobots creating matter at the same coordinate; one nanobot fissioning a nanobot at a coordinate and another nanobot waiting in the same coordinate.
More specifically, the commands of one nanobot group bots1 interferes with the commands of another nanobot group bots2 if the volatile coordinates of the commands of bots1 include any of the volatile coordinates of the commands of bots2.
The volatile coordinates of a nanobot group are those coordinates occupied by nanobots of the group and being “used” by the commands of the group during the time step.

一般的に、異なるナノボットのコマンドが干渉すると、エラーになります。
干渉の例には、同じ座標を移動する2つのナノボット、 2つのナノボットが同じ座標で物質を生成する、 1つのナノボットがある座標でナノボットを分裂させ、別のナノボットが同じ座標で待機する。
より具体的には、1つのナノボットグループ bots_1 のコマンドの揮発性座標が bots_2 のコマンドの揮発性座標のいずれかを含む場合、 bots_2 のコマンドと干渉する。
ナノボットグループの揮発性座標は、グループのナノボットによって占有され、時間ステップ中にグループのコマンドによって「使用される」座標である。

At the beginning of a time step, it is an error if the system state is not well-formed.

タイムステップの開始時に、システム状態が正しく構成されていないと、エラーになります。

The commands to be performed by the active nanobots are taken from the trace. Let S.bots = {bot1, bot2, …, botn}, where bot1.bid < bot2.bid < … < botn.bid. (In other words, the n active nanobots are sorted by identifier.) Let S.trace = cmd1 cmd2 … cmdn …. (It is an error if S.trace contains less than n commands.) Each nanobot boti is assigned the command cmdi and nanobot command groups are formed. Command pre-conditions that would lead to an error are checked with respect to the starting state matrix (before any updates to the state matrix). Interference between the volatile coordinates of nanobot command groups that would lead to an error are checked.

アクティブなナノボットによって実行されるコマンドは、トレースから取得されます。
`S.bots = {bot_1、bot_2、...、bot_n}` としましょう。ここで `bot_1.bid <bot_2.bid <... <bot_n.bid` です。
（言い換えれば、n個のアクティブなナノボットは識別子によってソートされます。）
`S.trace = cmd_1 cmd_2 ... cmd_n ...` とします。 （ `S.trace` のコマンドがn個未満のときエラーになります）。
各ナノボットにコマンド `cmd_i` が割り当てられ、ナノボットコマンドグループが形成されます。
エラーにつながるコマンド事前条件は、開始状態行列（状態行列の更新前）に関してチェックされます。
エラーにつながるナノボットコマンドグループの揮発性座標間の干渉がチェックされます。

Assuming no errors, then the system state is be updated:

エラーがないならば、次のようにシステム状態が更新されます。

+ There is an energy cost each time step to maintain the (global) resonant subspace field:

    （グローバル）共鳴部分空間フィールドを維持するためには、各ステップごとにエネルギーコストがかかります。

    ```text
    if (S.harmonics == High)
        S.energy := S.energy + 30 * R * R * R
    else // (S.harmonics == Low)
       S.energy := S.energy + 3 * R * R * R
    endif
    ```

+ There is an energy cost each time step for each active nanobot to maintain its (local) resonant subspace field:

    各アクティブなナノボットがその（局所的な）共鳴部分空間場を維持するためには、毎回エネルギーコストがかかります。

    ```text
    S.energy := S.energy + 20 * n
    ```

+ The effects of each nanobot command group on the state energy, matrix, and active bots are applied to the system state. Because none of the nanobot command groups interfere, the order in which the effects of each nanobot command group are applied does not matter.

    状態エネルギー、マトリックス、およびアクティブボットに対する各ナノボットコマンドグループの影響は、システム状態に適用されます。
    ナノボットコマンドグループのどれも干渉しないので、各ナノボットコマンドグループの効果が適用される順序は重要ではない。

+ The performed commands are removed from the trace:

    実行されたコマンドはトレースから削除されます。

    ```text
    S.trace := drop(S.trace, n)
    ```

## Nanobot Commands

### Singleton Nanobot Commands

Most commands are performed by a single nanobot in isolation.
In the following descriptions, assume that the command is being performed by nanobot `bot` and let `c = bot.pos` .
Note that `c` (the current position of the nanobot) is always a volatile coordinate, because there would be interference if the command of any other nanobot were to “use” the current position of the nanobot.

ほとんどのコマンドは、単独のナノボットによって実行されます。
以下の説明では、コマンドがナノボット `bot` によって実行されているとし、 `c = bot.pos` とします。
他のナノボットのコマンドがナノボットの現在の位置を「使用する」のであれば、干渉が存在するため、 `c` （ナノボットの現在の位置）は常に不安定な座標であることに注意してください。

+ **Halt**: (停止)

    It is an error if c ≠ (0, 0, 0) or if S.bots ≠ { bot } or if S.harmonics ≠ Low.

    The volatile coordinate of this command is the coordinate c.

    The effect of this command is:

    ```text
    S.bots := { }
    ```

    (The nanobot bot is removed from the set of active bots and the system halts. Note that the pre-condition S.bots == { bot } ensures that this was the only nanobot remaining in the system.)

    (ナノボットは、アクティブボットのセットから除去され、システムは停止する。 事前条件S.bots == {bot}は、これがシステムに残っている唯一のナノボットであることを保証します。)

+ **Wait**: (待機)

    The volatile coordinate of this command is the coordinate c.
    This command has no effect on the system state.

    影響を及ぼす座標は `c` である。
    このコマンドはシステムの状態に影響を及ぼさない。

+ **Flip**: (反転)

    The volatile coordinate of this command is the coordinate c.

    The effect of this command is:

    ```text
    if (S.harmonics == High)
      S.harmonics := Low
    else // (S.harmonics == Low)
      S.harmonics := High
    endif
    ```

+ **SMove** lld (Straight Move):  (15 セル以下だけまっすぐ動く)

    (lld is a long linear coordinate difference.)

    Let `c′ = c + lld` .

    It is an error if c′ is not a valid coordinate with respect to the resolution of the matrix. It is an error if any coordinate in the region [c,c′] is Full in the matrix.
    `c'` が行列の解像度に関して有効な座標でない場合は、エラーとなります。
    領域[c、c ']内のいずれかの座標が Full である場合はエラーとなります。

    The volatile coordinates of this command are all coordinates in the region [c,c′].

    The effect of this command is:

    ```text
    bot.pos := c'
    S.energy := S.energy + 2 * mlen(lld)
    ```

    (The nanobot’s position is updated and there is an energy cost proportional the Manhattan length of the move.)

+ **LMove** sld1 sld2 (L Move): (5 セル以下だけまっすぐ動く ×2)

    (sld1 and sld2 are short linear coordinate differences.)

    Let `c′ = c + sld1 and c″ = c′ + sld2` .

    It is an error if c′ or c″ is not a valid coordinate with respect to the resolution of the matrix. It is an error if any coordinate in the region [c,c′] or in the region [c′,c″] is Full in the matrix.

    c 'またはc "が行列の解像度に関して有効な座標でない場合はエラーです。
    領域[c、c ']または領域[c'、c "]内のいずれかの座標が Full である場合は、エラーとなります。

    The volatile coordinates of this command are all coordinates in the regions [c,c′] and [c′,c″].

    The effect of this command is:

    ```text
    bot.pos := c″
    S.energy := S.energy + 2 * (mlen(sld1) + 2 + mlen(sld2))
    ```

    (The nanobot’s position is updated and there is an energy cost proportional the Manhattan length of the move.)

+ **Fission** nd m: (分裂)

    (nd is a near coordinate difference and m is a non-negative integer.)

    It is an error if `bid.seeds = { }` .

    Let `{bid_1, bid_2, …, bid_n} = bid.seeds` (where `bid_1 < bid_2 < … < bid_n` ).

    Let `c′ = c + nd` .

    It is an error if `c′` is not a valid coordinate with respect to the resolution of the matrix.
    It is an error if coordinate c′ is Full in the matrix. It is an error if n < m + 1.

    The volatile coordinates of this command are the coordinates c and c′.

    The effect of this command is:

    ```text
    bot.seeds := {bid_{m+2}, …, bid_n}
    bot'.bid := bid_1
    bot'.pos := c'
    bot'.seeds := {bid_2, …, bid_{m+1} }
    S.bots := union(S.bots, { bot' })
    S.energy := S.energy + 24
    ```

    (The lowest m + 1 identifiers are removed from the parent bot’s seeds. A new child bot bot′ is added to the set of active bots. The lowest of the removed identifers becomes the identifier of the child bot and the remaining m of the removed identifiers become the seeds of the child bot. Energy is expended during the creation of the child bot.)

+ **Fill** nd: (埋める)

    (nd is a near coordinate difference.)

    Let `c' = c + nd` .

    It is an error if `c'` is not a valid coordinate with respect to the resolution of the matrix.

    The volatile coordinates of this command are the coordinates c and c′.

    The effect of this command is:

    ```text
    if (S.matrix(c′) == Void)
      S.matrix(c′) := Full
      S.energy := S.energy + 12
    else // (S.matrix(c′) == Full)
      S.energy := S.energy + 6
    endif
    ```

    (If the voxel had no matter, then energy is converted to matter (a positive energy cost). If the voxel had matter, then energy is lost (a positive energy cost).)

+ **Void** nd: (空ける)

    (nd is a near coordinate difference.)

    Let `c′ = c + nd` .

    It is an error if `c′` is not a valid coordinate with respect to the resolution of the matrix.

    The volatile coordinates of this command are the coordinates c and c′.

    The effect of this command is:

    ```text
    if (S.matrix(c′) == Full)
      S.matrix(c′) := Void
      S.energy := S.energy - 12
    else // (S.matrix(c′) == Void)
      S.energy := S.energy + 3
    endif
    ```

(If the voxel had matter, then the matter is converted to energy (a negative energy cost). If the voxel had no matter, then energy is lost (a positive energy cost).)

### Group Nanobot Commands

Some nanobot commands are performed through the coordinated action of a group of two or more nanobots. It is an error if a group is missing one or more partners.

いくつかのナノボットコマンドは、2つ以上のナノボットのグループの協調動作によって実行される。
グループに1人以上のパートナーがいない場合はエラーです。

+ **FusionP** nd (Fusion Primary), **FusionS** nd (Fusion Secondary):

    (nd is a near coordinate difference.)

    There are two nanobots `bot_p` and `bot_s` such that `bot_p` is performing **FusionP** `nd_p` and `bot_s` is performing **FusionS** `nd_s`, where `bot_p.pos + nd_p = bot_s.pos` and `bot_s.pos + nd_s = bot_p.pos` .
    (The primary nanobot identifies the secondary nanobot’s position and the secondary nanobot identifies the primary nanobot’s position.)

    It is an error if either coordinate `bot_p.pos + nd_p` or coordinate `bot_s.pos + nd_s` is not a valid coordinate with respect to the resolution of the matrix.

    The volatile coordinates of this command group are the coordinates `bot_p.pos` and `bot_s.pos`.

    The effect of this command group is:

    ```text
    S.bots := diff(S.bot_s, { bot_s })
    bot_p.seeds := union(bot_p.seeds, { bot_s.bid }, bot_s.seeds)
    S.energy := S.energy - 24
    ```

    (The secondary nanobot is removed from the set of active nanobots. The secondary nanobot’s identifier and the secondary nanobot’s seeds are added to the primary nanobot’s seeds. Energy is regained during the destruction of the secondary bot.)

    （第２ナノボットは、活性ナノボットのセットから除去される。第２ナノボットの識別子および第２ナノボットのseedsは、第１ナノボットのseedsに加えられ、エネルギーは第２ボットの破壊の間に回復される。)

+ **GFill** nd fd (Group Fill):

    (nd is a near coordinate difference and fd is a far coordinate difference.)

    Let `r` be a region and let `n = 2dim(r)` .

    There are n nanobots `bot_1, …, bot_n` such that each boti is performing GFill ndi fdi, where `r = [bot_i.pos + nd_i, bot_i.pos + nd_i + fd_i]`. (Recall that two regions are considered equal if they have the same set of coordinates as members; equivalently, if they describe the same rectangular cuboid.)

    It is an error if any coordinate `bot_i.pos + nd_i` or coordinate `bot_i.pos + nd_i + fd_i` is not a valid coordinate with respect to the resolution of the matrix. It is also an error if `bot_i.pos + nd_i = bot_j.pos + nd_j` (for i ≠ j). It is also an error if any coordinate `bot_i.pos` is a member of region r.

    Intuitively, the group of nanobots identify a region, with a distinct nanobot at each corner. Two nanobots can fill a “line”; four nanobots can fill a “plane”; and eight nanobots can fill a “box”.

    The volatile coordinates of this command are the coordinates `bot_1.pos, …, bot_n.pos` and all coordinates of the region r.

    The effect of this command group is:

    ```text
    for (c′ in r)
      if (S.matrix(c′) == Void)
        S.matrix(c′) := Full
        S.energy := S.energy + 12
      else // (S.matrix(c′) == Full)
        S.energy := S.energy + 6
      endif
    endfor
    ```

    (If a voxel in the region had no matter, then energy is converted to matter (a positive energy cost). If a voxel in the region had matter, then energy is lost (a positive energy cost).)

+ ***GVoid*** nd fd (Group Void):

    (nd is a near coordinate difference and fd is a far coordinate difference.)

    Let `r` be a region and let `n = 2dim(r)`.

    There are n nanobots bot1, …, botn such that each boti is performing GVoid ndi fdi, where `r = [boti.pos + ndi, boti.pos + ndi + fdi]`. (Recall that two regions are considered equal if they have the same set of coordinates as members; equivalently, if they describe the same rectangular cuboid.)

    It is an error if any coordinate boti.pos + ndi or coordinate boti.pos + ndi + fdi is not a valid coordinate with respect to the resolution of the matrix. It is also an error if boti.pos + ndi = botj.pos + ndj (for i ≠ j). It is also an error if any coordinate boti.pos is a member of region r.

    Intuitively, the group of nanobots identify a region, with a distinct nanobot at each corner. Two nanobots can void a “line”; four nanobots can void a “plane”; and eight nanobots can void a “box”.

    The volatile coordinates of this command are the coordinates bot1.pos, …, botn.pos and all coordinates of the region r.

    ```text
    for (c′ in r)
      if (S.matrix(c′) == Full)
        S.matrix(c′) := Void
        S.energy := S.energy - 12
      else // (S.matrix(c′) == Void)
        S.energy := S.energy + 3
      endif
    endfor
    ```

    (If the voxel in the region had matter, then the matter is converted to energy (a negative energy cost). If the voxel in the region had no matter, then energy is lost (a positive energy cost).)

## Traces

A trace T specifies the commands performed by each nanobot during an execution. A trace is simply a sequence of commands, implicitly ordered first by time step and then by nanobot identifier.

トレースTは、実行中に各ナノボットによって実行されるコマンドを指定する。 トレースは、単純に時間シーケンスによって暗黙的に順序付けられたコマンドシーケンスであり、次にナノボット識別子によって順序付けられます。

When executing a time step with n active nanobots, the n commands are taken from the trace and assigned to the active nanobots in identifier order (the first command is assigned to the nanobot with smallest identifier, the second command is assigned to the nanobot with the second smallest identifier, …, the last command is assigned to the nanobot with the largest identifier).

n個のアクティブなナノボットを有する時間ステップを実行すると、n個のコマンドはトレースから取り出され、識別子順にアクティブなナノボットに割り当てられる（第1のコマンドは最小の識別子でナノボットに割り当てられ、第2のコマンドは 2番目に小さい識別子、...、最後のコマンドは、最大の識別子を持つナノボットに割り当てられます）。

### Trace Files

A trace file is a binary encoding of a trace.
トレースファイルは、トレースのバイナリエンコーディングです。

By convention, a trace file has the extension `.nbt`.
慣習的に、トレースファイルの拡張子は `.nbt` です。

A trace file is simply a sequence of encoded commands, where each command is encoded as one, two, or four bytes.
トレースファイルは、単純に一連のコード化されたコマンドであり、各コマンドは1バイト、2バイト、または4バイトとしてエンコードされます。

In the following [b_n…b_2b_1]^n represents an n-bit value, where b1 is the least-significant bit and bn is the most-significant bit and […«x»^m…]^n represents the embedding of an m-bit value within a larger n-bit value.
次の[bn ... b2b1]^nはnビットの値を表し、b1は最下位ビットであり、bnは最上位ビットであり、[...«x»m ...]^nはnビット中へのmビットの埋め込み。

#### Encoding Coordinate Differences

The different types of coordinate differences that appear in commands have distinct encodings.

コマンドに現れるさまざまなタイプの座標差は、異なるエンコーディングを持ちます。

##### Encoding Linear Coordinate Differences

A short linear coordinate difference sld = <dx, dy, dz> is encoded as a 2-bit axis a and a 4-bit (unsigned) integer i as follows:

+ if dx ≠ 0, then a = [01]^2 and i = dx + 5
+ if dy ≠ 0, then a = [10]^2 and i = dy + 5
+ if dz ≠ 0, then a = [11]^2 and i = dz + 5

Recall that exactly one component of a short linear coordinate difference is non-zero and a short linear coordinate difference has Manhattan length greater than zero and less than or equal to 5.

A long linear coordinate difference lld = <dx, dy, dz> is encoded as a 2-bit axis a and a 5-bit (unsigned) integer i as follows:

+ if dx ≠ 0, then a = [01]^2 and i = dx + 15
+ if dy ≠ 0, then a = [10]^2 and i = dy + 15
+ if dz ≠ 0, then a = [11]^2 and i = dz + 15

Recall that exactly one component of a long linear coordinate difference is non-zero and a long linear coordinate difference has Manhattan length greater than zero and less than or equal to 15.

##### Encoding Near Coordinate Differences

A near coordinate difference nd = <dx, dy, dz> is encoded as a 5-bit (unsigned) integer with the value (dx + 1) * 9 + (dy + 1) * 3 + (dz + 1). Recall that each component of a near coordinate difference must have the value -1, 0, or 1, but not all combinations are legal. In particular, <1, 1, 1> is not a near coordinate difference; hence the 5-bit value [11111]5 = 31 is not the encoding of any near coordinate difference.

##### Encoding Far Coordinate Differences
A far coordinate difference fd = <dx, dy, dz> is encoded as a sequence of three 8-bit (unsigned) integers with the values dx + 30, dy + 30, and dz + 30. Recall that at least one component of a far coordinate difference is non-zero and each component of a far coordinate difference has a value between -30 and 30.

#### Encoding Commands
Each command is encoded by one or two bytes as follows:

+ Halt:

    ```text
    [11111111]^8
    ```

+ Wait:

    ```text
    [11111110]^8
    ```

+ Flip:

    ```text
    [11111101]^8
    ```

+ SMove lld:

    ```text
    [00«lld.a»^2 0100]^8 [000«lld.i»^5]^8
    ```

    For example, SMove <12,0,0> is encoded as [00010100] [00011011] and SMove <0,0,-4> is encoded as [00110100] [00001011].

+ LMove sld1 sld2:

    ```text
    [«sld2.a»^2 «sld1.a»^2 1100]^8 [«sld2.i»^4 «sld1.i»^4]^8
    ```

    For example, LMove <3,0,0> <0,-5,0> is encoded as [10011100] [00001000] and LMove <0,-2,0> <0,0,2> is encoded as [11101100] [01110011].

+ FusionP nd:

    ```text
    [«nd»^5 111]^8
    ```

    For example, FusionP <-1,1,0> is encoded as [00111111].

+ FusionS nd:

    ```text
    [«nd»^5 110]^8
    ```

    For example, FusionS <1,-1,0> is encoded as [10011110].

+ Fission nd m:

    ```text
    [«nd»^5 101]^8 [«m»^8]^8
    ```

    The non-negative integer m is encoded as an 8-bit (unsigned) integer with the value m.

    For example, Fission <0,0,1> 5 is encoded as [01110101] [00000101].

+ Fill nd:

    ```text
    [«nd»^5 011]^8
    ```

    For example, Fill <0,-1,0> is encoded as [01010011].

+ Void nd:

    ```text
    [«nd»5010]8
    ```

    For example, Void <1,0,1> is encoded as [10111010].

+ GFill nd fd:

    ```text
    [«nd»5001]8 [«fd.dx»8]8 [«fd.dy»8]8 [«fd.dz»8]8
    ```

    For example, GFill <0,-1,0> <10,-15,20> is encoded as [01010001] [00101000] [00001111] [00110010].

+ GVoid nd fd:

    ```text
    [«nd»5000]8 [«fd.dx»8]8 [«fd.dy»8]8 [«fd.dz»8]8
    ```
    For example, GVoid <1,0,0> <5,5,-5> is encoded as [10110000] [00100011] [00100011] [00011001].

## Models

A model Mdl specifies a 3D object. A model is comprised of the resolution R of a matrix (which is large enough to contain the object) and the set of Full coordinates of the matrix that make up the object.

モデルMdlは3Dオブジェクトを指定する。 モデルは、（オブジェクトを含むのに十分な大きさの）行列の解像度Rと、オブジェクトを構成する行列の完全な座標の集合とからなる。

A model is well-formed if

+ All coordinates of the set of Full coordinates must not belong to the reserved left-, right-, top-, near-, or far-face regions of the space. That is, all Full coordinates (x, y, z) satisfy 1 ≤ x ≤ R - 2 and 0 ≤ y ≤ R - 2 and 1 ≤ z ≤ R - 2.
+ All coordinates of the set of Full coordinates must be grounded. That is, all Full coordinates c = (x, y, z) satisfy either y = 0 or there exists an adjacent Full coordinate c′ = c + d (where mlen(d) = 1) that is grounded.

+ Full座標セットのすべての座標は、空間の予約された左、右、上、手前、または奥の領域に属してはいけません。 すべての座標（x、y、z）は、1≦x≦R-2、0≦y≦R-2、1≦z≦R-2を満たす。
+ Full座標セットのすべての座標は、接地する必要があります。 つまり、すべてのフル座標c =（x、y、z）はy = 0を満たすか、または接地された隣接フル座標c '= c + d（mlen（d）= 1）が存在する。

### Model Files

A model file is binary encoding of a model. (Note that this binary encoding handles both ill-formed and well-formed models.)

モデルファイルは、モデルのバイナリエンコーディングです。 （このバイナリエンコーディングは、不正な形式のモデルと well-formed のモデルの両方を扱うことに注意してください）。

By convention, a model file has the extension `.mdl`.

慣例により、モデルファイルの拡張子は `.mdl` です。

The first byte of the model file encodes the resolution R, interpreting the byte as an 8-bit (unsigned) integer.

モデルファイルの最初のバイトは分解能Rを符号化し、そのバイトを8ビット（符号なし）整数として解釈します。

The remaining ⌈(R × R × R) / 8⌉ bytes of the model file encode the set of Full coordinates. The sequence of bytes are interpreted as a sequence of bits corresponding to the coordinates of the matrix by traversing the x-dimension from 0 to R - 1, traversing the y-dimension from 0 to R - 1, and traversing the z-dimension from 0 to R - 1. More explicitly, coordinate (x, y, z) is Full in the model’s matrix if and only if bit x × R × R + y × R + z is set. Note that some high-bits of the last byte of the model file may be unused.

残りの（R×R×R）/ 8バイトのモデルファイルは、フル座標のセットをエンコードします。
バイトのシーケンスは、x次元を0からR-1まで横断し、y次元を0からR-1まで横断し、z次元を0からR-1まで横断することによって行列の座標に対応するビットのシーケンスとして解釈される。
より明示的には、ビットx×R×R + y×R + zが立っている場合にのみ、座標（x、y、z）はモデルの行列内でフルである。
モデルファイルの最後のバイトのいくつかの上位ビットは使用されないかもしれないことに注意してください。


#### Extended Model Files

An extended model file is a binary encoding of a model and a set of nanobot identifiers and positions.

拡張モデルファイルは、モデルとナノボット識別子および位置のセットのバイナリエンコーディングです。

By convention, a model file has the extension `.xmdl` .

通常、モデルファイルの拡張子は `.xmdl` です。

As with a model file, the first byte of the extended model file encodes the resolution R, interpreting the byte as an 8-bit (unsigned) integer, and the subsequent ⌈(R × R × R) / 8⌉ bytes of the extended model file encode the set of Full coordinates.

モデルファイルの場合と同様に、拡張モデルファイルの最初のバイトは、バイトを8ビット（符号なし）整数と解釈し、拡張Rの次の⌈（R×R×R）/ 8バイト モデルファイルは、フル座標のセットをエンコードします。

Subsequent 4-byte sequences encode the identifier bid and position (x, y, z) of nanobots.
The first byte encodes bid, the second byte encodes x, the third byte encodes y, and the fourth byte encodes z, in each case interpreting the byte as an 8-bit (unsigned) integer.

後続の4バイトシーケンスは、ナノボットの識別子bidおよび位置（x、y、z）を符号化する。
最初のバイトはbidを符号化し、2番目のバイトはxを符号化し、3番目のバイトはyを符号化し、4番目のバイトはzを符号化し、それぞれ8ビット（符号なし）整数として解釈する。

### Model Viewer
A JavaScript and WebGL-based [model viewer](https://icfpcontest2018.github.io/view-model.html) is available.

# Full Contest
[Saturday 21 July 2018 16:00 UTC](https://www.timeanddate.com/worldclock/fixedtime.html?msg=ICFP+Programming+Contest+2018+%28end+of+lightning+division%29&iso=20180721T16) to [Monday 23 July 2018 16:00 UTC](https://www.timeanddate.com/worldclock/fixedtime.html?msg=ICFP+Programming+Contest+2018+%28end+of+full+contest%29&iso=20180723T16)

Generate and submit nanobot traces to assemble, disassemble, and reassemble target 3D models while minimizing energy used. The initial nanobot (the Build-a-Tron 4000) has 39 seeds.

## Problems
Download problemsF.zip, which is a collection of (well-formed) target model files named FANNN_tgt.mdl, (well-formed) source model files named FDNNN_src.mdl, and (well-formed) source model files and target model files named FRNNN_src.mdl and FRNNN_tgt.mdl. For each target model file FANNN_tgt.mdl, generate a trace file named FANNN.nbt that assembles the target model; for each source model file FDNNN_src.mdl, generate a trace file named FDNNN.nbt that disassembles the source model; for each source model file FRNNN_src.mdl and target model file FRNNN_tgt.mdl (guaranteed to have equal resolutions), generate a trace file named FRNNN.nbt that reassembles from the source model to the target model. A problemsF.txt file acknowledges the sources for the problem models.

FANNN_tgt.mdlという名前の（整形式の）ターゲットモデルファイル、FDNNN_src.mdlという名前の（整形式の）ソースモデルファイル、（整形式の）ソースモデルファイルとターゲットモデルファイルのコレクションであるproblemsF.zipをダウンロードします。 FRNNN_src.mdlおよびFRNNN_tgt.mdl。 各ターゲット・モデル・ファイルFANNN_tgt.mdlに対して、ターゲット・モデルをアセンブルするFANNN.nbtという名前のトレース・ファイルを生成します。 各ソースモデルファイルFDNNN_src.mdlに対して、ソースモデルを逆アセンブルするトレースファイルFDNNN.nbtを生成します。 各ソース・モデル・ファイルFRNNN_src.mdlおよびターゲット・モデル・ファイルFRNNN_tgt.mdl（同等の分解能を保証する）に対して、ソース・モデルからターゲット・モデルに再アセンブルするトレース・ファイルFRNNN.nbtを生成します。 problemsF.txtファイルは、問題モデルのソースを認識します。

### Assembly Problems
For each FANNN_tgt.mdl and corresponding FANNN.nbt, let Mdltgt be the target model encoded by FANNN_tgt.mdl, let R be the resolution of Mdltgt, and let trace be the trace encoded by FANNN.nbt. The trace is correct for this problem if, when executed from the initial system state Sinit where

各FANNN_tgt.mdlおよび対応するFANNN.nbtに対して、MdltgtをFANNN_tgt.mdlによってコード化されたターゲットモデルとし、RをMdltgtの分解能とし、traceをFANNN.nbtによってコード化されたトレースとする。 このトレースは、初期システム状態Sinitから実行されたときに

+ Sinit.energy := 0
+ Sinit.harmonics := Low
+ Sinit.matrix(c) := Void    for all coordinates c valid with respect to the resolution R
+ Sinit.bots := { botinit }
+ botinit.bid := 1
+ botinit.pos := (0, 0, 0)
+ botinit.seeds := { 2, …, 40 }
+ Sinit.trace := trace

there are no decoding or execution errors and the final state Sfini satisfies

+ Sfini.harmonics == Low
+ Sfini.matrix(c) == Mdltgt(c)    for all coordinates c valid with respect to the resolution R
+ Sfini.bots == { }
+ Sfini.trace == ε

The final energy of the trace is Sfini.energy.

### Disassembly Problems
For each FDNNN_src.mdl and corresponding FDNNN.nbt, let Mdlsrc be the source model encoded by FDNNN_src.mdl, let R be the resolution of Mdlsrc, and let trace be the trace encoded by FDNNN.nbt. The trace is correct for this problem if, when executed from the initial system state Sinit where

各FDNNN_src.mdlとそれに対応するFDNNN.nbtに対して、MdlsrcをFDNNN_src.mdlによってコード化されたソースモデルとし、RをMdlsrcの分解能とし、traceをFDNNN.nbtによってコード化されたトレースとする。 このトレースは、初期システム状態Sinitから実行されたときに

+ Sinit.energy := 0
+ Sinit.harmonics := Low
+ Sinit.matrix(c) := Mdlsrc(c)    for all coordinates c valid with respect to the resolution R
+ Sinit.bots := { botinit }
+ botinit.bid := 1
+ botinit.pos := (0, 0, 0)
+ botinit.seeds := { 2, …, 40 }
+ Sinit.trace := trace

there are no decoding or execution errors and the final state Sfini satisfies

+ Sfini.harmonics == Low
+ Sfini.matrix(c) == Void    for all coordinates c valid with respect to the resolution R
+ Sfini.bots == { }
+ Sfini.trace == ε

The final energy of the trace is Sfini.energy.

### Reassembly Problems
For each FRNNN_src.mdl and FRNNN_tgt.mdl and corresponding FRNNN.nbt, let Mdlsrc be the source model encoded by FDNNN_src.mdl, Mdltgt be the target model encoded by FDNNN_tgt.mdl, let R be the resolution of Mdlsrc and Mdltgt (guaranteed to be equal), and let trace be the trace encoded by FRNNN.nbt. The trace is correct for this problem if, when executed from the initial system state Sinit where

各FRNNN_src.mdlおよびFRNNN_tgt.mdlおよび対応するFRNNN.nbtについて、MdlsrcをFDNNN_src.mdlによってコード化されるソースモデルとし、MdltgtをFDNNN_tgt.mdlによってコード化されるターゲットモデルとし、RをMdlsrcおよびMdltgtの解像度とする 等しい）、トレースをFRNNN.nbtによってコード化されたトレースとする。 このトレースは、初期システム状態Sinitから実行されたときに

+ Sinit.energy := 0
+ Sinit.harmonics := Low
+ Sinit.matrix(c) := Mdlsrc(c)    for all coordinates c valid with respect to the resolution R
+ Sinit.bots := { botinit }
+ botinit.bid := 1
+ botinit.pos := (0, 0, 0)
+ botinit.seeds := { 2, …, 40 }
+ Sinit.trace := trace

there are no decoding or execution errors and the final state Sfini satisfies

+ Sfini.harmonics == Low
+ Sfini.matrix(c) == Mdltgt(c)    for all coordinates c valid with respect to the resolution R
+ Sfini.bots == { }
+ Sfini.trace == ε

The final energy of the trace is Sfini.energy.

## Default Traces
A provided dfltTracesF.zip is a collection of default traces. These default traces establish an upper-bound energy for each problem, used for scoring.

提供されるdfltTracesF.zipは、デフォルトのトレースの集合です。 これらのデフォルトトレースは、スコアリングに使用される各問題の上限エネルギーを確立します。

Each default trace for an assembly problem applies the same uniform strategy (a “classic” 3D printing): compute a bounding box for the target model, set harmonics to High, use a single “head” nanobot to sweep each xz-plane of the bounding box from bottom to top and filling the voxel below the nanobot as dictated by the target model, return to the origin, set harmonics to Low, and halt.

アセンブリ問題の各デフォルトトレースは、同じ統一戦略（「古典的な」3D印刷）を適用します。ターゲットモデルのバウンディングボックスを計算し、高調波を高に設定し、単一の「ヘッド」ナノボットを使用して、 ボクシングボックスを下から上に移動し、ターゲットモデルによって指示されるようにナノボットの下にボクセルを充填し、原点に戻し、高調波を低に設定し、停止させる。

Each default trace for a disassembly problem applies the same uniform strategy (the inverse of the “classic” 3D printing): compute a bounding box for the source model, set harmonics to High, use a single “head” nanobot to sweep each xz-plane of the bounding box from top to bottom and voiding the voxel below the nanobot as dictated by the source model, return to the origin, set harmonics to Low, and halt.

逆アセンブリ問題の各デフォルトトレースは、同じクラシック戦略（「古典的」3D印刷の逆）を適用します。ソースモデルの境界ボックスを計算し、高調波を高に設定し、単一の「ヘッド」ナノボットを使用して各xz- バウンディングボックスの平面を上から下に移動し、ソースモデルによって指示されるようにナノボットの下のボクセルをボイドし、原点に戻り、高調波を低に設定し、停止させる。

Each default trace for a reassembly problem applies the same uniform strategy: compute a bounding box for the union of the source and target models, set harmonics to High, use a single “head” nanobot to sweep each xz-plane of the bounding box from top to bottom and void Full voxels of the source model when moving into a voxel and fill Full voxels of the target model when moving out of a voxel, return to the origin, set harmonics to Low, and halt.

リアセンブリ問題の各デフォルトトレースは、同じ統一戦略を適用します。ソースモデルとターゲットモデルの和集合の境界ボックスを計算し、高調波を高に設定し、単一の「ヘッド」ナノボットを使用して境界ボックスの各xz平面を掃引します 上から下およびボイドボクセルに移動するときのソースモデルのフルボクセルと、ボクセルから移動して原点に戻り、高調波を低に設定して停止するときに、ターゲットモデルの全ボクセルを塗りつぶします。

## Scoring
A team’s score for each problem depends upon the final energy of their submitted trace, the final energy of the corresponding default trace, and the minimum energy among all teams’ corresponding submitted traces, and the resolution of the problem.

それぞれの問題に対するチームの得点は、提出されたトレースの最終エネルギー、対応するデフォルトトレースの最終エネルギー、およびすべてのチームの対応する提出されたトレース間の最小エネルギー、および問題の解決に依存する。

Let energyteam be the final energy of the team’s submitted trace, energydflt be the final energy of the corresponding default trace, energybest be the minimum energy among all teams’ corresponding submitted traces and the value energydflt - 1, and R be the resolution of the problem. (If a submitted trace has decoding or execution errors or final energy that exceeds that of the corresponding default trace, then treat it as having final energy equal to that of the corresponding default trace. This ensures that it is always the case that energybest ≤ energyteam ≤ energydflt and energybest < energydflt.) The team’s score for the problem is

エネルギーチームは、チームの提出されたトレースの最終エネルギー、energydfltは対応するデフォルトトレースの最終エネルギー、energybestはすべてのチームの対応する提出されたトレースの最小エネルギー、値はenergydflt - 1、Rは問題の解決率 。 （提出されたトレースに、デコードまたは実行エラー、または対応するデフォルトトレースの最終エネルギーを超える最終エネルギーがある場合、それを対応するデフォルトトレースと同じ最終エネルギーとして扱います。これにより、常に`energybest ≦ energyteam ≤ energydflt`かつ`energybest < energydflt`）問題のチームの得点は

```text
⌊(⌊log2 R⌋ × 1000 × (energydflt - energyteam)) / (energydflt - energybest)⌋
```

(Intuitively, a team’s score is linearly interpolated from 0 (if the submitted trace is no better than the default) to ⌊log2 R⌋ × 1000 (if the submitted trace is the best). The ⌊log2 R⌋ term gives more weight to larger problems and the flooring allows scores to be calculated with (arbitrary-precision) integers.)

A team’s contest score is the sum of their scores for all problems.

The contest winner is the team with the highest score.

## Trace Checker
A JavaScript [trace checker](https://icfpcontest2018.github.io/full/chk-trace.html) is available, which can be used to verify that a trace file successfully decodes to a sequence of commands and to display a prefix and suffix of the full trace.

## Trace Executor
A JavaScript and WebGL-based [trace executor](https://icfpcontest2018.github.io/full/exec-trace.html) is available for testing submissions. A (marginally) faster JavaScript-based trace [executor without visualization](https://icfpcontest2018.github.io/fulln/exec-trace-novis.html) is also available for testing submissions.

## Registration and Submission
[Register](https://icfpcontest2018.github.io/register.html) a contest team to obtain a team-specific private identifier (a 32-digit hexadecimal string).

After generating correct traces, prepare a single .zip file containing exactly (no more than and no less than) the files FANNN.nbt, FDNNN.nbt, and FRNNN.nbt; the provided dfltTracesF.zip demonstrates the submission format. The .zip file may optionally be encrypted (zip --encrypt) with the team-specific private identifier (if a team is concerned about posting submissions to a publicly accessible location during the contest). Make the .zip file available at a publicly accessible URL (a personal or institutional web server, Dropbox, Google Drive, etc.). [Submit](https://icfpcontest2018.github.io/submit.html) the URL and SHA256 checksum of the .zip file before the [end of the full contest](https://www.timeanddate.com/worldclock/fixedtime.html?msg=ICFP+Programming+Contest+2018+%28end+of+full+contest%29&iso=20180723T16) and watch for a [submission acknowledgement](https://icfpcontest2018.github.io/submission-acks.html). Note that submissions for the full contest open at [2018-07-21T18:00:00Z](https://www.timeanddate.com/worldclock/fixedtime.html?msg=ICFP+Programming+Contest+2018+%28full+contest+submissions+open%29&iso=20180721T18) (two hours into the full contest).

Teams may submit multiple times during the contest (using either a new or the same URL, but different SHA256 checksum), but teams are limited to one submission every 15 minutes; early submissions may be evaluated during the contest for live standings, but only the last submissions for the full contest will be considered for prizes. The last submissions for the full contest should remain available for up to two weeks after the end of the contest.

To be considered for prizes, [within two hours of end of the contest](https://www.timeanddate.com/worldclock/fixedtime.html?msg=ICFP+Programming+Contest+2018+%28source+code+submission+deadline%29&iso=20180723T18), teams must update their [profile](https://icfpcontest2018.github.io/profile.html) with complete team information and submit the URL and SHA256 checksum of a single .zip archive with their source code, a README.txt file (brief directions for judges to build/run the solution; description of the solution approach; feedback about the contest; self-nomination for judges’ prize; etc.), and any other supporting materials.