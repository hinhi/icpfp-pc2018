import sys

for name in sys.argv[1:]:
    with open(name.replace('.mdl', '.model'), 'w') as of:
        with open(name, 'rb') as f:
            b = f.read(1)
            r = ord(b)
            print(name, r, b)
            of.write('%i\n' % r)
            i = 8
            b = 0
            for x in range(r):
                for y in range(r):
                    for z in range(r):
                        if i == 8:
                            i = 0
                            b = ord(f.read(1))
                        of.write(str(1 if b & 1 << i else 0))
                        i += 1
                    of.write('\n')
