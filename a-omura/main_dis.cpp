
#include "../lib/system.h"

using namespace std;

typedef Vector3D V;


const int mode = 1;

class AI {

public:
	int R, N;
	int LX, RX, LY, RY, LZ, RZ;
	System system;
	int WX, WZ;

	vector<Vector3D> target;
	vector<int> start;

	AI(int R, int N) : system(R, N, mode), R(R), N(N), target(N + 1, Vector3D(-1, -1, -1)), start(N + 1, 0), upf(N+1, 0), endExpansion(false) {
	};

	void fill(Bot &bot, Vector3D t) {
		auto nd = Vector3D(- bot.pos.x + t.x, - bot.pos.y + t.y, - bot.pos.z + t.z);
		int res = system.canFill(bot.pos, nd, bot.id);
		if (res == 1) {
			system.fill(bot, nd);
		}else 
		if (res == 0){ 
			system.wait(bot); 
		} 
		else if (res == 2) { 
			system.flip(bot); 
		}
	}

	void move(Bot &bot, Vector3D t) {

		int dir, dist;
		if (bot.pos.y != t.y) {
			int d = t.y - bot.pos.y;
			dir = 1;
			dist = (d > 0 ? min(15, d) : max(-15, d));
		}
		else
		if(bot.pos.x != t.x) {
			int d = t.x - bot.pos.x;
			dir = 0;
			dist = (d > 0 ? min(15, d) : max(-15, d));
		} else
		if(bot.pos.z != t.z) {
			int d = t.z - bot.pos.z;
			dir = 2;
			dist = (d > 0 ? min(15, d) : max(-15, d));
		}else {
			system.wait(bot);
			return;
		}

		bool res = system.fmove(bot, dir, dist);
		if (!res) {
			V v;
			if (dir == 0) {
				v = V(dist > 0 ? 1 : -1, 0, 0);
			} else 			
			if (dir == 1) {
				v = V(0, dist > 0 ? 1 : -1, 0);
			} else			
			if (dir == 2) {
				v = V(0, 0, dist > 0 ? 1 : -1);
			}
			if (system.canVoid(bot, v)) {
				system.Void(bot, v);
			}
			else {
				system.wait(bot);
			}
		}
	}

	int standby(Bot &bot) {
		int id = bot.id;

		// 目的地へ
		if (start[id] == 0 && !(target[bot.id - 1] == bot.pos)) {
			move(bot, target[bot.id - 1]);
			return 0;
		}
		else
		// 分裂する
		if (bot.seeds.size() > 40 - N) {

			int next = bot.id;
			auto v = V(target[next].x - bot.pos.x, target[next].y - bot.pos.y, target[next].z - bot.pos.z);
			if (v.x > 0) {
				if (system.canFission(bot, Vector3D(1, 0, 0), int(bot.seeds.size()) - 1)) {
					system.fission(bot, Vector3D(1, 0, 0), int(bot.seeds.size()) - 1);
				} else 
				if (system.canVoid(bot, Vector3D(1, 0, 0))) {
					system.Void(bot, Vector3D(1, 0, 0));
				}
			} else
			if (v.y > 0) {
				if (system.canFission(bot, Vector3D(0, 1, 0), int(bot.seeds.size()) - 1)) {
					system.fission(bot, Vector3D(0, 1, 0), int(bot.seeds.size()) - 1);
				}
				else
				if (system.canVoid(bot, Vector3D(0, 1, 0))) {
					system.Void(bot, Vector3D(0, 1, 0));
				}
			} else
			if (v.z > 0) {
				if (system.canFission(bot, Vector3D(0, 0, 1), int(bot.seeds.size()) - 1)) {
					system.fission(bot, Vector3D(0, 0, 1), int(bot.seeds.size()) - 1);
				}
				else
				if (system.canVoid(bot, Vector3D(0, 0, 1))) {
					system.Void(bot, Vector3D(0, 0, 1));
				}
			} else
			if (v.x < 0) {
				if (system.canFission(bot, Vector3D(-1, 0, 0), int(bot.seeds.size()) - 1)) {
					system.fission(bot, Vector3D(-1, 0, 0), int(bot.seeds.size()) - 1);
				}
				else
				if (system.canVoid(bot, Vector3D(-1, 0, 0))) {
					system.Void(bot, Vector3D(-1, 0, 0));
				}
			}
			else
			if (v.y < 0) {
				if (system.canFission(bot, Vector3D(0, -1, 0), int(bot.seeds.size()) - 1)) {
					system.fission(bot, Vector3D(0, -1, 0), int(bot.seeds.size()) - 1);
				}
				else
				if (system.canVoid(bot, Vector3D(0, -1, 0))) {
					system.Void(bot, Vector3D(0, -1, 0));
				}
			}
			else
			if (v.z < 0) {
				if (system.canFission(bot, Vector3D(0, 0, -1), int(bot.seeds.size()) - 1)) {
					system.fission(bot, Vector3D(0, 0, -1), int(bot.seeds.size()) - 1);
				}
				else
				if (system.canVoid(bot, Vector3D(0, 0, -1))) {
					system.Void(bot, Vector3D(0, 0, -1));
				}
			}
			return 0;
		}
		else {
			system.wait(bot);
			start[id] = 1;
		}

		return 1;
	}

	int countMatrix(int y, int minx, int maxx, int minz, int maxz) {
		if (y < 0) return 0;
		int count = 0;
		for (int x = minx; x <= maxx; x++) {
			for (int z = minz; z <= maxz; z++) {
				if (system.matrix[x][y][z]) {
					count++;
				}
			}
		}
		return count;
	}


	int allMove(int dir, int dist) {
		vector<int> bid;

		dist = min(15, dist);
		dist = max(-15, dist);

		rep(i, system.bots.size()) {
			Bot &bot = system.bots[i];
			if (bot.isActive) bid.push_back(bot.id);
		}
		each(i, bid) {
			if (!system.smoveResult(system.bots[i - 1], dir, dist).first) {
				return 0;
			}
		}

		each(i, bid) {
			system.smove(system.bots[i - 1], dir, dist);
		}
		return 1;
	}

	int flip() {
		vector<int> bid;
		rep(i, system.bots.size()) {
			Bot &bot = system.bots[i];
			if (bot.isActive) bid.push_back(bot.id);
		}

		int count = 0;
		each(i, bid) {
			if (count == 0) {
				system.flip(system.bots[i - 1]);
			}
			else {
				system.wait(system.bots[i - 1]);
			}
			count++;
		}
		return 1;
	}


	vector<int> upf;

	int dig(int i) {
		int minx = min({ system.bots[i].pos.x, system.bots[i + 1].pos.x , system.bots[i + 2].pos.x , system.bots[i + 3].pos.x }),
			maxx = max({ system.bots[i].pos.x, system.bots[i + 1].pos.x , system.bots[i + 2].pos.x , system.bots[i + 3].pos.x });
		int minz = min({ system.bots[i].pos.z, system.bots[i + 1].pos.z , system.bots[i + 2].pos.z , system.bots[i + 3].pos.z }),
			maxz = max({ system.bots[i].pos.z, system.bots[i + 1].pos.z , system.bots[i + 2].pos.z , system.bots[i + 3].pos.z });

		if (countMatrix(system.bots[i].pos.y - 1, minx, maxx, minz, maxz)) {
			auto nd = V(0, -1, 0);

			if (system.canGVoidPlane(system.bots[i], system.bots[i + 2], system.bots[i + 1], system.bots[i + 3], nd, nd, nd, nd)) {
				system.GVoidPlane(system.bots[i], system.bots[i + 2], system.bots[i + 1], system.bots[i + 3], nd, nd, nd, nd);
			}
			else {
				flip();
			}
		}
		else {
			if (system.bots[i].pos.y == 1) {
				return 1;
			}
			else {
				if (system.harmonics && system.canLowHarmonicsSimpleHonesty()) {
					flip();
				}
				else {
					allMove(1, -1);
					return 0;
				}
			}
		}
		return 0;
	}

	int up() {
		if (system.bots[1].pos.y == RY) {
			return 1;
		}
		else {
			assert(allMove(1, RY - system.bots[1].pos.y) == 1);
			return 0;
		}
	}

	int bestMove(int i) {
		int mp[250][250];
		clr(mp);
		for (int x = LX; x < RX; x++) {
			for (int y = LY; y < RY; y++) {
				for (int z = LZ; z < RZ; z++) {
					if (system.matrix[x][y][z]) {
						mp[x][z]++;
					}
				}
			}
		}
		int best = 0; 
		int bestX, bestZ;
		for (int wx = LX; wx < RX; wx += WX) {
			if (wx + WX >= RX) break;
			for (int wz = LZ; wz < RZ; wz += WZ) {
				int count = 0;
				if (wz + WZ >= RZ) break;
				for (int x = wx; x < wx + WX; x++) {
					for (int z = wz; z < wz + WZ; z++) {
						count += mp[x][z];
					}
				}
				if (best < count) {
					best = count;
					bestX = wx; bestZ = wz;
				}
			}
		}

		for (int wz = LZ; wz < RZ; wz += WZ) {
			int count = 0;
			if (wz + WZ >= RZ) break;
			for (int x = RX - 1; x >= RX - WX - 1; x--) {
				for (int z = wz; z < wz + WZ; z++) {
					count += mp[x][z];
				}
			}
			if (best < count) {
				best = count;
				bestX = RX - WX - 1; bestZ = wz;
			}
		}
		for (int wx = LX; wx < RX; wx += WX) {
			int count = 0;
			if (wx + WX >= RX) break;
			for (int z = RZ - 1; z >= RZ - WZ - 1; z--) {
				for (int x = wx; x < wx + WX; x++) {
					count += mp[x][z];
				}
			}
			if (best < count) {
				best = count;
				bestX = wx; bestZ = RZ - WZ - 1;
			}
		}
		{
			int count = 0;
			for (int x = RX - 1; x >= RX - WX - 1; x--) {
				for (int z = RZ - 1; z >= RZ - WZ - 1; z--) {
					count += mp[x][z];
				}
			}
			if (best < count) {
				best = count;
				bestX = RX - WX - 1; bestZ = RZ - WZ - 1;
			}
		}

		if (bestX != system.bots[1].pos.x) {
			if (allMove(0, bestX - system.bots[1].pos.x) == 0) {
				if (bestZ == system.bots[1].pos.z) {
					return 1;
				} else
				if (allMove(2, bestZ - system.bots[1].pos.z) == 0) {
					return 1;
				} else return 0;
			}
			else return 0;
		} else
		if (bestZ != system.bots[1].pos.z) {
			if (allMove(2, bestZ - system.bots[1].pos.z) == 0) {
				if (bestX == system.bots[1].pos.x) {
					return 1;
				} else
				if (allMove(0, bestX - system.bots[1].pos.x) == 0) {
					return 1;
				}
				else return 0;
			}
			else return 0;
		}
		else {
			return 1;
		}
	}

	bool endExpansion;

	int expansion() {
		endExpansion = true;


		vector<int> bid;
		rep(i, system.bots.size()) {
			Bot &bot = system.bots[i];
			if (bot.isActive) bid.push_back(bot.id - 1);
		}

		each(i, bid) {
			system.wait(system.bots[i]);
		}

		return 1;
	}


	int think() {

		while(1) {
			if (upf[0] == 1) {
				if (dig(0)) {
					upf[0] = 2;
				}
				else break;
			}
			else
			if (upf[0] == 2) {
				if (up()) {
					upf[0] = 0;
				}
				else break;
			}
			else
			if (upf[0] == 0) {
				if (bestMove(0)) {
					upf[0] = 1;
				}
				else break;
			}
		}

		return 0;
	}

	bool standbyHalt() {

		if (system.harmonics) {
			flip();
			return 0;
		}

		vector<int> bid;
		rep(i, system.bots.size()) {
			Bot &bot = system.bots[i];
			if (bot.isActive) bid.push_back(bot.id-1);
		}


		if (bid.size() == 1) {
			if (system.bots[bid[0]].pos == Vector3D(0, 0, 0)) {
				return 1;
			}
			else {
				move(system.bots[bid[0]], V(0,0,0));
				return 0;
			}
		}

		vector<int> f(N, 0);
		int count = 0;
		each(i, bid) if(f[i] == 0){
			each(j, bid) if (f[i] == 0 && f[j] == 0 && i < j) {
				if (system.mlen(Vector3D(system.bots[i].pos.x - system.bots[j].pos.x, system.bots[i].pos.y - system.bots[j].pos.y, system.bots[i].pos.z - system.bots[j].pos.z)) == 1) {
					f[i] = 1;
					f[j] = 1;
					system.fusion(system.bots[i], system.bots[j]);
				}
			}
		}

		each(i, bid) if (f[i] == 0) {
			move(system.bots[i], V(0, 0, 0));
		}

		return 0;
	}

	void run() {

		LY = LX = LZ = 1e9;
		RY = RX = RZ = 0;
		rep(x, R) rep(y, R) rep(z, R) {
			if (system.matrix[x][y][z]) {
				LY = min(LY, y);
				RY = max(RY, y + 1);
				LX = min(LX, x);
				RX = max(RX, x + 1);
				LZ = min(LZ, z);
				RZ = max(RZ, z + 1);
			}
		}

		WX = min(30, RX - LX);
		WZ = min(30, RZ - LZ);

		target[3] = V(LX+WX, RY, LZ+WZ);
		target[2] = V(LX+WX, RY, LZ);
		target[1] = V(LX, RY, LZ);
		target[0] = V(LX, RY, LZ+WZ);

		N = 4;

		while(true) {
			system.beforeTurn();
			vector<int> bid;
			rep(i, system.bots.size()) {
				Bot &bot = system.bots[i];
				if (bot.isActive) bid.push_back(bot.id);
			}
			int startc = 0, endc = 0;
			each(i, bid) {
				if (start[i]) startc++;
			}

			if (system.isModelCreateComplete()) {
				if (standbyHalt()) break;
			} else
			if (endExpansion) {
				think();
			} else
			if (startc == bid.size()) {
				expansion();
			} else {
				each(i, bid) {
					standby(system.bots[i - 1]);
				}
			}

			system.afterTurn();
		}

		system.beforeTurn();
		system.halt();
		system.afterTurn();

	}

};

int main(int argc, char *argv[]) {
	string filename = "C:/workspace/icpfp-pc2018/problemsF/FD043_src.mdl";
	if (argc == 2) {
		filename = string(argv[1]);
	}
	int R = read_mdl(filename.c_str(), mode);
	int N = 40;

	AI(R, N).run();

	return 0;
}
